# Example of visitor pattern

fun visit (n *Node):
	println("Visiting: ", n.data)

fun main:
	var tree *Node = build_tree()

fun build_tree *Node:
	var root *Node = new_node(CAR, "Volvo")
#	root.left = new(Node)
#	root.left.data = 1
#	root.right = new(Node)
#	root.right.data = 3
	return root

fun new_node (type int, data string) *Node :
	var n *Node = new(Node)
	n.type = type
	n.data = data
	return n

struct Node:
#	children Node[4] *
	type int = 0
	data string

const int:
	CAR
	ENGINE
	WHEEL
