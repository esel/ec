use token::*
use position::*
use utils::*

struct public Scanner:
	infile file
	pos Position
	char_buffer int[CHAR_BUFFER_SIZE]
	char_buffer_size int = 0
	char_buffer_index int = 0
	is_peeked bool = false
	is_error bool = false
	error_reason string
	peeked Token
	is_prev_eol bool = true
	ignore_eol bool = false
	ignored_eol_since_next int = 0

fun public scanner_next_token (ctx *Scanner) Token :
	var next Token
	if ctx.ignored_eol_since_next != 0 && !ctx.ignore_eol:
		next.type = TOK_EOL
	elif ctx.is_peeked:
		ctx.is_peeked = false
		next = ctx.peeked
	else:
		next = next_token_filtered(ctx)
	ctx.ignored_eol_since_next = 0
	return next

fun public scanner_peek_token (ctx *Scanner) Token :
	if !ctx.is_peeked:
		ctx.is_peeked = true
		ctx.peeked = next_token_filtered(ctx)
	return ctx.peeked

fun public scanner_ignore_newlines (ctx *Scanner, ignore bool) :
	ctx.ignore_eol = ignore

fun read_char0 (ctx *Scanner) int :
	var c int = 0
	var i int = 0
	while i < CHAR_BUFFER_SIZE:
		c = c::fgetc(ctx.infile)
		if c == eof:
			break
		ctx.char_buffer[i] = c
		i += 1
	if c == eof && c::ferror(ctx.infile):
		ctx.is_error = true
		ctx.error_reason = "error reading character from file"
	return i

fun read_char (ctx *Scanner) int :
	var size int
	var index int
	if ctx.char_buffer_size == 0:
		size = read_char0(ctx)
		index = 0
	else:
		size = ctx.char_buffer_size
		index = ctx.char_buffer_index
	if size == 0:
		return eof
	var c int = ctx.char_buffer[index]
	ctx.char_buffer_size = size - 1
	ctx.char_buffer_index = index + 1
	if c == to_char("\n"):
		ctx.pos.col = -1
		ctx.pos.line += 1
		ctx.pos.indent = 0
	else:
		ctx.pos.col += 1
		if c == to_char("\t"):
			ctx.pos.indent += 1
	return c

fun peek_char (ctx *Scanner) int :
	if ctx.char_buffer_size == 0:
		ctx.char_buffer_size = read_char0(ctx)
		ctx.char_buffer_index = 0
	if ctx.char_buffer_size == 0:
		return eof
	return ctx.char_buffer[ctx.char_buffer_index]

fun is_digit (c int) bool :
	return to_char("0") <= c && c <= to_char("9")

fun is_alpha (c int) bool :
	return (c >= to_char("a") && c <= to_char("z")) || (c >= to_char("A") && c <= to_char("Z")) || (c == to_char("_"))

fun is_alphanumeric (c int) bool :
	return is_digit(c) || is_alpha(c)

fun is_whitespace (c int) bool :
	return (c == to_char(" ")) || (c == to_char("\r")) || (c == to_char("\t"))

fun next_non_whitespace_char (ctx *Scanner) int :
	var c int = read_char(ctx)
	while is_whitespace(c):
		c = read_char(ctx)
	return c

fun is_number_literal_end (c int) bool :
	return c == eof || !(is_alphanumeric(c) || c == to_char(".") || c == to_char("+") || c == to_char("-"))

fun scan_number_literal (ctx *Scanner, tok *Token, c int) :
	var is_float bool = false
	while true:
		if c == to_char("."):
			is_float = true
		tok.text += to_char(c)
		c = peek_char(ctx)
		if is_number_literal_end(c):
			break
		read_char(ctx)
	var err bool
	if is_float:
		tok.type = TOK_LITERAL_FLOAT
		tok.float_value = to_float(tok.text, err)
	else:
		tok.type = TOK_LITERAL_INT
		tok.int_value = to_int(tok.text, err)
	if err:
		ctx.is_error = true
		ctx.error_reason = "invalid number literal: " + tok.text

fun scan_keyword_or_id (ctx *Scanner, tok *Token, c int) :
	var buf int[MAX_ID_LENGTH]
	buf[0] = c
	for i in 1, MAX_ID_LENGTH :
		c = peek_char(ctx)
		if( is_alpha(c) || is_digit(c) ):
			read_char(ctx)
			buf[i] = c
		else:
			var s string = to_char(buf, i)
			if "int" == s :        tok.type = TOK_INT
			elif "float" == s :    tok.type = TOK_FLOAT
			elif "string" == s :   tok.type = TOK_STRING
			elif "file" == s :     tok.type = TOK_FILE
			elif "bool" == s :     tok.type = TOK_BOOL
			elif "true" == s :     tok.type = TOK_TRUE
			elif "false" == s :    tok.type = TOK_FALSE
			elif "nil" == s :      tok.type = TOK_NIL
			elif "fun" == s :      tok.type = TOK_FUN
			elif "struct" == s :   tok.type = TOK_STRUCT
			elif "const" == s :    tok.type = TOK_CONST
			elif "var" == s :      tok.type = TOK_VAR
			elif "return" == s :   tok.type = TOK_RETURN
			elif "break" == s :    tok.type = TOK_BREAK
			elif "continue" == s : tok.type = TOK_CONTINUE
			elif "if" == s :       tok.type = TOK_IF
			elif "elif" == s :     tok.type = TOK_ELIF
			elif "else" == s :     tok.type = TOK_ELSE
			elif "while" == s :    tok.type = TOK_WHILE
			elif "for" == s :      tok.type = TOK_FOR
			elif "use" == s :      tok.type = TOK_USE
			elif "in" == s :       tok.type = TOK_IN
			elif "public" == s :   tok.type = TOK_PUBLIC
			else:
				tok.text = s
				tok.type = TOK_ID
			return
	ctx.is_error = true
	ctx.error_reason = "internal error: id too long"

fun scan_dot_or_float_literal (ctx *Scanner, tok *Token, c int) :
	if is_digit(peek_char(ctx)):
		scan_number_literal(ctx, tok, c)
	else:
		tok.type = TOK_DOT

fun get_escaped_char (c int) int :
	if c == to_char("n"):   return to_char("\n")
	if c == to_char("r"):   return to_char("\r")
	if c == to_char("t"):   return to_char("\t")
	if c == to_char("\\"):  return to_char("\\")
	if c == to_char("\""):  return to_char("\"")
	return eof

fun scan_string_literal (ctx *Scanner, tok *Token, c int) :
	var escape bool = false
	var end_quote int = c
	for i in 1, MAX_STRING_LITERAL_LENGTH:
		c = read_char(ctx)
		if escape:
			escape = false
			var ec int = get_escaped_char(c)
			if ec == eof:
				ctx.is_error = true
				ctx.error_reason = "invalid escape character"
				return
			else:
				tok.text += to_char(ec)
		elif c == to_char("\\"):
			escape = true
		elif c == end_quote:
			tok.type = TOK_LITERAL_STRING
			return
		else:
			tok.text += to_char(c)
	ctx.is_error = true
	ctx.error_reason = "internal error: string literal too long"

fun consume_empty_lines (ctx *Scanner) :
	var c int = peek_char(ctx)
	while is_whitespace(c) || c == to_char('\n'):
		read_char(ctx)
		c = peek_char(ctx)

fun scan_comment_eol0 (ctx *Scanner, tok *Token) :
	var c int = read_char(ctx)
	while c != to_char('\n') && c != eof:
		c = read_char(ctx)

fun scan_comment_eol (ctx *Scanner, tok *Token) :
	var c int
	while true:
		scan_comment_eol0(ctx, tok)
		consume_empty_lines(ctx)
		c = peek_char(ctx)
		if c != to_char('#'):
			break
	if ctx.is_prev_eol:
		tok.type = TOK_IGNORE
	else:
		tok.type = TOK_EOL

fun scan_op2 (ctx *Scanner, next string, neq int, eq int) int :
	if peek_char(ctx) != to_char(next): return neq
	read_char(ctx)
	return eq

fun scan_op3 (ctx *Scanner, next1 string, next2 string, eq0 int, eq1 int, eq2 int) int :
	var c int = peek_char(ctx)
	if c == to_char(next1):
		read_char(ctx)
		return eq1
	if c == to_char(next2):
		read_char(ctx)
		return eq2
	return eq0

fun next_token_raw (ctx *Scanner) Token :
	var tok Token
	var c int
	while true:
		c = next_non_whitespace_char(ctx)
		if !ctx.is_prev_eol || (c != to_char('\n')): break
	tok.pos = ctx.pos
	if c == to_char("\n"):
		tok.type = TOK_EOL
		consume_empty_lines(ctx)
		ctx.is_prev_eol = true
	else:
		if is_alpha(c):            scan_keyword_or_id(ctx, &tok, c)
		elif is_digit(c):          scan_number_literal(ctx, &tok, c)
		elif c == to_char("."):    scan_dot_or_float_literal(ctx, &tok, c)
		elif c == to_char('"'):    scan_string_literal(ctx, &tok, c)
		elif c == to_char("'"):    scan_string_literal(ctx, &tok, c)
		elif c == to_char("#"):    scan_comment_eol(ctx, &tok)
		elif c == to_char("{"):    tok.type = TOK_GENERICS_OPEN
		elif c == to_char("}"):    tok.type = TOK_GENERICS_CLOSE
		elif c == to_char("["):    tok.type = TOK_ARRAY_OPEN
		elif c == to_char("]"):    tok.type = TOK_ARRAY_CLOSE
		elif c == to_char("("):    tok.type = TOK_ARGS_OPEN
		elif c == to_char(")"):    tok.type = TOK_ARGS_CLOSE
		elif c == to_char(","):    tok.type = TOK_ARGS_DELIM
		elif c == to_char("*"):    tok.type = TOK_STAR
		elif c == to_char("%"):    tok.type = TOK_PERCENT
		elif c == to_char("/"):    tok.type = TOK_SLASH
		elif c == to_char("~"):    tok.type = TOK_NOT
		elif c == to_char("^"):    tok.type = TOK_XOR
		elif c == to_char(":"):    tok.type = scan_op2(ctx, ":", TOK_COLON, TOK_MODULE)
		elif c == to_char("="):    tok.type = scan_op2(ctx, "=", TOK_ASSIGN, TOK_EQ)
		elif c == to_char("!"):    tok.type = scan_op2(ctx, "=", TOK_LOGICAL_NOT, TOK_NEQ)
		elif c == to_char("+"):    tok.type = scan_op2(ctx, "=", TOK_PLUS, TOK_ASSIGN_INC)
		elif c == to_char("-"):    tok.type = scan_op2(ctx, "=", TOK_MINUS, TOK_ASSIGN_DEC)
		elif c == to_char("&"):    tok.type = scan_op3(ctx, "&", "=", TOK_AND, TOK_LOGICAL_AND, TOK_ASSIGN_AND)
		elif c == to_char("|"):    tok.type = scan_op3(ctx, "|", "=", TOK_OR, TOK_LOGICAL_OR, TOK_ASSIGN_OR)
		elif c == to_char("<"):    tok.type = scan_op3(ctx, "=", "<", TOK_LT, TOK_LTEQ, TOK_SHIFT_LEFT)
		elif c == to_char(">"):    tok.type = scan_op3(ctx, "=", ">", TOK_GT, TOK_GTEQ, TOK_SHIFT_RIGHT)
		elif c == eof:             tok.type = TOK_EOF
		else:
			ctx.is_error = true
			ctx.error_reason = "unrecognized token at " + pos_to_string(&ctx.pos)
		ctx.is_prev_eol = false
	return tok

fun next_token_filtered (ctx *Scanner) Token :
	var tok Token
	while true:
		tok = next_token_raw(ctx)
		if ctx.ignore_eol && tok.type == TOK_EOL:
			ctx.ignored_eol_since_next += 1
		elif tok.type != TOK_IGNORE:
			break
	return tok

const public : CHAR_BUFFER_SIZE int = 256 # FIXME must be public since it is used in a public type
const: MAX_ID_LENGTH int = 256
const: MAX_STRING_LITERAL_LENGTH int = (1 << 16)
