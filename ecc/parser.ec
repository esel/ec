use scanner::*
use emitter::*
use utils::*
use token::*
use position::*
use ast::*
use log::*
use modules::*

struct public Parser:
	log Log
	ast *Node = nil
	scanner Scanner
	modules *Modules

fun parser_exit_mismatch (ctx *Parser, actual *Token, msg string) :
	println("PARSER ERROR: unexpected token ", token_to_string(actual))
	println(msg)
	c::exit(1)

fun parser_exit_error (ctx *Parser, msg string, pos Position) :
	println("PARSER ERROR: ", msg)
	println("Near ", pos_to_string(&pos))
	c::exit(1)

fun public parse_prog (ctx *Parser) *Node :
	ctx.ast = new_node(AST_PROG)
	parse_use(ctx, ctx.ast)
	while true:
		var tok Token = next_token(ctx)
		if tok.type == TOK_CONST:
			parse_const(ctx, ctx.ast, &tok)
		elif tok.type == TOK_STRUCT:
			parse_struct(ctx, ctx.ast, &tok)
		elif tok.type == TOK_FUN:
			parse_fun(ctx, ctx.ast, &tok)
		elif tok.type == TOK_EOF:
			break
		else:
			parser_exit_mismatch(ctx, &tok, "expected constant, structure or function")
	return ctx.ast

fun parse_use (ctx *Parser, root *Node) :
	while peek_token(ctx).type == TOK_USE:
		next_token(ctx)
		var module Token = match(ctx, TOK_ID)
		if peek_token(ctx).type == TOK_MODULE:
			next_token(ctx)
			if peek_token(ctx).type == TOK_STAR:
				next_token(ctx)
				import_module(ctx, &module)
			else:
				parser_exit_error(ctx, "TODO only star allowed for now", module.pos)
		add_new_node(root, AST_USE, module)
		match_end(ctx)

fun import_module (ctx *Parser, module *Token) :
	var file_name string = module.text + ".ec"
	for i in 0, ctx.modules.n_search_dirs:
		var path string = ctx.modules.search_dirs[i] + "/" + file_name
		log_start_debug(&ctx.log, "SEARCHING path ")
		log_end_debug(&ctx.log, path)
		var f file = fopen_ec(path, "rb")
		if f != nil:
			log_start_debug(&ctx.log, "LOADING symbols from file: ")
			log_end_debug(&ctx.log, path)
			var module_index int = ctx.modules.n_used_modules
			var m *Module = &ctx.modules.used_modules[module_index]
			ctx.modules.n_used_modules += 1
			var ctx2 Parser
			ctx2.scanner.infile = f
			ctx2.log = ctx.log
			m.ast = import_module_symbols(&ctx2)
			m.name = get_module_name_from_file_name(module.text)
			fclose_ec(f)
			return
	parser_exit_error(ctx, "could not find file: " + file_name, module.pos)

fun import_module_symbols (ctx *Parser) *Node :
	var ast *Node = new_node(AST_PROG)
	var tok Token
	while tok.type != TOK_EOF:
		tok = next_token(ctx)
		var was_imported bool = true
		if tok.type == TOK_STRUCT && peek_token(ctx).type == TOK_PUBLIC:
			parse_struct(ctx, ast, &tok)
		elif tok.type == TOK_FUN && peek_token(ctx).type == TOK_PUBLIC:
			parse_fun(ctx, ast, &tok)
		elif tok.type == TOK_CONST && peek_token(ctx).type == TOK_PUBLIC:
			parse_const(ctx, ast, &tok)
		else:
			was_imported = false
		if was_imported:
			var typenode *Node = ast.children.last
			set_node_flag(typenode, NODE_FLAG_IMPORTED)
			log_start_debug(&ctx.log, "GOT PUBLIC SYMBOL: ")
			log_end_debug(&ctx.log, typenode.data.text)
	return ast

fun parse_const_single (ctx *Parser, root *Node, is_const_list bool, is_single_type bool, single_type Token, is_public bool) :
	var node *Node = add_new_node(root, AST_CONST, match(ctx, TOK_ID))
	if is_public:
		set_node_flag(node, NODE_FLAG_PUBLIC)
	if is_single_type:
		add_new_node(node, AST_VAR_TYPE, single_type)
	else:
		add_new_node(node, AST_VAR_TYPE, next_token(ctx))
	var assign Token = peek_token(ctx)
	if assign.type == TOK_ASSIGN:
		next_token(ctx)
		add_node(node, parse_expr_rvalue(ctx))
	match_end(ctx)

fun parse_const (ctx *Parser, root *Node, consttok *Token) :
	var type Token
	var is_public bool = false
	if peek_token(ctx).type == TOK_PUBLIC:
		next_token(ctx)
		is_public = true
	var is_single_type bool = false
	if peek_token(ctx).type != TOK_COLON:
		is_single_type = true
		type = match_type(ctx)
	match(ctx, TOK_COLON)
	if !is_single_type && is_next_token_not_end(ctx):
		parse_const_single(ctx, root, false, false, type, is_public)
	else:
		match_end(ctx)
		var node *Node = add_new_node(root, AST_CONST_LIST, *consttok)
		if is_public:
			set_node_flag(node, NODE_FLAG_PUBLIC)
		while true:
			parse_const_single(ctx, node, true, is_single_type, type, is_public)
			if !is_next_token_ast_child(ctx, consttok):
				break

fun parse_generic_parameters (ctx *Parser, node *Node) *Node :
	var type_list *Node = add_node(node, new_node(AST_GENERIC_PARAMS))
	if peek_token(ctx).type == TOK_GENERICS_OPEN:
		type_list.data = next_token(ctx)
		set_node_flag(node, NODE_FLAG_GENERIC)
		while peek_token(ctx).type != TOK_GENERICS_CLOSE:
			add_new_node(type_list, AST_GENERIC_PARAM, match(ctx, TOK_ID))
			if peek_token(ctx).type != TOK_GENERICS_CLOSE:
				match(ctx, TOK_ARGS_DELIM)
		match(ctx, TOK_GENERICS_CLOSE)
	return type_list

fun parse_struct (ctx *Parser, root *Node, typetok *Token) :
	var flags int = 0
	if peek_token(ctx).type == TOK_PUBLIC:
		next_token(ctx)
		flags = set_flag(flags, NODE_FLAG_PUBLIC)
	var node *Node = add_new_node(root, AST_STRUCT, match(ctx, TOK_ID))
	node.flags = flags
	parse_generic_parameters(ctx, node)
	match(ctx, TOK_COLON)
	match_end(ctx)
	while true:
		parse_statement_var(ctx, node, true)
		if !is_next_token_ast_child(ctx, typetok):
			break

fun parse_fun (ctx *Parser, root *Node, funtok *Token) :
	var flags int = 0
	if peek_token(ctx).type == TOK_PUBLIC:
		next_token(ctx)
		flags = set_flag(flags, NODE_FLAG_PUBLIC)
	var fnode *Node = add_new_node(root, AST_FUN, match(ctx, TOK_ID))
	fnode.flags = flags
	parse_generic_parameters(ctx, fnode)
	parse_fun_signature(ctx, fnode)
	match(ctx, TOK_COLON)
	match_end(ctx)
	parse_statement_block(ctx, fnode, funtok)

fun parse_fun_signature (ctx *Parser, parent *Node) :
	if peek_token(ctx).type == TOK_ARGS_OPEN:
		next_token(ctx)
		while peek_token(ctx).type != TOK_ARGS_CLOSE:
			var param *Node = add_new_node(parent, AST_PARAM, match(ctx, TOK_ID))
			parse_fun_type(ctx, AST_PARAM_TYPE, parent, param)
			if peek_token(ctx).type != TOK_ARGS_CLOSE:
				match(ctx, TOK_ARGS_DELIM)
		match(ctx, TOK_ARGS_CLOSE)
	var tok Token = peek_token(ctx)
	if tok.type != TOK_ARGS_DELIM && tok.type != TOK_COLON && is_token_not_end(tok):
		parse_fun_type(ctx, AST_FUN_TYPE, parent, parent)

fun parse_fun_type (ctx *Parser, type int, fnode *Node, idnode *Node) :
	if peek_token(ctx).type == TOK_FUN:
		var typenode *Node = add_new_node(idnode, type, next_token(ctx))
		parse_fun_signature (ctx, typenode)
	else:
		var flags int = 0
		if peek_token(ctx).type == TOK_STAR:
			next_token(ctx)
			flags = set_flag(flags, NODE_FLAG_POINTER)
		var typenode *Node = add_new_node(idnode, type, match_type(ctx))
		if peek_token(ctx).type == TOK_GENERICS_OPEN:
			var type_list *Node = add_new_node(idnode, AST_GENERIC_PARAMS, next_token(ctx))
			while peek_token(ctx).type != TOK_GENERICS_CLOSE:
				add_new_node(type_list, AST_GENERIC_PARAM, match_type(ctx))
			match(ctx, TOK_GENERICS_CLOSE)
		if peek_token(ctx).type == TOK_ARRAY_OPEN:
			next_token(ctx)
			match(ctx, TOK_ARRAY_CLOSE)
			flags = set_flag(flags, NODE_FLAG_ARRAY)
		typenode.flags = flags
		idnode.flags |= flags

fun parse_statement_block (ctx *Parser, root *Node, parent *Token) :
	var block *Node = add_new_node(root, AST_BLOCK, *parent)
	var tok Token = peek_token(ctx)
	while is_ast_child(*parent, tok):
		parse_statement_compound(ctx, block, &tok)
		tok = peek_token(ctx)

fun parse_statement_compound (ctx *Parser, block *Node, tok *Token) :
	if tok.type == TOK_VAR:
		next_token(ctx)
		parse_statement_var(ctx, block, false)
	elif tok.type == TOK_IF:
		parse_statement_if(ctx, block)
	elif tok.type == TOK_WHILE:
		parse_statement_while(ctx, block)
	elif tok.type == TOK_FOR:
		parse_statement_for(ctx, block)
	else:
		parse_statement_simple(ctx, block, tok)

fun parse_statement_simple (ctx *Parser, block *Node, tok *Token) :
	if tok.type == TOK_RETURN:
		parse_statement_return(ctx, block)
	elif tok.type == TOK_BREAK:
		add_new_node(block, AST_BREAK, next_token(ctx))
	elif tok.type == TOK_CONTINUE:
		add_new_node(block, AST_CONTINUE, next_token(ctx))
	else:
		add_node(block, parse_expr_statement(ctx))
	match_end(ctx)

fun add_int_expr (ctx *Parser, parent *Node, i int) :
	var tok Token
	tok.type = TOK_LITERAL_INT
	tok.int_value = i
	add_new_node(parent, AST_EXPR_CONST, tok)

fun parse_statement_var (ctx *Parser, parent *Node, is_struct_member bool) :
	var is_array bool = false
	var is_array_dim bool = false
	var is_pointer bool = false
	var flags int = 0
	var vnode *Node = add_new_node(parent, AST_VAR, match(ctx, TOK_ID))
	var typenode *Node = add_node(vnode, new_node(AST_VAR_TYPE))
	if peek_token(ctx).type == TOK_FUN:
		typenode.data = next_token(ctx)
		parse_fun_signature (ctx, typenode)
	else:
		if peek_token(ctx).type == TOK_STAR:
			next_token(ctx)
			is_pointer = true
			flags = set_flag(flags, NODE_FLAG_POINTER)
		typenode.data = match_type(ctx)
		if peek_token(ctx).type == TOK_GENERICS_OPEN:
			var generic_types *Node = add_new_node(vnode, AST_GENERIC_PARAMS, next_token(ctx))
			while peek_token(ctx).type != TOK_GENERICS_CLOSE:
				add_new_node(generic_types, AST_GENERIC_PARAM, match_type(ctx))
				if peek_token(ctx).type != TOK_GENERICS_CLOSE:
					match(ctx, TOK_ARGS_DELIM)
			match(ctx, TOK_GENERICS_CLOSE)
	if peek_token(ctx).type == TOK_ARRAY_OPEN:
		next_token(ctx)
		is_array = true
		flags = set_flag(flags, NODE_FLAG_ARRAY)
		if peek_token(ctx).type != TOK_ARRAY_CLOSE:
			is_array_dim = true
			var dim_expr *Node = add_node(vnode, parse_expr_rvalue(ctx))
			set_node_flag(dim_expr, NODE_FLAG_EXPR_ARRAY_DIM)
		match(ctx, TOK_ARRAY_CLOSE)
		if peek_token(ctx).type == TOK_STAR:
			next_token(ctx)
			flags = set_flag(flags, NODE_FLAG_ARRAY_OF_POINTERS)
	vnode.flags = flags
	typenode.flags = flags
	if peek_token(ctx).type == TOK_ASSIGN:
		var assigntok Token = next_token(ctx)
		var init_expr *Node = parse_expr_rvalue(ctx)
		set_node_flag(init_expr, NODE_FLAG_EXPR_INIT)
		if is_struct_member:
			assert(parent.type == AST_STRUCT, "parent is a struct")
			add_node(vnode, init_expr)
		else:
			var init_assign *Node = add_new_node(parent, AST_EXPR_ASSIGN, assigntok)
			var init_id *Node = add_new_node(init_assign, AST_EXPR_ID, vnode.data)
			add_node(init_assign, init_expr)
		if is_array && !is_array_dim:
			add_int_expr(ctx, vnode, init_expr.children.count)
	elif is_array && !is_pointer && !is_array_dim:
		parser_exit_error(ctx, "missing array dimension in previous array declaration", vnode.data.pos)
	match_end(ctx)

fun parse_conditional (ctx *Parser, node *Node, parenttok *Token, is_else bool) :
	if !is_else:
		add_node(node, parse_expr_rvalue(ctx))
	match(ctx, TOK_COLON)
	var tok Token = peek_token(ctx)
	var block *Node
	if is_token_not_end(tok):
		block = add_new_node(node, AST_BLOCK, *parenttok)
		parse_statement_simple(ctx, block, &tok)
	else:
		next_token(ctx)
		parse_statement_block(ctx, node, parenttok)
		block = node.children.last
	if is_else:
		set_node_flag(block, NODE_FLAG_ELSE)

fun parse_statement_if (ctx *Parser, root *Node) :
	var iftok Token = match(ctx, TOK_IF)
	var ifindent int = iftok.pos.indent
	var node *Node = add_new_node(root, AST_IF, iftok)
	parse_conditional(ctx, node, &iftok, false)
	while true:
		var eliftok Token = peek_token(ctx)
		if eliftok.type == TOK_ELIF && eliftok.pos.indent == ifindent:
			next_token(ctx)
			parse_conditional(ctx, node, &eliftok, false)
		else:
			break
	var elsetok Token = peek_token(ctx)
	if peek_token(ctx).type == TOK_ELSE && elsetok.pos.indent == ifindent:
		next_token(ctx)
		parse_conditional(ctx, node, &elsetok, true)

fun parse_statement_while (ctx *Parser, root *Node) :
	var parenttok Token = match(ctx, TOK_WHILE)
	var node *Node = add_new_node(root, AST_WHILE, parenttok)
	add_node(node, parse_expr_rvalue(ctx))
	match(ctx, TOK_COLON)
	match_end(ctx)
	parse_statement_block(ctx, node, &parenttok)

fun parse_statement_for (ctx *Parser, root *Node) :
	# TODO for now only supports the syntax "for i in 0, 10:" with i of int type
	var parenttok Token = match(ctx, TOK_FOR)
	var node *Node = add_new_node(root, AST_FOR, parenttok)
	var vnode *Node = add_new_node(node, AST_VAR, match(ctx, TOK_ID))
	var type_token Token
	type_token.type = TOK_INT
	add_new_node(vnode, AST_VAR_TYPE, type_token)
	match(ctx, TOK_IN)
	add_node(node, parse_expr_rvalue(ctx))
	match(ctx, TOK_ARGS_DELIM)
	add_node(node, parse_expr_rvalue(ctx))
	match(ctx, TOK_COLON)
	match_end(ctx)
	parse_statement_block(ctx, node, &parenttok)

fun parse_statement_return (ctx *Parser, root *Node) :
	var rnode *Node = add_new_node(root, AST_RETURN, match(ctx, TOK_RETURN))
	if is_next_token_not_end(ctx):
		add_node(rnode, parse_expr_rvalue(ctx))

fun parse_expr0 (ctx *Parser) *Node :
	return parse_expr_binary(ctx)

fun parse_expr_rvalue (ctx *Parser) *Node :
	scanner_ignore_newlines(&ctx.scanner, true)
	var expr *Node = parse_expr0(ctx)
	scanner_ignore_newlines(&ctx.scanner, false)
	return expr

fun parse_expr_statement (ctx *Parser) *Node :
	var e *Node = parse_expr_rvalue(ctx)
	var t Token = peek_token(ctx)
	if t.type == TOK_ASSIGN || t.type == TOK_ASSIGN_INC || t.type == TOK_ASSIGN_DEC || t.type == TOK_ASSIGN_OR || t.type == TOK_ASSIGN_AND:
		next_token(ctx)
		var lvalue *Node = e
		var rvalue *Node = parse_expr_rvalue(ctx)
		e = new_node(AST_EXPR_ASSIGN)
		e.data = t
		add_node(e, lvalue)
		add_node(e, rvalue)
	return e

fun parse_expr_postfix (ctx *Parser) *Node :
	var e *Node = parse_expr_primary(ctx)
	while true:
		var t Token = peek_token(ctx)
		if t.type == TOK_MODULE:
			if e.type != AST_EXPR_ID:
				parser_exit_mismatch(ctx, &t, "invalid module specifier")
			next_token(ctx)
			var funcname Token = next_token(ctx)
			var call *Node = parse_expr_call(ctx, &funcname)
			var modulecall *Node = new_node(AST_CALL_MODULE)
			add_node(modulecall, e)
			add_node(modulecall, call)
			e = modulecall
		elif t.type == TOK_ARGS_OPEN || t.type == TOK_GENERICS_OPEN:
			if e.type != AST_EXPR_ID:
				parser_exit_mismatch(ctx, &t, "malformed function call")
			var call *Node = parse_expr_call(ctx, &e.data)
			destroy_ast(e)
			e = call
		elif t.type == TOK_ARRAY_OPEN:
			var node *Node = new_node(AST_EXPR_ARRAY)
			node.data = next_token(ctx)
			add_node(node, e)
			add_node(node, parse_expr0(ctx))
			e = node
			match(ctx, TOK_ARRAY_CLOSE)
		elif t.type == TOK_DOT:
			var node *Node = new_node(AST_EXPR_DOT)
			node.data = next_token(ctx)
			add_node(node, e)
			var id *Node = add_new_node(node, AST_EXPR_ID, match(ctx, TOK_ID))
			e = node
		else:
			break
	return e

fun parse_expr_call (ctx *Parser, id *Token) *Node :
	assert(id.type == TOK_ID, "token is an identifier")
	var cnode *Node = new_node(AST_CALL)
	cnode.data = *id
	if peek_token(ctx).type == TOK_GENERICS_OPEN:
		set_node_flag(cnode, NODE_FLAG_GENERIC)
		var generic_types *Node = add_new_node(cnode, AST_GENERIC_PARAMS, next_token(ctx))
		while peek_token(ctx).type != TOK_GENERICS_CLOSE:
			add_new_node(generic_types, AST_GENERIC_PARAM, match_type(ctx))
		match(ctx, TOK_GENERICS_CLOSE)
	match(ctx, TOK_ARGS_OPEN)
	var next Token = peek_token(ctx)
	while next.type != TOK_ARGS_CLOSE:
		if next.type == TOK_INT || next.type == TOK_FLOAT || next.type == TOK_STRING:
			# some builtin functions accept type arguments
			add_new_node(cnode, AST_EXPR_ID, next_token(ctx))
		else:
			# argument is not a native type, but a user type (ID) or an expression
			add_node(cnode, parse_expr0(ctx))
		if peek_token(ctx).type != TOK_ARGS_CLOSE:
			match(ctx, TOK_ARGS_DELIM)
		next = peek_token(ctx)
	match(ctx, TOK_ARGS_CLOSE)
	return cnode

fun parse_expr_primary (ctx *Parser) *Node :
	var t Token = next_token(ctx)
	var node *Node = nil
	var save_token bool = true
	if t.type == TOK_LITERAL_INT || t.type == TOK_LITERAL_FLOAT || t.type == TOK_LITERAL_STRING || t.type == TOK_NIL || t.type == TOK_TRUE || t.type == TOK_FALSE:
		node = new_node(AST_EXPR_CONST)
	elif t.type == TOK_ID:
		node = new_node(AST_EXPR_ID)
	elif t.type == TOK_ARGS_OPEN:
		save_token = false
		node = parse_expr0(ctx)
		match(ctx, TOK_ARGS_CLOSE)
	elif t.type == TOK_ARRAY_OPEN:
		node = new_node(AST_ARRAY_LITERAL)
		var item Token = peek_token(ctx)
		while item.type != TOK_ARRAY_CLOSE:
			add_node(node, parse_expr0(ctx))
			item = peek_token(ctx)
			if item.type != TOK_ARRAY_CLOSE:
				match(ctx, TOK_ARGS_DELIM)
				item = peek_token(ctx)
		match(ctx, TOK_ARRAY_CLOSE)
		t = match_type(ctx)
	else:
		parser_exit_mismatch(ctx, &t, "expected a constant, identifier or '('")
	if save_token:
		node.data = t
	return node

fun parse_expr_binary (ctx *Parser) *Node :
	return parse_expr_log_or(ctx)

fun parse_expr_log_or (ctx *Parser) *Node :
	var expr *Node = parse_expr_log_and(ctx)
	var op Token = peek_token(ctx)
	while op.type == TOK_LOGICAL_OR:
		next_token(ctx)
		expr = new_expr_binary(op, expr, parse_expr_log_and(ctx))
		op = peek_token(ctx)
	return expr

fun parse_expr_log_and (ctx *Parser) *Node :
	var expr *Node = parse_expr_bit_or(ctx)
	var op Token = peek_token(ctx)
	while op.type == TOK_LOGICAL_AND:
		next_token(ctx)
		expr = new_expr_binary(op, expr, parse_expr_bit_or(ctx))
		op = peek_token(ctx)
	return expr

fun parse_expr_bit_or (ctx *Parser) *Node :
	var expr *Node = parse_expr_bit_xor(ctx)
	var op Token = peek_token(ctx)
	while op.type == TOK_OR:
		next_token(ctx)
		expr = new_expr_binary(op, expr, parse_expr_bit_xor(ctx))
		op = peek_token(ctx)
	return expr

fun parse_expr_bit_xor (ctx *Parser) *Node :
	var expr *Node = parse_expr_bit_and(ctx)
	var op Token = peek_token(ctx)
	while op.type == TOK_XOR:
		next_token(ctx)
		expr = new_expr_binary(op, expr, parse_expr_bit_and(ctx))
		op = peek_token(ctx)
	return expr

fun parse_expr_bit_and (ctx *Parser) *Node :
	var expr *Node = parse_expr_eq_neq(ctx)
	var op Token = peek_token(ctx)
	while op.type == TOK_AND:
		next_token(ctx)
		expr = new_expr_binary(op, expr, parse_expr_eq_neq(ctx))
		op = peek_token(ctx)
	return expr

fun parse_expr_eq_neq (ctx *Parser) *Node :
	var expr *Node = parse_expr_lt_gt(ctx)
	var op Token = peek_token(ctx)
	while op.type == TOK_EQ || op.type == TOK_NEQ:
		next_token(ctx)
		expr = new_expr_binary(op, expr, parse_expr_lt_gt(ctx))
		op = peek_token(ctx)
	return expr

fun parse_expr_lt_gt (ctx *Parser) *Node :
	var expr *Node = parse_expr_shift(ctx)
	var op Token = peek_token(ctx)
	while op.type == TOK_LT || op.type == TOK_LTEQ || op.type == TOK_GT || op.type == TOK_GTEQ:
		next_token(ctx)
		expr = new_expr_binary(op, expr, parse_expr_shift(ctx))
		op = peek_token(ctx)
	return expr

fun parse_expr_shift (ctx *Parser) *Node :
	var expr *Node = parse_expr_add(ctx)
	var op Token = peek_token(ctx)
	while op.type == TOK_SHIFT_LEFT || op.type == TOK_SHIFT_RIGHT:
		next_token(ctx)
		expr = new_expr_binary(op, expr, parse_expr_add(ctx))
		op = peek_token(ctx)
	return expr

fun parse_expr_add (ctx *Parser) *Node :
	var expr *Node = parse_expr_mul(ctx)
	var op Token = peek_token(ctx)
	while op.type == TOK_PLUS || op.type == TOK_MINUS:
		next_token(ctx)
		expr = new_expr_binary(op, expr, parse_expr_mul(ctx))
		op = peek_token(ctx)
	return expr

fun parse_expr_mul (ctx *Parser) *Node :
	var expr *Node = parse_expr_unary(ctx)
	var op Token = peek_token(ctx)
	while op.type == TOK_STAR || op.type == TOK_SLASH || op.type == TOK_PERCENT:
		next_token(ctx)
		expr = new_expr_binary(op, expr, parse_expr_unary(ctx))
		op = peek_token(ctx)
	return expr

fun new_expr_binary (op Token, left *Node, right *Node) *Node :
	var expr *Node = new_node(AST_EXPR_BINARY)
	expr.data = op
	add_node(expr, left)
	add_node(expr, right)
	return expr

fun parse_expr_unary (ctx *Parser) *Node :
	var t Token = peek_token(ctx)
	var e *Node = nil
	if t.type == TOK_LOGICAL_NOT || t.type == TOK_NOT || t.type == TOK_PLUS || t.type == TOK_MINUS || t.type == TOK_STAR || t.type == TOK_AND:
		e = new_node(AST_EXPR_UNARY)
		e.data = next_token(ctx)
		add_node(e, parse_expr_postfix(ctx))
	else:
		e = parse_expr_postfix(ctx)
	return e

fun match (ctx *Parser, type int) Token :
	var tok Token = next_token(ctx)
	if tok.type != type:
		parser_exit_mismatch(ctx, &tok, "expected '" + token_type_string(type) + "'")
	return tok

fun is_token_not_end (tok Token) bool :
	return tok.type != TOK_EOL && tok.type != TOK_EOF

fun is_next_token_not_end (ctx *Parser) bool :
	return is_token_not_end(peek_token(ctx))

fun is_ast_child (parent Token, child Token) bool :
	return child.type != TOK_EOF && (child.pos.indent == (parent.pos.indent + 1))

fun is_next_token_ast_child (ctx *Parser, parent *Token) bool :
	return is_ast_child(*parent, peek_token(ctx))

fun match_end (ctx *Parser) :
	var next Token = next_token(ctx)
	if is_token_not_end(next):
		parser_exit_mismatch(ctx, &next, "expected end-of-line or end-of-file")

fun is_type_token (type int) bool:
	return type == TOK_BOOL || type == TOK_INT || type == TOK_FLOAT || type == TOK_STRING || type == TOK_FILE || type == TOK_ID

fun match_type (ctx *Parser) Token :
	var tok Token = next_token(ctx)
	if !is_type_token(tok.type):
		parser_exit_mismatch(ctx, &tok, "expected type")
	return tok

fun next_token (ctx *Parser) Token :
	var tok Token = scanner_next_token(&ctx.scanner)
	if ctx.scanner.is_error:
		parser_exit_error(ctx, ctx.scanner.error_reason, ctx.scanner.pos)
	return tok

fun peek_token (ctx *Parser) Token :
	var tok Token = scanner_peek_token(&ctx.scanner)
	if ctx.scanner.is_error:
		parser_exit_error(ctx, ctx.scanner.error_reason, ctx.scanner.pos)
	return tok
