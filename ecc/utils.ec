
fun public fopen_ec (path string, mode string) file :
	return c::fopen(c_str(path), c_str(mode))

fun public fclose_ec (f file) bool :
	var success bool = true
	if f != nil:
		success = (c::fclose(f) != eof)
	return success

fun public fwrite_ec (f file, text string) bool :
	assert(f != nil, "file handle is not null")
	var written int = c::fwrite(c_str(text), 1, len(text), f)
	return written == len(text)

fun public file_is_valid (f file) bool :
	return f != nil && c::ferror(f) == 0

fun public find_file_name_no_ext_from_path (path string) string :
	# FIXME this function is very simplistic and will break for lots of input
	# stupid approach: find last slash and copy till we reach end or a dot

	var slash int = to_char('/') 
	if get_platform() == "win32":
		slash = to_char("\\")

	var name string
	var start int = 0
	for i in 0, len(path):
		if path[i] == slash:
			start = i
	for i in start + 1, len(path):
		if path[i] == to_char('.'):
			break
		name += path[i]
	return name

fun public get_module_name_from_file_name (file_name string) string :
	# the file name could be a reserved word in C++, so need to ensure that the module name
	# is valid for use in "namespace X {" and "X::foo()" contexts.
	return '_' + file_name

