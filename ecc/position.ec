struct public Position:
	line int = 0
	col int = -1
	indent int = 0

fun public pos_to_string (pos *Position) string :
	return "line=" + to_string(pos.line + 1) + " col=" + to_string(pos.col + 1)
