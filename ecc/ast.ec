use token::*
use utils::*

struct public NodeList:
	count int = 0
	first *Node = nil
	last  *Node = nil

struct public Node:
	type int = UNRECOGNIZED
	flags int = 0
	parent *Node = nil
	next *Node = nil
	prev *Node = nil
	children NodeList
	data Token

const public int:
	AST_PROG
	AST_USE
	AST_STRUCT
	AST_CONST
	AST_CONST_LIST
	AST_FUN
	AST_FUN_TYPE
	AST_PARAM
	AST_PARAM_TYPE
	AST_GENERIC_PARAMS
	AST_GENERIC_PARAM
	AST_BLOCK
	AST_WHILE
	AST_FOR
	AST_IF
	AST_CALL
	AST_CALL_MODULE
	AST_VAR
	AST_VAR_TYPE
	AST_RETURN
	AST_BREAK
	AST_CONTINUE
	AST_EXPR_ASSIGN
	AST_EXPR_BINARY
	AST_EXPR_UNARY
	AST_EXPR_CONST
	AST_EXPR_ID
	AST_EXPR_ARRAY
	AST_EXPR_DOT
	AST_ARRAY_LITERAL

fun public ast_type_string (t int) string :
	if t == AST_PROG              : return "ast_prog"
	if t == AST_USE               : return "ast_use"
	if t == AST_STRUCT            : return "ast_struct"
	if t == AST_CONST             : return "ast_const"
	if t == AST_CONST_LIST        : return "ast_const_list"
	if t == AST_FUN               : return "ast_fun"
	if t == AST_FUN_TYPE          : return "ast_fun_type"
	if t == AST_PARAM             : return "ast_param"
	if t == AST_PARAM_TYPE        : return "ast_param_type"
	if t == AST_GENERIC_PARAMS    : return "ast_generic_params"
	if t == AST_GENERIC_PARAM     : return "ast_generic_param"
	if t == AST_BLOCK             : return "ast_block"
	if t == AST_WHILE             : return "ast_while"
	if t == AST_FOR               : return "ast_for"
	if t == AST_IF                : return "ast_if"
	if t == AST_CALL_MODULE       : return "ast_call_module"
	if t == AST_CALL              : return "ast_call"
	if t == AST_VAR               : return "ast_var"
	if t == AST_VAR_TYPE          : return "ast_var_type"
	if t == AST_RETURN            : return "ast_return"
	if t == AST_BREAK             : return "ast_break"
	if t == AST_CONTINUE          : return "ast_continue"
	if t == AST_EXPR_ASSIGN       : return "ast_expr_assign"
	if t == AST_EXPR_BINARY       : return "ast_expr_binary"
	if t == AST_EXPR_UNARY        : return "ast_expr_unary"
	if t == AST_EXPR_CONST        : return "ast_expr_const"
	if t == AST_EXPR_ID           : return "ast_expr_id"
	if t == AST_EXPR_ARRAY        : return "ast_expr_array"
	if t == AST_EXPR_DOT          : return "ast_expr_dot"
	if t == AST_ARRAY_LITERAL     : return "ast_array_literal"
	return "UNKNOWN"

const public int:
	NODE_FLAG_POINTER
	NODE_FLAG_ARRAY
	NODE_FLAG_ARRAY_OF_POINTERS
	NODE_FLAG_OUT
	NODE_FLAG_ELSE
	NODE_FLAG_GENERATED
	NODE_FLAG_EXPR_ARRAY_DIM
	NODE_FLAG_EXPR_INIT
	NODE_FLAG_PUBLIC
	NODE_FLAG_IMPORTED
	NODE_FLAG_GENERIC           # generic type, i.e. the type is not fully known in the function or type definition
	NODE_FLAG_SPECIFIC          # specific type, i.e. not a generic

fun public new_node (type int) *Node :
	var node *Node = new(Node)
	node.type = type
	node.data.type = TOK_IGNORE
	return node

fun public add_node (parent *Node, child *Node) *Node :
	var list * NodeList = &parent.children
	if list.first == nil:
		list.first = child
		list.last = child
	elif list.last == nil:
		list.last = child
		list.first.next = child
		child.prev = list.first
	else:
		child.prev = list.last
		list.last.next = child
		list.last = child
	child.parent = parent
	list.count += 1
	return child

fun public add_new_node (parent *Node, child_type int, data Token) *Node :
	var child *Node = new_node(child_type)
	child.data = data
	return add_node(parent, child)

fun public is_node_flag (node *Node, flag int) bool :
	assert(node != nil, "node is not nil")
	var pow2 int = (1 << flag)
	return (node.flags & pow2) != 0

fun public set_node_flag (node *Node, flag int) :
	assert(node != nil, "node is not nil")
	node.flags = set_flag(node.flags, flag)

fun public reset_node_flag (node *Node, flag int) :
	assert(node != nil, "node is not nil")
	var mask int = ~(1 << flag)
	node.flags &= mask

fun public set_flag (bits int, bit int) int :
	return bits | (1 << bit)

fun public find_child_of_type (parent *Node, child_type int) *Node :
	var child *Node = parent.children.first
	while child != nil:
		if child.type == child_type:
			return child
		child = child.next
	return nil

fun public find_child_for_id (parent *Node, child_name string) *Node :
	assert(parent != nil, "parent is not null")
	var child *Node = parent.children.first
	while child != nil:
		if child.data.text == child_name:
			return child
		child = child.next
	return nil

fun public print_ast_json (f file, node *Node, level int) :
	# NOTE: this json output is actually more verbose than the xml output...
	print_indent(f, level)
	fwrite_ec(f, "{\"")
	fwrite_ec(f, ast_type_string(node.type))
	fwrite_ec(f, "\": {\n")

	print_indent(f, level+1)
	fwrite_ec(f, "\"token_type\": \"")
	fwrite_ec(f, token_type_string(node.data.type))
	fwrite_ec(f, "\",\n")

	print_indent(f, level+1)
	fwrite_ec(f, "\"token_line\": \"")
	fwrite_ec(f, to_string(node.data.pos.line))
	fwrite_ec(f, "\",\n")

	print_indent(f, level+1)
	fwrite_ec(f, "\"token_col\": \"")
	fwrite_ec(f, to_string(node.data.pos.col))
	fwrite_ec(f, "\",\n")

	print_indent(f, level+1)
	fwrite_ec(f, "\"children\": [\n")

	var child *Node = node.children.first
	while child != nil:
		print_ast_json(f, child, level+2)
		fwrite_ec(f, ",\n")
		child = child.next

	print_indent(f, level+2)
	fwrite_ec(f, "null\n")
	print_indent(f, level+1)
	fwrite_ec(f, "]\n")

	print_indent(f, level)
	fwrite_ec(f, "}}")

fun public print_ast_xml (f file, node *Node, level int) :
	print_indent(f, level)
	fwrite_ec(f, "<")
	fwrite_ec(f, ast_type_string(node.type))

	fwrite_ec(f, " line=\"")
	fwrite_ec(f, to_string(node.data.pos.line))
	fwrite_ec(f, "\"")

	fwrite_ec(f, " token_text=\"")
	fwrite_ec(f, escape_xml(node.data.text))
	fwrite_ec(f, "\">\n")

	print_indent(f, level+1)
	fwrite_ec(f, "<token line=\"")
	fwrite_ec(f, to_string(node.data.pos.line))
	fwrite_ec(f, "\" col=\"")
	fwrite_ec(f, to_string(node.data.pos.col))
	fwrite_ec(f, "\"/>\n")

	var child *Node = node.children.first
	while child != nil:
		print_ast_xml(f, child, level+1)
		child = child.next

	print_indent(f, level)
	fwrite_ec(f, "</")
	fwrite_ec(f, ast_type_string(node.type))
	fwrite_ec(f, ">\n")

fun escape_xml (text string) string :
	var s string
	for i in 0, len(text):
		var c int = text[i]
		if c == to_char("<"):
			s += "&lt;"
		elif c == to_char(">"):
			s += "&gt;"
		elif c == to_char("\""):
			s += "&quot;"
		elif c == to_char("&"):
			s += "&amp;"
		else:
			s += c
	return s

fun print_indent (f file, level int) :
	for i in 0, level:
		fwrite_ec(f, "  ")

fun public destroy_ast (node *Node) :
	var child *Node = node.children.first
	while child != nil:
		var next *Node = child.next
		destroy_ast(child)
		child = next
	delete(node)
