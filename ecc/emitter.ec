use utils::*

struct public Emit:
	indent int = 0
	is_enabled bool = true
	outfile_current file = nil

fun public emit (emit *Emit, s string) :
	assert(emit != nil, "emit is not null")
	if emit.is_enabled:
		fwrite_ec(emit.outfile_current, s)

fun public emit_indent (emit *Emit) :
	assert(emit != nil, "emit is not null")
	if emit.is_enabled:
		assert(file_is_valid(emit.outfile_current), "file is valid")
		for i in 0, emit.indent:
			fwrite_ec(emit.outfile_current, "\t")

fun public emit_line (emit *Emit, s string) :
	assert(emit != nil, "emit is not null")
	if emit.is_enabled:
		assert(file_is_valid(emit.outfile_current), "file is valid")
		if len(s) != 0:
			emit_indent(emit)
			fwrite_ec(emit.outfile_current, s)
		fwrite_ec(emit.outfile_current, "\n")

fun public emit_inc_indent (emit *Emit) :
	assert(emit != nil, "emit is not null")
	emit.indent += 1

fun public emit_dec_indent (emit *Emit) :
	assert(emit != nil, "emit is not null")
	emit.indent -= 1
	assert(emit.indent >= 0, "indent is not negative")

fun public emit_set_outfile (emit *Emit, f file) :
	assert(emit != nil, "emit is not null")
	emit.outfile_current = f

fun public emit_get_outfile (emit *Emit) file :
	assert(emit != nil, "emit is not null")
	return emit.outfile_current

fun public emit_enable (emit *Emit) :
	assert(emit != nil, "emit is not null")
	emit.is_enabled = true

fun public emit_disable (emit *Emit) :
	assert(emit != nil, "emit is not null")
	emit.is_enabled = false
