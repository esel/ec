use scanner::*
use emitter::*
use utils::*
use token::*
use position::*
use ast::*
use log::*
use modules::*

struct public Generator:
	log Log
	ast *Node = nil
	emit Emit
	current_fun_or_struct *Node = nil
	modules *Modules
	module_name string
	infile_name_no_ext string
	outfile_impl file = nil
	outfile_symbols file = nil

struct Eval:
	decl *Node = nil

struct Symbol:
	node *Node = nil
	module_name string
	is_imported bool = false

fun public generate_code (ctx *Generator) :
	var ast *Node = ctx.ast
	assert(ast != nil, "ast is not null")
	generate_module_symbols(ctx, ast)
	generate_private_includes(ctx, ast)
	generate_namespace_start(ctx)
	generate_constants(ctx, ast, false)
	generate_structure_prototypes(ctx, ast, false)
	generate_structures(ctx, ast, false)
	generate_function_prototypes(ctx, ast, false)
	generate_function_definitions(ctx, ast)
	emit_line(&ctx.emit, '} // namespace')
	generate_main(ctx)

fun generator_exit_error (ctx *Generator, msg string, pos Position) :
	println("GENERATOR ERROR: ", msg)
	println("Near ", pos_to_string(&pos))
	c::exit(1)

fun generate_namespace_start (ctx *Generator) :
	emit(&ctx.emit, 'namespace ')
	emit(&ctx.emit, ctx.module_name)
	emit_line(&ctx.emit, ' {')

fun generate_public_includes (ctx *Generator, ast *Node) :
 	# FIXME should only include the headers needed for the public symbols, not those which are only needed for the private implementation
 	emit(&ctx.emit, "// Includes\n")
	emit(&ctx.emit, "#include <ecc_sys.h>\n") # always include the system
	var child *Node = ast.children.first
	while child != nil:
		if child.type == AST_USE:
			emit(&ctx.emit, "#include <")
			emit(&ctx.emit, child.data.text)
			emit(&ctx.emit, ".h>\n")
		child = child.next

fun generate_private_includes (ctx *Generator, ast *Node) :
	emit(&ctx.emit, '#include "')
	emit(&ctx.emit, ctx.infile_name_no_ext)
	emit(&ctx.emit, '.h"\n')

fun generate_constants (ctx *Generator, ast *Node, is_public bool) :
	emit(&ctx.emit, '// Constants\n')
	var child *Node = ast.children.first
	while child != nil:
		if is_node_flag(child, NODE_FLAG_PUBLIC) == is_public:
			if child.type == AST_CONST:
				generate_constant(ctx, child, 0)
			elif child.type == AST_CONST_LIST:
				var index int = 0
				var c *Node = child.children.first
				while c != nil:
					generate_constant(ctx, c, index)
					index += 1
					c = c.next
		child = child.next

fun generate_constant (ctx *Generator, c *Node, index int) :
	emit(&ctx.emit, 'static const ')
	emit(&ctx.emit, ec_type_to_c_type(ctx, c.children.first))
	emit(&ctx.emit, ' ')
	emit(&ctx.emit, c.data.text)
	emit(&ctx.emit, ' = ')
	if c.children.count == 2:
		generate_expr(ctx, c.children.last)
	else:
		emit(&ctx.emit, to_string(index))
	emit(&ctx.emit, ';\n')

fun generate_module_symbols (ctx *Generator, ast *Node) :
	var prev_outfile file = emit_get_outfile(&ctx.emit)
	emit_set_outfile(&ctx.emit, ctx.outfile_symbols)
	emit_line(&ctx.emit, "#pragma once")
	generate_public_includes(ctx, ast)
	generate_namespace_start(ctx)
	generate_constants(ctx, ast, true)
	generate_structure_prototypes(ctx, ast, true)
	generate_structures(ctx, ast, true)
	generate_function_prototypes(ctx, ast, true)
	# C++ requires template function declaration and definition to be in the same file
	emit(&ctx.emit, '// Function definitions (public and generic)\n')
	generate_functions(ctx, ast, true, true)
	emit_line(&ctx.emit, '} // namespace')
	emit_set_outfile(&ctx.emit, prev_outfile)

fun generate_structure_prototypes (ctx *Generator, ast *Node, is_public bool) :
	emit(&ctx.emit, '// Structure Prototypes\n')
	var child *Node = ast.children.first
	while child != nil:
		if child.type == AST_STRUCT:
			if is_node_flag(child, NODE_FLAG_PUBLIC) == is_public:
				if is_node_flag(child, NODE_FLAG_GENERIC):
					generate_template(ctx, child)
				emit(&ctx.emit, 'struct ' + child.data.text + ';\n')
		child = child.next

fun generate_template (ctx *Generator, declaration *Node) :
	emit(&ctx.emit, 'template <')
	var generic_types *Node = find_child_of_type(declaration, AST_GENERIC_PARAMS)
	assert(generic_types != nil, "generic parameter list is not nil")
	var generic_child *Node = generic_types.children.first
	while generic_child != nil:
		assert(generic_child.type == AST_GENERIC_PARAM, "node is a generic type name")
		emit(&ctx.emit, 'class ')
		emit(&ctx.emit, generic_child.data.text)
		generic_child = generic_child.next
		if generic_child != nil:
			emit(&ctx.emit, ',')
	emit(&ctx.emit, '> ')

fun generate_function_prototypes (ctx *Generator, ast *Node, is_public bool) :
	emit(&ctx.emit, "// Function prototypes\n")
	var child *Node = ast.children.first
	while child != nil:
		if child.type == AST_FUN:
			if is_node_flag(child, NODE_FLAG_PUBLIC) == is_public:
				generate_function_prototype(ctx, child)
		child = child.next

fun generate_structures (ctx *Generator, ast *Node, is_public bool) :
	emit(&ctx.emit, '// Structures\n')
	reset_generated_flag_on_structures(ctx, ast)
	var generated bool = true
	while generated:
		generated = false
		var child *Node = ast.children.first
		while child != nil:
			if child.type == AST_STRUCT && (is_node_flag(child, NODE_FLAG_PUBLIC) == is_public):
				if !is_node_flag(child, NODE_FLAG_GENERATED) && all_members_defined(ctx, ast, child):
					ctx.current_fun_or_struct = child
					generate_structure(ctx, child)
					ctx.current_fun_or_struct = nil
					generated = true
					set_node_flag(child, NODE_FLAG_GENERATED)
			child = child.next

fun reset_generated_flag_on_structures (ctx *Generator, ast *Node) :
	var child *Node = ast.children.first
	while child != nil:
		if child.type == AST_STRUCT:
			reset_node_flag(child, NODE_FLAG_GENERATED)
		child = child.next

fun all_members_defined (ctx *Generator, ast *Node, struct_node *Node) bool :
	var member_node *Node = struct_node.children.first
	while member_node != nil:
		var typenode *Node = member_node.children.first
		var typetok *Token = &typenode.data
		if member_node.type == AST_VAR && typetok.type == TOK_ID && !is_node_flag(typenode, NODE_FLAG_POINTER) && !is_member_generic(ctx, struct_node, member_node):
			var membertype Symbol = get_struct(ctx, typetok.text)
			if membertype.node == nil:
				generator_exit_error(ctx, "undefined structure member type " + typetok.text, typetok.pos )
			if !is_node_flag(membertype.node, NODE_FLAG_GENERATED) && !is_node_flag(membertype.node, NODE_FLAG_IMPORTED):
				return false
		member_node = member_node.next
	return true

fun is_member_generic (ctx *Generator, struct_node *Node, member_node *Node) bool :
	assert(struct_node.type == AST_STRUCT, "struct node is a structure definition")
	assert(member_node.type == AST_VAR, "member node is a variable definition")
	return is_type_generic(struct_node, member_node, member_node.children.first, nil)

fun is_type_generic (parent_node *Node, child_node *Node, child_type *Node, generic_types *Node) bool :
	if is_node_flag(child_node, NODE_FLAG_GENERIC):
		return true
	if is_node_flag(child_node, NODE_FLAG_SPECIFIC):
		return false
	if child_type.data.type == TOK_ID:  # generic types are identifiers
		if generic_types == nil:
			generic_types = find_child_of_type(parent_node, AST_GENERIC_PARAMS)
		if generic_types != nil:
			var generic_type *Node = generic_types.children.first
			while generic_type != nil:
				if generic_type.data.text == child_type.data.text:
					set_node_flag(child_node, NODE_FLAG_GENERIC)
					return true
				generic_type = generic_type.next
	set_node_flag(child_node, NODE_FLAG_SPECIFIC)
	return false

fun get_node_in_ast (ast *Node, name string, container_type int, type int) *Node :
	assert(ast != nil, "not null")
	var child *Node = ast.children.first
	while child != nil:
		if (type == -1 || child.type == type) && child.data.text == name:
			return child
		if child.type == container_type:
			var n *Node = get_node_in_ast(child, name, type, type)
			if n != nil:
				return n
		child = child.next
	return nil

fun get_node_for_name (ctx *Generator, symbol_name string, container_type int, symbol_type int) Symbol :
	var symbol Symbol
	symbol.node = get_node_in_ast(ctx.ast, symbol_name, container_type, symbol_type)
	if symbol.node != nil:
		symbol.module_name = ctx.module_name
		return symbol
	log_start_debug(&ctx.log, "NON-LOCAL symbol used, searching in imported modules.")
	log_add_debug(&ctx.log, " Name=")
	log_add_debug(&ctx.log, symbol_name)
	log_add_debug(&ctx.log, " Type=")
	log_end_debug(&ctx.log, ast_type_string(symbol_type))
	for i in 0, ctx.modules.n_used_modules:
		var module *Module = &ctx.modules.used_modules[i]
		symbol.node = get_node_in_ast(module.ast, symbol_name, container_type, symbol_type)
		if symbol.node != nil:
			log_start_debug(&ctx.log, "FOUND SYMBOL in module: ")
			log_end_debug(&ctx.log, module.name)
			symbol.module_name = module.name
			symbol.is_imported = true
			break
	if symbol.node == nil:
		log_debug(&ctx.log, "WARNING: could not find symbol in any module")
	return symbol

fun get_struct (ctx *Generator, type_name string) Symbol :
	return get_node_for_name(ctx, type_name, AST_STRUCT, AST_STRUCT)

fun get_fun (ctx *Generator, fun_name string) Symbol :
	return get_node_for_name(ctx, fun_name, AST_FUN, AST_FUN)

fun get_const (ctx *Generator, const_name string) Symbol :
	return get_node_for_name(ctx, const_name, AST_CONST_LIST, AST_CONST)

fun generate_structure (ctx *Generator, child *Node) :
	if is_node_flag(child, NODE_FLAG_GENERIC):
		generate_template(ctx, child)
	emit(&ctx.emit, 'struct ')
	emit(&ctx.emit, child.data.text)
	emit(&ctx.emit, '\n{\n')
	emit_inc_indent(&ctx.emit)
	var member *Node = child.children.first
	while member != nil:
		if member.type == AST_VAR:
			emit_indent(&ctx.emit)
			generate_var(ctx, member)
		member = member.next
	emit_line(&ctx.emit, child.data.text + '()')
	emit_line(&ctx.emit, '{')
	emit_inc_indent(&ctx.emit)
	member = child.children.first
	while member != nil:
		if member.type == AST_VAR:
			if member.children.count > 1 && is_node_flag(member.children.last, NODE_FLAG_EXPR_INIT):
				emit_indent(&ctx.emit)
				emit(&ctx.emit, member.data.text + ' = ')
				generate_expr(ctx, member.children.last)
				emit(&ctx.emit, ';\n')
		member = member.next
	emit_dec_indent(&ctx.emit)
	emit_line(&ctx.emit, '}')
	emit_dec_indent(&ctx.emit)
	emit(&ctx.emit, '};\n')

fun generate_main (ctx *Generator) :
	var mainfun *Node = get_node_in_ast(ctx.ast, 'main', AST_FUN, AST_FUN)
	if mainfun != nil:
		emit(&ctx.emit, '#define ECC_MAIN_FUNCTION ')
		emit(&ctx.emit, ctx.module_name)
		emit(&ctx.emit, '::ecc_main\n')
		if param_count(mainfun) == 0:
			emit_line(&ctx.emit, '#include <ecc_main_void.h>')
		elif param_count(mainfun) == 2:
			emit_line(&ctx.emit, '#include <ecc_main_args.h>')
		else:
			generator_exit_error(ctx, 'incorrect signature for main function', mainfun.data.pos)

fun param_count (func *Node) int :
	return child_count(func, AST_PARAM)

fun child_count (root *Node, child_type int) int :
	var count int = 0
	var child *Node = root.children.first
	while child != nil:
		if child.type == child_type:
			count += 1
		child = child.next
	return count

fun generate_functions (ctx *Generator, ast *Node, is_generic bool, is_public bool) :
	var child *Node = ast.children.first
	while child != nil:
		if child.type == AST_FUN && (is_node_flag(child, NODE_FLAG_GENERIC) == is_generic) && (is_node_flag(child, NODE_FLAG_PUBLIC) == is_public):
			ctx.current_fun_or_struct = child
			generate_function_header(ctx, child)
			emit(&ctx.emit, '\n')
			generate_block(ctx, find_child_of_type(child, AST_BLOCK))
			ctx.current_fun_or_struct = nil
		child = child.next

fun generate_function_definitions (ctx *Generator, ast *Node) :
	emit(&ctx.emit, '// Function definitions (private generic and all non-generic)\n')
	generate_functions(ctx, ast, false, true)
	generate_functions(ctx, ast, false, false)
	generate_functions(ctx, ast, true, false)

fun generate_block (ctx *Generator, block *Node) :
	assert(block.type == AST_BLOCK, "block type")
	emit_line(&ctx.emit, '{')
	emit_inc_indent(&ctx.emit)
	var child *Node = block.children.first
	while child != nil:
		emit_indent(&ctx.emit)
		if child.type == AST_RETURN:
			generate_return(ctx, child)
		elif child.type == AST_BREAK:
			emit(&ctx.emit, 'break;\n')
		elif child.type == AST_CONTINUE:
			emit(&ctx.emit, 'continue;\n')
		elif child.type == AST_IF:
			generate_if(ctx, child)
		elif child.type == AST_WHILE:
			generate_while(ctx, child)
		elif child.type == AST_FOR:
			generate_for(ctx, child)
		elif child.type == AST_VAR:
			generate_var(ctx, child)
		else:
			generate_expr(ctx, child)
			emit(&ctx.emit, ';\n')
		child = child.next
	emit_dec_indent(&ctx.emit)
	emit_line(&ctx.emit, "}")

fun is_user_type (type_node *Node) bool :
	return type_node.data.type == TOK_ID

fun generate_return (ctx *Generator, n *Node) :
	emit(&ctx.emit, 'return ')
	if n.children.count != 0:
		assert(n.children.count == 1, "return statement takes one expression")
		generate_expr(ctx, n.children.first)
	emit(&ctx.emit, ';\n')

fun generate_if (ctx *Generator, n *Node) :
	emit(&ctx.emit, 'if ( ')
	var child *Node = n.children.first
	generate_expr(ctx, child)
	emit(&ctx.emit, ' )\n')
	generate_block(ctx, child.next)
	child = child.next.next
	while child != nil && !is_node_flag(child, NODE_FLAG_ELSE):
		emit_indent(&ctx.emit)
		emit(&ctx.emit, 'else if ( ')
		generate_expr(ctx, child)
		emit(&ctx.emit, ' )\n')
		generate_block(ctx, child.next)
		child = child.next.next
	if child != nil && is_node_flag(child, NODE_FLAG_ELSE):
		emit_line(&ctx.emit, 'else')
		generate_block(ctx, child)
		child = child.next

fun generate_while (ctx *Generator, n *Node) :
	assert(n.children.count == 2, "while statement takes one expression and one body")
	emit(&ctx.emit, 'while ( ')
	generate_expr(ctx, n.children.first)
	emit(&ctx.emit, ' )\n')
	generate_block(ctx, n.children.last)

fun generate_for (ctx *Generator, n *Node) :
	assert(n.children.count == 4, "for statement takes one id, two expressions and one body")
	var decl *Node = n.children.first
	assert(decl.type == AST_VAR, "first child is the declaration of the loop counter variable")
	var decl_type *Node = decl.children.first
	assert(decl_type.type == AST_VAR_TYPE, "loop counter type")
	emit(&ctx.emit, 'for ( ')
	emit(&ctx.emit, ec_type_to_c_type_token(ctx, &decl_type.data))
	emit(&ctx.emit, ' ')
	emit(&ctx.emit, decl.data.text)
	emit(&ctx.emit, ' = ')
	var start *Node = decl.next
	generate_expr(ctx, start)
	emit(&ctx.emit, '; ')
	emit(&ctx.emit, decl.data.text)
	emit(&ctx.emit, ' < ')
	var end *Node = start.next
	generate_expr(ctx, end)
	emit(&ctx.emit, '; ++')
	emit(&ctx.emit, decl.data.text)
	emit(&ctx.emit, ' )\n')
	generate_block(ctx, n.children.last)

fun generate_expr (ctx *Generator, expr *Node) :
	generate_expr0(ctx, expr, nil)

fun generate_expr0 (ctx *Generator, expr *Node, eval *Eval) :
	if expr.type == AST_EXPR_CONST:
		if expr.data.type == TOK_LITERAL_STRING:
			generate_string_literal(ctx, expr)
		elif expr.data.type == TOK_NIL:
			emit(&ctx.emit, 'NULL')
		elif expr.data.type == TOK_TRUE:
			emit(&ctx.emit, 'true')
		elif expr.data.type == TOK_FALSE:
			emit(&ctx.emit, 'false')
		else:
			emit(&ctx.emit, expr.data.text)
	elif expr.type == AST_EXPR_ID:
		if expr.data.text == 'eof':
			emit(&ctx.emit, 'ecc_eof') # FIXME this should not be here (const expr like nil/true/false?)
		else:
			var decl Symbol
			decl.node = find_var_for_id(ctx, expr) # FIXME should also return const declarations
			if decl.node == nil:
				# could not find a variable declaration in this module for the id, so search for constants
				decl = get_const(ctx, expr.data.text)
			if decl.node == nil:
				# could not find a variable or constant declaration in this module for the id, so search for functions
				decl = get_fun(ctx, expr.data.text)
			if decl.node == nil:
				generator_exit_error(ctx, "undeclared identifier " + expr.data.text, expr.data.pos)
			if decl.is_imported:
				emit(&ctx.emit, decl.module_name)
				emit(&ctx.emit, '::')
			emit(&ctx.emit, expr.data.text)
			if eval != nil:
				eval.decl = decl.node
	elif expr.type == AST_EXPR_ASSIGN:
		generate_expr0(ctx, expr.children.first, eval)
		emit(&ctx.emit, ' ' + token_type_string(expr.data.type) + ' ')
		generate_expr0(ctx, expr.children.last, eval)
	elif expr.type == AST_EXPR_BINARY:
		emit(&ctx.emit, '(')
		generate_expr0(ctx, expr.children.first, eval)
		emit(&ctx.emit, ' ' + token_type_string(expr.data.type) + ' ')
		generate_expr0(ctx, expr.children.last, eval)
		emit(&ctx.emit, ')')
	elif expr.type == AST_EXPR_UNARY:
		emit(&ctx.emit, '(')
		emit(&ctx.emit, token_type_string(expr.data.type))
		generate_expr0(ctx, expr.children.first, eval)
		emit(&ctx.emit, ')')
	elif expr.type == AST_EXPR_ARRAY:
		generate_array_expr(ctx, expr, eval)
	elif expr.type == AST_EXPR_DOT:
		generate_dot_expr(ctx, expr, eval)
	elif expr.type == AST_CALL_MODULE:
		# NOTE: _always_ explicitly specify namespace:
		# - toplevel :: for libc and builtin ecc functions
		# - mymodule::foo() for normal functions
		var module *string = &expr.children.first.data.text
		if *module != "c":
			# special case the "c" module to mean the libc toplevel functions
			# so for the normal cases, simply output the module name for the namespace
			emit(&ctx.emit, get_module_name_from_file_name(expr.children.first.data.text))
		emit(&ctx.emit, '::')
		generate_call_user(ctx, expr.children.last, eval)
	else:
		assert(expr.type == AST_CALL, ast_type_string(expr.type))
		generate_call(ctx, expr, eval)

fun generate_array_expr (ctx *Generator, expr *Node, eval *Eval) :
	assert(expr.type == AST_EXPR_ARRAY, ast_type_string(expr.type))
	generate_expr0(ctx, expr.children.first, eval)
	emit(&ctx.emit, '[')
	generate_expr0(ctx, expr.children.last, eval)
	emit(&ctx.emit, ']')

fun generate_dot_expr (ctx *Generator, expr *Node, eval_return *Eval) :
	assert(expr != nil, "expression is not nil")
	assert(expr.type == AST_EXPR_DOT, ast_type_string(expr.type))
	var eval Eval
	generate_expr0(ctx, expr.children.first, &eval)
	assert(eval.decl != nil, "type of left side of dot expression is known")
	if is_node_flag(eval.decl, NODE_FLAG_POINTER):
		emit(&ctx.emit, '->')
	else:
		emit(&ctx.emit, '.')
	emit(&ctx.emit, expr.children.last.data.text)
	if eval_return != nil:
		var type_name string = eval.decl.data.text
		var typedecl Symbol = get_struct(ctx, type_name)
		if typedecl.node == nil:
			generator_exit_error(ctx, "undefined type " + type_name, eval.decl.data.pos)
		var member_name string = expr.children.last.data.text
		var memberdecl *Node = find_child_for_id(typedecl.node, member_name)
		assert(memberdecl != nil, member_name)
		eval_return.decl = memberdecl.children.first

fun find_var_for_id (ctx *Generator, id *Node) *Node :
	# traverse siblings backwards and then parents upwards to find the variable declaration for id
	# NOTE: id identifies the variable declaration, so cannot be a compound type member
	assert(id != nil, "expression is not nil")
	var name string = id.data.text
	var node *Node = id.prev
	var parent *Node = id.parent
	while true:
		while node != nil:
			if (node.type == AST_VAR || node.type == AST_PARAM) && node.data.text == name:
				return node.children.first
			node = node.prev
		node = parent
		if node == nil:
			return nil
		parent = node.parent

fun generate_string_literal (ctx *Generator, expr *Node) :
	var length int = len(expr.data.text)
	emit(&ctx.emit, '"')
	for i in 0, length:
		var c int = expr.data.text[i]
		if c == to_char('\n'):
			emit(&ctx.emit, '\\n')
		elif c == to_char('\r'):
			emit(&ctx.emit, '\\r')
		elif c == to_char('\t'):
			emit(&ctx.emit, '\\t')
		elif c == to_char('\\'):
			emit(&ctx.emit, '\\\\')
		elif c == to_char('"'):
			emit(&ctx.emit, '\\"')
		else:
			emit(&ctx.emit, to_char(c))
	emit(&ctx.emit, '"')

fun generate_call (ctx *Generator, expr *Node, eval *Eval) :
	if expr.data.text == "print":
		generate_call_print(ctx, expr, eval)
	elif expr.data.text == "println":
		generate_call_println(ctx, expr, eval)
	elif expr.data.text == "new":
		generate_call_new(ctx, expr, eval)
	elif expr.data.text == "delete":
		generate_call_delete(ctx, expr, eval)
	elif is_builtin_function(expr.data.text):
		generate_call_builtin(ctx, expr, eval)
	else:
		var fundecl Symbol = get_fun(ctx, expr.data.text)
		if fundecl.node == nil:
			# could not find normal function, see if there is a function pointer in scope
			var fp *Node = find_var_for_id(ctx, expr)
			assert(fp != nil, "function found")
			# TODO this is really not needed for generator purposes, but more a semantic check!
		else:
			emit(&ctx.emit, fundecl.module_name)
			emit(&ctx.emit, '::')
		generate_call_user(ctx, expr, eval)
		if eval != nil:
			eval.decl = find_child_of_type(fundecl.node, AST_FUN_TYPE)

fun is_builtin_function (f string) bool :
	return  f == "assert" || f == "to_string" || f == "to_char" || f == "to_int" || f == "to_float" || f == "len" || f == "sizeof" || f == "c_str" || f == "get_platform"

fun generate_call_user (ctx *Generator, expr *Node, eval *Eval) :
	emit(&ctx.emit, expr.data.text)
	var generic_params *Node = find_child_of_type(expr, AST_GENERIC_PARAMS)
	if generic_params != nil && generic_params.children.count != 0:
		emit(&ctx.emit, '<')
		var param *Node = generic_params.children.first
		while param != nil:
			generate_param_type (ctx, expr, param, param, generic_params)
			param = param.next
			if param != nil:
				emit(&ctx.emit, ',')
		emit(&ctx.emit, '>')
	emit(&ctx.emit, "(")
	var child *Node = expr.children.first
	while child != nil:
		if child.type != AST_GENERIC_PARAMS:
			generate_expr0(ctx, child, eval)
			if child.next != nil:
				emit(&ctx.emit, ", ")
		child = child.next
	emit(&ctx.emit, ")")

fun generate_call_builtin (ctx *Generator, expr *Node, eval *Eval) :
	emit(&ctx.emit, 'ecc_' + expr.data.text)
	emit(&ctx.emit, "(")
	var child *Node = expr.children.first
	if child != nil:
		generate_expr0(ctx, child, eval)
		child = child.next
	while child != nil:
		emit(&ctx.emit, ", ")
		generate_expr0(ctx, child, eval)
		child = child.next
	emit(&ctx.emit, ")")

fun generate_call_new (ctx *Generator, expr *Node, eval *Eval) :
	emit(&ctx.emit, 'new(')
	if expr.children.count == 1:
		assert(expr.children.first.type == AST_EXPR_ID, "first parameter is a type id")
		emit(&ctx.emit, ec_type_to_c_type(ctx, expr.children.last))
	elif expr.children.count == 2:
		# new(array_size, Type) -> new Type[array_size]
		assert(expr.children.last.type == AST_EXPR_ID, "second parameter is a type id")
		emit(&ctx.emit, ec_type_to_c_type(ctx, expr.children.last))
		emit(&ctx.emit, '[')
		generate_expr0(ctx, expr.children.first, eval)
		emit(&ctx.emit, ']')
	else:
		assert(false, "new() takes one or two parameters")
	emit(&ctx.emit, ')')

fun generate_call_delete (ctx *Generator, expr *Node, eval_return *Eval) :
	assert(expr.children.count == 1, "delete() takes one parameter")

	# evaluate the expression to get the type
	var eval Eval
	emit_disable(&ctx.emit)
	generate_expr0(ctx, expr.children.first, &eval)
	emit_enable(&ctx.emit)
	assert(eval.decl != nil, "declaration of pointer to delete is found")

	emit(&ctx.emit, 'delete')
	if is_node_flag(eval.decl, NODE_FLAG_ARRAY):
		emit(&ctx.emit, '[]')
	emit(&ctx.emit, '(')
	generate_expr0(ctx, expr.children.first, eval_return)
	emit(&ctx.emit, ')')

fun generate_call_println (ctx *Generator, expr *Node, eval *Eval) :
	generate_call_print(ctx, expr, eval)
	emit(&ctx.emit, ' << std::endl')

fun generate_call_print (ctx *Generator, expr *Node, eval *Eval) :
	var child *Node = expr.children.first
	emit(&ctx.emit, 'std::cout')
	while child != nil:
		emit(&ctx.emit, ' << ')
		generate_expr0(ctx, child, eval)
		child = child.next

fun generate_function_prototype (ctx *Generator, node *Node) :
	generate_function_header(ctx, node)
	emit(&ctx.emit, ";\n")

fun generate_function_header (ctx *Generator, node *Node) :
	if is_node_flag(node, NODE_FLAG_GENERIC):
		generate_template(ctx, node)
	if !is_node_flag(node, NODE_FLAG_PUBLIC):
		emit(&ctx.emit, "static ")
	var generic_types *Node = node.children.first
	assert(generic_types.type == AST_GENERIC_PARAMS, "first function child is type parameter list")
	var fun_type *Node = find_child_of_type(node, AST_FUN_TYPE)
	if fun_type != nil:
		if is_function_pointer(fun_type):
			# FIXME this is incorrect. Syntax: http://www.newty.de/fpt/fpt.html#r_value
			generate_function_pointer(ctx, node, fun_type, generic_types)
		else:
			generate_param_type(ctx, node, node, fun_type, generic_types)
		emit(&ctx.emit, " ")
	else:
		emit(&ctx.emit, "void ")
	if node.data.text == "main":
		emit(&ctx.emit, 'ecc_main')
	else:
		emit(&ctx.emit, node.data.text)
	generate_param_list(ctx, node, generic_types)

fun generate_param_list (ctx *Generator, node *Node, generic_types *Node) :
	emit(&ctx.emit, "(")
	var n_param int = 0
	var param *Node = find_child_of_type(node, AST_PARAM)
	while param != nil:
		if param.type == AST_PARAM:
			var type_node *Node = param.children.first
			if n_param != 0:
				emit(&ctx.emit, ", ")
			if is_function_pointer(type_node):
				generate_function_pointer(ctx, param, type_node, generic_types)
			else:
				generate_param_type(ctx, node, param, type_node, generic_types)
				emit(&ctx.emit, " ")
				emit(&ctx.emit, param.data.text)
			n_param += 1
		param = param.next
	if n_param == 0:
		emit(&ctx.emit, "void")
	emit(&ctx.emit, ")")

fun generate_function_pointer (ctx *Generator, var_node *Node, type_node *Node, generic_types *Node):
	var fun_type *Node = find_child_of_type(type_node, AST_FUN_TYPE)
	if fun_type != nil:
		emit(&ctx.emit, ec_type_to_c_type_token(ctx, &fun_type.data))
	else:
		emit(&ctx.emit, "void")
	emit(&ctx.emit, " (*")
	emit(&ctx.emit, var_node.data.text)
	emit(&ctx.emit, ")")
	generate_param_list(ctx, type_node, generic_types)

fun is_function_pointer (type_node *Node) bool :
	return type_node.data.type == TOK_FUN

fun generate_var (ctx *Generator, var_node *Node) :
	assert(var_node.type == AST_VAR, "node is a variable declaration")
	var parent_node *Node = ctx.current_fun_or_struct
	assert(parent_node != nil, "current parent node is not nil")
	var type_node *Node = var_node.children.first
	var generic_types *Node = find_child_of_type(parent_node, AST_GENERIC_PARAMS)
	if is_function_pointer(type_node):
		generate_function_pointer(ctx, var_node, type_node, generic_types)
	else:
		generate_type(ctx, parent_node, var_node, type_node, generic_types)
		if is_node_flag(var_node, NODE_FLAG_POINTER) || is_node_flag(var_node, NODE_FLAG_ARRAY_OF_POINTERS):
			emit(&ctx.emit, '* ')
		else:
			emit(&ctx.emit, ' ')
		emit(&ctx.emit, var_node.data.text)
		# FIXME function pointer arrays should also be legal
		if is_node_flag(type_node, NODE_FLAG_ARRAY):
			if !is_node_flag(var_node, NODE_FLAG_POINTER):
				emit(&ctx.emit, '[')
				assert(var_node.children.count > 1, "array declaration has explicit size")
				generate_expr(ctx, var_node.children.last)
				emit(&ctx.emit, ']')
			else:
				assert(var_node.children.count == 1, "array type does not include the dimension")
	emit(&ctx.emit, ';\n')

fun generate_param_type (ctx *Generator, parent_node *Node, decl_node *Node, type_node *Node, generic_types *Node) :
	generate_type(ctx, parent_node, decl_node, type_node, generic_types)
	if is_node_flag(decl_node, NODE_FLAG_ARRAY):
		emit(&ctx.emit, '*')
	if is_node_flag(decl_node, NODE_FLAG_POINTER):
		emit(&ctx.emit, '*')

fun generate_type (ctx *Generator, parent_node *Node, decl_node *Node, type_node *Node, generic_types *Node) :
	assert(parent_node != nil, "parent node is not nil")
	assert(decl_node != nil, "decl node is not nil")
	assert(type_node != nil, "type node is not nil")
	if generic_types != nil && is_type_generic(parent_node, decl_node, type_node, generic_types):
		# emit the generic type parameter name verbatim
		emit(&ctx.emit, type_node.data.text)
	else:
		emit(&ctx.emit, ec_type_to_c_type_token(ctx, &type_node.data))
		var params *Node = find_child_of_type(decl_node, AST_GENERIC_PARAMS)
		if params != nil && params.children.count != 0:
			emit(&ctx.emit, '<')
			var param *Node = params.children.first
			while param != nil:
				if generic_types != nil && find_child_for_id(generic_types, param.data.text) != nil:
					emit(&ctx.emit, param.data.text) # parameter type is generic (i.e. in the function <A,B,C> list)
				else:
					emit(&ctx.emit, ec_type_to_c_type(ctx, param))
				param = param.next
				if param != nil:
					emit(&ctx.emit, ',')
			emit(&ctx.emit, '>')

fun ec_type_to_c_type (ctx *Generator, node *Node) string :
	var str string = ec_type_to_c_type_token(ctx, &node.data)
	if is_node_flag(node, NODE_FLAG_ARRAY):
		str += '*'
	if is_node_flag(node, NODE_FLAG_POINTER):
		str += '*'
	return str

fun ec_type_to_c_type_token (ctx *Generator, tok *Token) string :
	if tok.type == TOK_INT:      return "int"
	if tok.type == TOK_FLOAT:    return "float"
	if tok.type == TOK_BOOL:     return "bool"
	if tok.type == TOK_STRING:   return "ecc_string"
	if tok.type == TOK_FILE:     return "ecc_file"
	assert(tok.type == TOK_ID, token_to_string(tok))
	var sym Symbol = get_struct(ctx, tok.text)
	return sym.module_name + "::" + tok.text
