use utils::*

struct public Log:
	log_file file = nil

fun public log_set_file (log *Log, f file) bool :
	assert(log != nil, "log is not nil")
	log.log_file = f
	return file_is_valid(f)

fun public log_start_debug (log *Log, msg string) :
	assert(log != nil && file_is_valid(log.log_file), "log is not nil and log file is valid")
	fwrite_ec(log.log_file, "[DEBUG] " + msg)

fun public log_add_debug (log *Log, msg string) :
	assert(log != nil && file_is_valid(log.log_file), "log is not nil and log file is valid")
	fwrite_ec(log.log_file, msg)

fun public log_end_debug (log *Log, msg string) :
	assert(log != nil && file_is_valid(log.log_file), "log is not nil and log file is valid")
	fwrite_ec(log.log_file, msg + "\n")

fun public log_debug (log *Log, msg string) :
	log_start_debug(log, msg)
	fwrite_ec(log.log_file, "\n")
