use modules::*
use generator::*
use parser::*
use scanner::*
use emitter::*
use utils::*
use ast::*
use log::*

struct Context:
	log Log
	ast *Node = nil
	is_ast_printed bool = false
	srcpath string
	dstpath string
	dstpath_impl string
	dstpath_symbols string
	infile_path string
	outfile_path string
	outfile_debug file = nil # output of AST
	modules Modules
	parser Parser
	generator Generator

fun main (argc int, argv string[]) :
	if argc < 3:
		println("ERROR: usage: ", argv[0], " <path to EC source file> <path to output file> [search dirs]")
		c::exit(1)

	var ctx Context
	ctx.srcpath = argv[1]
	ctx.dstpath = argv[2]

	println("Opening file: ", ctx.srcpath)

	ctx.dstpath_impl = ctx.dstpath + ".cpp"
	ctx.dstpath_symbols = ctx.dstpath + ".h"
	ctx.infile_path = ctx.srcpath
	ctx.generator.infile_name_no_ext = find_file_name_no_ext_from_path(ctx.srcpath)
	ctx.generator.module_name = get_module_name_from_file_name(ctx.generator.infile_name_no_ext)
	ctx.outfile_path = ctx.dstpath
	ctx.parser.scanner.infile = fopen_ec(ctx.srcpath, "rb")
	if !file_is_valid(ctx.parser.scanner.infile):
		println("ERROR: source file could not be opened: ", ctx.srcpath)
		c::exit(1)

	println("Output file (impl): ", ctx.dstpath_impl)
	ctx.generator.outfile_impl = fopen_ec(ctx.dstpath_impl, "wb")
	emit_set_outfile(&ctx.generator.emit, ctx.generator.outfile_impl)
	if !file_is_valid(ctx.generator.outfile_impl):
		println("ERROR: output file could not be opened: ", ctx.dstpath_impl)
		c::exit(1)

	println("Output file (symbols): ", ctx.dstpath_symbols)
	ctx.generator.outfile_symbols = fopen_ec(ctx.dstpath_symbols, "wb")
	if !file_is_valid(ctx.generator.outfile_symbols):
		println("ERROR: output file could not be opened: ", ctx.dstpath_symbols)
		c::exit(1)

	var dbgpath string = "ast.xml"
	println("AST debug output: ", dbgpath)
	ctx.outfile_debug = fopen_ec(dbgpath, "a")
	if !file_is_valid(ctx.outfile_debug):
		println("ERROR: output file could not be opened: ", dbgpath)
		c::exit(1)

	var logpath string = "log.txt"
	println("Log output: ", logpath)
	if !log_set_file(&ctx.log, fopen_ec(logpath, "a")):
		println("ERROR: log file could not be opened: ", logpath)
		c::exit(1)

	log_debug(&ctx.log, "starting compile of " + ctx.srcpath)

	ctx.modules.n_search_dirs = argc - 3
	for i in 0, ctx.modules.n_search_dirs:
		ctx.modules.search_dirs[i] = argv[3 + i]

	ctx.parser.log = ctx.log
	ctx.generator.log = ctx.log

	ctx.parser.modules = &ctx.modules
	ctx.generator.modules = &ctx.modules

	compile(&ctx)
	println("Done")
	c::exit(0)

fun cleanup (ctx *Context) :
	print_ast(ctx) # always print the debug info, also on error
	if !fclose_ec(ctx.parser.scanner.infile):
		println("ERROR closing input file: ", ctx.infile_path)
	if !fclose_ec(ctx.generator.outfile_impl):
		println("ERROR closing output file (impl): ", ctx.outfile_path)
	if !fclose_ec(ctx.generator.outfile_symbols):
		println("ERROR closing output file (symbols): ", ctx.outfile_path)
	if !fclose_ec(ctx.outfile_debug):
		println("ERROR closing debug output file")
	if !fclose_ec(ctx.log.log_file):
		println("ERROR closing log file")
	for i in 0, ctx.modules.n_used_modules:
		var module *Module = &ctx.modules.used_modules[i]
		destroy_ast(module.ast)
	if ctx.ast != nil:
		destroy_ast(ctx.ast)

fun compile (ctx *Context) :
	ctx.ast = parse_prog(&ctx.parser)
	print_ast(ctx)
	ctx.generator.ast = ctx.ast
	generate_code(&ctx.generator)

fun print_ast (ctx *Context) :
	if !ctx.is_ast_printed:
		print_ast_xml(ctx.outfile_debug, ctx.ast, 0)
		ctx.is_ast_printed = true
