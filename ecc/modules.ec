use ast::*

const public: MAX_USED_MODULES int = 256
const public: MAX_SEARCH_DIRS int = 256

struct public Modules:
	n_used_modules int = 0
	used_modules Module[MAX_USED_MODULES]
	n_search_dirs int = 0
	search_dirs string[MAX_SEARCH_DIRS]

struct public Module:
	ast *Node = nil
	name string
