use position::*

struct public Token:
	type int = UNRECOGNIZED
	int_value int = 0
	float_value float = 0
	pos Position
	text string

fun public token_to_string (tok *Token) string :
	var s string
	s += "line=" + to_string(tok.pos.line + 1)
	s += " col=" + to_string(tok.pos.col + 1)
	s += " type='" + token_type_string(tok.type) + "'"
	if tok.type == TOK_LITERAL_STRING || tok.type == TOK_ID:
		s += " value=" + tok.text
	elif tok.type == TOK_LITERAL_INT:
		s += " value=" + to_string(tok.int_value)
	elif tok.type == TOK_LITERAL_FLOAT:
		s += " value=" + to_string(tok.float_value)
	return s

fun public token_type_string (t int) string :
	if t == UNRECOGNIZED          : return "UNRECOGNIZED"
	if t == TOK_IGNORE            : return "IGNORE"
	if t == TOK_EOF               : return "eof"
	if t == TOK_EOL               : return "eol"
	if t == TOK_COMMENT_EOL       : return "comment"
	if t == TOK_DOT               : return "."
	if t == TOK_COLON             : return ":"
	if t == TOK_MODULE            : return "::"
	if t == TOK_EQ                : return "=="
	if t == TOK_NEQ               : return "!="
	if t == TOK_LT                : return "<"
	if t == TOK_LTEQ              : return "<="
	if t == TOK_GT                : return ">"
	if t == TOK_GTEQ              : return ">="
	if t == TOK_ASSIGN            : return "="
	if t == TOK_ASSIGN_INC        : return "+="
	if t == TOK_ASSIGN_DEC        : return "-="
	if t == TOK_ASSIGN_OR         : return "|="
	if t == TOK_ASSIGN_AND        : return "&="
	if t == TOK_STAR              : return "*"
	if t == TOK_PERCENT           : return "%"
	if t == TOK_SLASH             : return "/"
	if t == TOK_SHIFT_LEFT        : return "<<"
	if t == TOK_SHIFT_RIGHT       : return ">>"
	if t == TOK_PLUS              : return "+"
	if t == TOK_PUBLIC            : return "+"
	if t == TOK_MINUS             : return "-"
	if t == TOK_ARGS_OPEN         : return "("
	if t == TOK_ARGS_CLOSE        : return ")"
	if t == TOK_ARRAY_OPEN        : return "["
	if t == TOK_ARRAY_CLOSE       : return "]"
	if t == TOK_GENERICS_OPEN     : return "{"
	if t == TOK_GENERICS_CLOSE    : return "}"
	if t == TOK_ARGS_DELIM        : return ","
	if t == TOK_WHILE             : return "while"
	if t == TOK_FOR               : return "for"
	if t == TOK_IF                : return "if"
	if t == TOK_ELIF              : return "elif"
	if t == TOK_ELSE              : return "else"
	if t == TOK_STRUCT            : return "struct"
	if t == TOK_CONST             : return "const"
	if t == TOK_FUN               : return "fun"
	if t == TOK_VAR               : return "var"
	if t == TOK_RETURN            : return "return"
	if t == TOK_BREAK             : return "break"
	if t == TOK_CONTINUE          : return "continue"
	if t == TOK_INT               : return "int"
	if t == TOK_FLOAT             : return "float"
	if t == TOK_STRING            : return "string"
	if t == TOK_FILE              : return "file"
	if t == TOK_BOOL              : return "bool"
	if t == TOK_TRUE              : return "true"
	if t == TOK_FALSE             : return "false"
	if t == TOK_NIL               : return "nil"
	if t == TOK_LITERAL_STRING    : return "literal_string"
	if t == TOK_LITERAL_INT       : return "literal_int"
	if t == TOK_LITERAL_FLOAT     : return "literal_float"
	if t == TOK_ID                : return "id"
	if t == TOK_OR                : return "|"
	if t == TOK_LOGICAL_OR        : return "||"
	if t == TOK_AND               : return "&"
	if t == TOK_LOGICAL_AND       : return "&&"
	if t == TOK_LOGICAL_NOT       : return "!"
	if t == TOK_NOT               : return "~"
	if t == TOK_XOR               : return "^"
	if t == TOK_USE               : return "use"
	if t == TOK_IN                : return "in"
	if t == TOK_PUBLIC            : return "public"
	return "UNKNOWN"

const public int:
	UNRECOGNIZED
	TOK_IGNORE
	TOK_EOF
	TOK_EOL
	TOK_COMMENT_EOL
	TOK_DOT
	TOK_COLON
	TOK_MODULE
	TOK_EQ
	TOK_NEQ
	TOK_LT
	TOK_LTEQ
	TOK_GT
	TOK_GTEQ
	TOK_ASSIGN
	TOK_ASSIGN_INC
	TOK_ASSIGN_DEC
	TOK_ASSIGN_OR
	TOK_ASSIGN_AND
	TOK_SHIFT_LEFT
	TOK_SHIFT_RIGHT
	TOK_PLUS
	TOK_MINUS
	TOK_STAR
	TOK_SLASH
	TOK_PERCENT
	TOK_ARGS_OPEN
	TOK_ARGS_CLOSE
	TOK_ARRAY_OPEN
	TOK_ARRAY_CLOSE
	TOK_GENERICS_OPEN
	TOK_GENERICS_CLOSE
	TOK_ARGS_DELIM
	TOK_WHILE
	TOK_FOR
	TOK_IF
	TOK_ELIF
	TOK_ELSE
	TOK_STRUCT
	TOK_CONST
	TOK_FUN
	TOK_VAR
	TOK_OUT
	TOK_RETURN
	TOK_BREAK
	TOK_CONTINUE
	TOK_INT
	TOK_FLOAT
	TOK_STRING
	TOK_FILE
	TOK_BOOL
	TOK_TRUE
	TOK_FALSE
	TOK_NIL
	TOK_LITERAL_STRING
	TOK_LITERAL_INT
	TOK_LITERAL_FLOAT
	TOK_ID
	TOK_OR
	TOK_LOGICAL_OR
	TOK_AND
	TOK_LOGICAL_AND
	TOK_LOGICAL_NOT
	TOK_NOT
	TOK_XOR
	TOK_USE
	TOK_IN
	TOK_PUBLIC
