#pragma once
// Includes
#include <ecc_sys.h>
#include <scanner.h>
#include <emitter.h>
#include <utils.h>
#include <token.h>
#include <position.h>
#include <ast.h>
#include <log.h>
#include <modules.h>
namespace _parser {
// Constants
// Structure Prototypes
struct Parser;
// Structures
struct Parser
{
	_log::Log log;
	_ast::Node* ast;
	_scanner::Scanner scanner;
	_modules::Modules* modules;
	Parser()
	{
		ast = NULL;
	}
};
// Function prototypes
_ast::Node* parse_prog(_parser::Parser* ctx);
// Function definitions (public and generic)
} // namespace
