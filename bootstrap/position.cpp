#include "position.h"
namespace _position {
// Constants
// Structure Prototypes
// Structures
// Function prototypes
// Function definitions (private generic and all non-generic)
ecc_string pos_to_string(_position::Position* pos)
{
	return ((("line=" + ecc_to_string((pos->line + 1))) + " col=") + ecc_to_string((pos->col + 1)));
}
} // namespace
