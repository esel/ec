#include "log.h"
namespace _log {
// Constants
// Structure Prototypes
// Structures
// Function prototypes
// Function definitions (private generic and all non-generic)
bool log_set_file(_log::Log* log, ecc_file f)
{
	ecc_assert((log != NULL), "log is not nil");
	log->log_file = f;
	return _utils::file_is_valid(f);
}
void log_start_debug(_log::Log* log, ecc_string msg)
{
	ecc_assert(((log != NULL) && _utils::file_is_valid(log->log_file)), "log is not nil and log file is valid");
	_utils::fwrite_ec(log->log_file, ("[DEBUG] " + msg));
}
void log_add_debug(_log::Log* log, ecc_string msg)
{
	ecc_assert(((log != NULL) && _utils::file_is_valid(log->log_file)), "log is not nil and log file is valid");
	_utils::fwrite_ec(log->log_file, msg);
}
void log_end_debug(_log::Log* log, ecc_string msg)
{
	ecc_assert(((log != NULL) && _utils::file_is_valid(log->log_file)), "log is not nil and log file is valid");
	_utils::fwrite_ec(log->log_file, (msg + "\n"));
}
void log_debug(_log::Log* log, ecc_string msg)
{
	_log::log_start_debug(log, msg);
	_utils::fwrite_ec(log->log_file, "\n");
}
} // namespace
