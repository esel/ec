#include "ecc.h"
namespace _ecc {
// Constants
// Structure Prototypes
struct Context;
// Structures
struct Context
{
	_log::Log log;
	_ast::Node* ast;
	bool is_ast_printed;
	ecc_string srcpath;
	ecc_string dstpath;
	ecc_string dstpath_impl;
	ecc_string dstpath_symbols;
	ecc_string infile_path;
	ecc_string outfile_path;
	ecc_file outfile_debug;
	_modules::Modules modules;
	_parser::Parser parser;
	_generator::Generator generator;
	Context()
	{
		ast = NULL;
		is_ast_printed = false;
		outfile_debug = NULL;
	}
};
// Function prototypes
static void ecc_main(int argc, ecc_string* argv);
static void cleanup(_ecc::Context* ctx);
static void compile(_ecc::Context* ctx);
static void print_ast(_ecc::Context* ctx);
// Function definitions (private generic and all non-generic)
static void ecc_main(int argc, ecc_string* argv)
{
	if ( (argc < 3) )
	{
		std::cout << "ERROR: usage: " << argv[0] << " <path to EC source file> <path to output file> [search dirs]" << std::endl;
		::exit(1);
	}
	_ecc::Context ctx;
	ctx.srcpath = argv[1];
	ctx.dstpath = argv[2];
	std::cout << "Opening file: " << ctx.srcpath << std::endl;
	ctx.dstpath_impl = (ctx.dstpath + ".cpp");
	ctx.dstpath_symbols = (ctx.dstpath + ".h");
	ctx.infile_path = ctx.srcpath;
	ctx.generator.infile_name_no_ext = _utils::find_file_name_no_ext_from_path(ctx.srcpath);
	ctx.generator.module_name = _utils::get_module_name_from_file_name(ctx.generator.infile_name_no_ext);
	ctx.outfile_path = ctx.dstpath;
	ctx.parser.scanner.infile = _utils::fopen_ec(ctx.srcpath, "rb");
	if ( (!_utils::file_is_valid(ctx.parser.scanner.infile)) )
	{
		std::cout << "ERROR: source file could not be opened: " << ctx.srcpath << std::endl;
		::exit(1);
	}
	std::cout << "Output file (impl): " << ctx.dstpath_impl << std::endl;
	ctx.generator.outfile_impl = _utils::fopen_ec(ctx.dstpath_impl, "wb");
	_emitter::emit_set_outfile((&ctx.generator.emit), ctx.generator.outfile_impl);
	if ( (!_utils::file_is_valid(ctx.generator.outfile_impl)) )
	{
		std::cout << "ERROR: output file could not be opened: " << ctx.dstpath_impl << std::endl;
		::exit(1);
	}
	std::cout << "Output file (symbols): " << ctx.dstpath_symbols << std::endl;
	ctx.generator.outfile_symbols = _utils::fopen_ec(ctx.dstpath_symbols, "wb");
	if ( (!_utils::file_is_valid(ctx.generator.outfile_symbols)) )
	{
		std::cout << "ERROR: output file could not be opened: " << ctx.dstpath_symbols << std::endl;
		::exit(1);
	}
	ecc_string dbgpath;
	dbgpath = "ast.xml";
	std::cout << "AST debug output: " << dbgpath << std::endl;
	ctx.outfile_debug = _utils::fopen_ec(dbgpath, "a");
	if ( (!_utils::file_is_valid(ctx.outfile_debug)) )
	{
		std::cout << "ERROR: output file could not be opened: " << dbgpath << std::endl;
		::exit(1);
	}
	ecc_string logpath;
	logpath = "log.txt";
	std::cout << "Log output: " << logpath << std::endl;
	if ( (!_log::log_set_file((&ctx.log), _utils::fopen_ec(logpath, "a"))) )
	{
		std::cout << "ERROR: log file could not be opened: " << logpath << std::endl;
		::exit(1);
	}
	_log::log_debug((&ctx.log), ("starting compile of " + ctx.srcpath));
	ctx.modules.n_search_dirs = (argc - 3);
	for ( int i = 0; i < ctx.modules.n_search_dirs; ++i )
	{
		ctx.modules.search_dirs[i] = argv[(3 + i)];
	}
	ctx.parser.log = ctx.log;
	ctx.generator.log = ctx.log;
	ctx.parser.modules = (&ctx.modules);
	ctx.generator.modules = (&ctx.modules);
	_ecc::compile((&ctx));
	std::cout << "Done" << std::endl;
	::exit(0);
}
static void cleanup(_ecc::Context* ctx)
{
	_ecc::print_ast(ctx);
	if ( (!_utils::fclose_ec(ctx->parser.scanner.infile)) )
	{
		std::cout << "ERROR closing input file: " << ctx->infile_path << std::endl;
	}
	if ( (!_utils::fclose_ec(ctx->generator.outfile_impl)) )
	{
		std::cout << "ERROR closing output file (impl): " << ctx->outfile_path << std::endl;
	}
	if ( (!_utils::fclose_ec(ctx->generator.outfile_symbols)) )
	{
		std::cout << "ERROR closing output file (symbols): " << ctx->outfile_path << std::endl;
	}
	if ( (!_utils::fclose_ec(ctx->outfile_debug)) )
	{
		std::cout << "ERROR closing debug output file" << std::endl;
	}
	if ( (!_utils::fclose_ec(ctx->log.log_file)) )
	{
		std::cout << "ERROR closing log file" << std::endl;
	}
	for ( int i = 0; i < ctx->modules.n_used_modules; ++i )
	{
		_modules::Module* module;
		module = (&ctx->modules.used_modules[i]);
		_ast::destroy_ast(module->ast);
	}
	if ( (ctx->ast != NULL) )
	{
		_ast::destroy_ast(ctx->ast);
	}
}
static void compile(_ecc::Context* ctx)
{
	ctx->ast = _parser::parse_prog((&ctx->parser));
	_ecc::print_ast(ctx);
	ctx->generator.ast = ctx->ast;
	_generator::generate_code((&ctx->generator));
}
static void print_ast(_ecc::Context* ctx)
{
	if ( (!ctx->is_ast_printed) )
	{
		_ast::print_ast_xml(ctx->outfile_debug, ctx->ast, 0);
		ctx->is_ast_printed = true;
	}
}
} // namespace
#define ECC_MAIN_FUNCTION _ecc::ecc_main
#include <ecc_main_args.h>
