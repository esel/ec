#include "scanner.h"
namespace _scanner {
// Constants
static const int MAX_ID_LENGTH = 256;
static const int MAX_STRING_LITERAL_LENGTH = (1 << 16);
// Structure Prototypes
// Structures
// Function prototypes
static int read_char0(_scanner::Scanner* ctx);
static int read_char(_scanner::Scanner* ctx);
static int peek_char(_scanner::Scanner* ctx);
static bool is_digit(int c);
static bool is_alpha(int c);
static bool is_alphanumeric(int c);
static bool is_whitespace(int c);
static int next_non_whitespace_char(_scanner::Scanner* ctx);
static bool is_number_literal_end(int c);
static void scan_number_literal(_scanner::Scanner* ctx, _token::Token* tok, int c);
static void scan_keyword_or_id(_scanner::Scanner* ctx, _token::Token* tok, int c);
static void scan_dot_or_float_literal(_scanner::Scanner* ctx, _token::Token* tok, int c);
static int get_escaped_char(int c);
static void scan_string_literal(_scanner::Scanner* ctx, _token::Token* tok, int c);
static void consume_empty_lines(_scanner::Scanner* ctx);
static void scan_comment_eol0(_scanner::Scanner* ctx, _token::Token* tok);
static void scan_comment_eol(_scanner::Scanner* ctx, _token::Token* tok);
static int scan_op2(_scanner::Scanner* ctx, ecc_string next, int neq, int eq);
static int scan_op3(_scanner::Scanner* ctx, ecc_string next1, ecc_string next2, int eq0, int eq1, int eq2);
static _token::Token next_token_raw(_scanner::Scanner* ctx);
static _token::Token next_token_filtered(_scanner::Scanner* ctx);
// Function definitions (private generic and all non-generic)
_token::Token scanner_next_token(_scanner::Scanner* ctx)
{
	_token::Token next;
	if ( ((ctx->ignored_eol_since_next != 0) && (!ctx->ignore_eol)) )
	{
		next.type = _token::TOK_EOL;
	}
	else if ( ctx->is_peeked )
	{
		ctx->is_peeked = false;
		next = ctx->peeked;
	}
	else
	{
		next = _scanner::next_token_filtered(ctx);
	}
	ctx->ignored_eol_since_next = 0;
	return next;
}
_token::Token scanner_peek_token(_scanner::Scanner* ctx)
{
	if ( (!ctx->is_peeked) )
	{
		ctx->is_peeked = true;
		ctx->peeked = _scanner::next_token_filtered(ctx);
	}
	return ctx->peeked;
}
void scanner_ignore_newlines(_scanner::Scanner* ctx, bool ignore)
{
	ctx->ignore_eol = ignore;
}
static int read_char0(_scanner::Scanner* ctx)
{
	int c;
	c = 0;
	int i;
	i = 0;
	while ( (i < CHAR_BUFFER_SIZE) )
	{
		c = ::fgetc(ctx->infile);
		if ( (c == ecc_eof) )
		{
			break;
		}
		ctx->char_buffer[i] = c;
		i += 1;
	}
	if ( ((c == ecc_eof) && ::ferror(ctx->infile)) )
	{
		ctx->is_error = true;
		ctx->error_reason = "error reading character from file";
	}
	return i;
}
static int read_char(_scanner::Scanner* ctx)
{
	int size;
	int index;
	if ( (ctx->char_buffer_size == 0) )
	{
		size = _scanner::read_char0(ctx);
		index = 0;
	}
	else
	{
		size = ctx->char_buffer_size;
		index = ctx->char_buffer_index;
	}
	if ( (size == 0) )
	{
		return ecc_eof;
	}
	int c;
	c = ctx->char_buffer[index];
	ctx->char_buffer_size = (size - 1);
	ctx->char_buffer_index = (index + 1);
	if ( (c == ecc_to_char("\n")) )
	{
		ctx->pos.col = (-1);
		ctx->pos.line += 1;
		ctx->pos.indent = 0;
	}
	else
	{
		ctx->pos.col += 1;
		if ( (c == ecc_to_char("\t")) )
		{
			ctx->pos.indent += 1;
		}
	}
	return c;
}
static int peek_char(_scanner::Scanner* ctx)
{
	if ( (ctx->char_buffer_size == 0) )
	{
		ctx->char_buffer_size = _scanner::read_char0(ctx);
		ctx->char_buffer_index = 0;
	}
	if ( (ctx->char_buffer_size == 0) )
	{
		return ecc_eof;
	}
	return ctx->char_buffer[ctx->char_buffer_index];
}
static bool is_digit(int c)
{
	return ((ecc_to_char("0") <= c) && (c <= ecc_to_char("9")));
}
static bool is_alpha(int c)
{
	return ((((c >= ecc_to_char("a")) && (c <= ecc_to_char("z"))) || ((c >= ecc_to_char("A")) && (c <= ecc_to_char("Z")))) || (c == ecc_to_char("_")));
}
static bool is_alphanumeric(int c)
{
	return (_scanner::is_digit(c) || _scanner::is_alpha(c));
}
static bool is_whitespace(int c)
{
	return (((c == ecc_to_char(" ")) || (c == ecc_to_char("\r"))) || (c == ecc_to_char("\t")));
}
static int next_non_whitespace_char(_scanner::Scanner* ctx)
{
	int c;
	c = _scanner::read_char(ctx);
	while ( _scanner::is_whitespace(c) )
	{
		c = _scanner::read_char(ctx);
	}
	return c;
}
static bool is_number_literal_end(int c)
{
	return ((c == ecc_eof) || (!(((_scanner::is_alphanumeric(c) || (c == ecc_to_char("."))) || (c == ecc_to_char("+"))) || (c == ecc_to_char("-")))));
}
static void scan_number_literal(_scanner::Scanner* ctx, _token::Token* tok, int c)
{
	bool is_float;
	is_float = false;
	while ( true )
	{
		if ( (c == ecc_to_char(".")) )
		{
			is_float = true;
		}
		tok->text += ecc_to_char(c);
		c = _scanner::peek_char(ctx);
		if ( _scanner::is_number_literal_end(c) )
		{
			break;
		}
		_scanner::read_char(ctx);
	}
	bool err;
	if ( is_float )
	{
		tok->type = _token::TOK_LITERAL_FLOAT;
		tok->float_value = ecc_to_float(tok->text, err);
	}
	else
	{
		tok->type = _token::TOK_LITERAL_INT;
		tok->int_value = ecc_to_int(tok->text, err);
	}
	if ( err )
	{
		ctx->is_error = true;
		ctx->error_reason = ("invalid number literal: " + tok->text);
	}
}
static void scan_keyword_or_id(_scanner::Scanner* ctx, _token::Token* tok, int c)
{
	int buf[MAX_ID_LENGTH];
	buf[0] = c;
	for ( int i = 1; i < MAX_ID_LENGTH; ++i )
	{
		c = _scanner::peek_char(ctx);
		if ( (_scanner::is_alpha(c) || _scanner::is_digit(c)) )
		{
			_scanner::read_char(ctx);
			buf[i] = c;
		}
		else
		{
			ecc_string s;
			s = ecc_to_char(buf, i);
			if ( ("int" == s) )
			{
				tok->type = _token::TOK_INT;
			}
			else if ( ("float" == s) )
			{
				tok->type = _token::TOK_FLOAT;
			}
			else if ( ("string" == s) )
			{
				tok->type = _token::TOK_STRING;
			}
			else if ( ("file" == s) )
			{
				tok->type = _token::TOK_FILE;
			}
			else if ( ("bool" == s) )
			{
				tok->type = _token::TOK_BOOL;
			}
			else if ( ("true" == s) )
			{
				tok->type = _token::TOK_TRUE;
			}
			else if ( ("false" == s) )
			{
				tok->type = _token::TOK_FALSE;
			}
			else if ( ("nil" == s) )
			{
				tok->type = _token::TOK_NIL;
			}
			else if ( ("fun" == s) )
			{
				tok->type = _token::TOK_FUN;
			}
			else if ( ("struct" == s) )
			{
				tok->type = _token::TOK_STRUCT;
			}
			else if ( ("const" == s) )
			{
				tok->type = _token::TOK_CONST;
			}
			else if ( ("var" == s) )
			{
				tok->type = _token::TOK_VAR;
			}
			else if ( ("return" == s) )
			{
				tok->type = _token::TOK_RETURN;
			}
			else if ( ("break" == s) )
			{
				tok->type = _token::TOK_BREAK;
			}
			else if ( ("continue" == s) )
			{
				tok->type = _token::TOK_CONTINUE;
			}
			else if ( ("if" == s) )
			{
				tok->type = _token::TOK_IF;
			}
			else if ( ("elif" == s) )
			{
				tok->type = _token::TOK_ELIF;
			}
			else if ( ("else" == s) )
			{
				tok->type = _token::TOK_ELSE;
			}
			else if ( ("while" == s) )
			{
				tok->type = _token::TOK_WHILE;
			}
			else if ( ("for" == s) )
			{
				tok->type = _token::TOK_FOR;
			}
			else if ( ("use" == s) )
			{
				tok->type = _token::TOK_USE;
			}
			else if ( ("in" == s) )
			{
				tok->type = _token::TOK_IN;
			}
			else if ( ("public" == s) )
			{
				tok->type = _token::TOK_PUBLIC;
			}
			else
			{
				tok->text = s;
				tok->type = _token::TOK_ID;
			}
			return ;
		}
	}
	ctx->is_error = true;
	ctx->error_reason = "internal error: id too long";
}
static void scan_dot_or_float_literal(_scanner::Scanner* ctx, _token::Token* tok, int c)
{
	if ( _scanner::is_digit(_scanner::peek_char(ctx)) )
	{
		_scanner::scan_number_literal(ctx, tok, c);
	}
	else
	{
		tok->type = _token::TOK_DOT;
	}
}
static int get_escaped_char(int c)
{
	if ( (c == ecc_to_char("n")) )
	{
		return ecc_to_char("\n");
	}
	if ( (c == ecc_to_char("r")) )
	{
		return ecc_to_char("\r");
	}
	if ( (c == ecc_to_char("t")) )
	{
		return ecc_to_char("\t");
	}
	if ( (c == ecc_to_char("\\")) )
	{
		return ecc_to_char("\\");
	}
	if ( (c == ecc_to_char("\"")) )
	{
		return ecc_to_char("\"");
	}
	return ecc_eof;
}
static void scan_string_literal(_scanner::Scanner* ctx, _token::Token* tok, int c)
{
	bool escape;
	escape = false;
	int end_quote;
	end_quote = c;
	for ( int i = 1; i < MAX_STRING_LITERAL_LENGTH; ++i )
	{
		c = _scanner::read_char(ctx);
		if ( escape )
		{
			escape = false;
			int ec;
			ec = _scanner::get_escaped_char(c);
			if ( (ec == ecc_eof) )
			{
				ctx->is_error = true;
				ctx->error_reason = "invalid escape character";
				return ;
			}
			else
			{
				tok->text += ecc_to_char(ec);
			}
		}
		else if ( (c == ecc_to_char("\\")) )
		{
			escape = true;
		}
		else if ( (c == end_quote) )
		{
			tok->type = _token::TOK_LITERAL_STRING;
			return ;
		}
		else
		{
			tok->text += ecc_to_char(c);
		}
	}
	ctx->is_error = true;
	ctx->error_reason = "internal error: string literal too long";
}
static void consume_empty_lines(_scanner::Scanner* ctx)
{
	int c;
	c = _scanner::peek_char(ctx);
	while ( (_scanner::is_whitespace(c) || (c == ecc_to_char("\n"))) )
	{
		_scanner::read_char(ctx);
		c = _scanner::peek_char(ctx);
	}
}
static void scan_comment_eol0(_scanner::Scanner* ctx, _token::Token* tok)
{
	int c;
	c = _scanner::read_char(ctx);
	while ( ((c != ecc_to_char("\n")) && (c != ecc_eof)) )
	{
		c = _scanner::read_char(ctx);
	}
}
static void scan_comment_eol(_scanner::Scanner* ctx, _token::Token* tok)
{
	int c;
	while ( true )
	{
		_scanner::scan_comment_eol0(ctx, tok);
		_scanner::consume_empty_lines(ctx);
		c = _scanner::peek_char(ctx);
		if ( (c != ecc_to_char("#")) )
		{
			break;
		}
	}
	if ( ctx->is_prev_eol )
	{
		tok->type = _token::TOK_IGNORE;
	}
	else
	{
		tok->type = _token::TOK_EOL;
	}
}
static int scan_op2(_scanner::Scanner* ctx, ecc_string next, int neq, int eq)
{
	if ( (_scanner::peek_char(ctx) != ecc_to_char(next)) )
	{
		return neq;
	}
	_scanner::read_char(ctx);
	return eq;
}
static int scan_op3(_scanner::Scanner* ctx, ecc_string next1, ecc_string next2, int eq0, int eq1, int eq2)
{
	int c;
	c = _scanner::peek_char(ctx);
	if ( (c == ecc_to_char(next1)) )
	{
		_scanner::read_char(ctx);
		return eq1;
	}
	if ( (c == ecc_to_char(next2)) )
	{
		_scanner::read_char(ctx);
		return eq2;
	}
	return eq0;
}
static _token::Token next_token_raw(_scanner::Scanner* ctx)
{
	_token::Token tok;
	int c;
	while ( true )
	{
		c = _scanner::next_non_whitespace_char(ctx);
		if ( ((!ctx->is_prev_eol) || (c != ecc_to_char("\n"))) )
		{
			break;
		}
	}
	tok.pos = ctx->pos;
	if ( (c == ecc_to_char("\n")) )
	{
		tok.type = _token::TOK_EOL;
		_scanner::consume_empty_lines(ctx);
		ctx->is_prev_eol = true;
	}
	else
	{
		if ( _scanner::is_alpha(c) )
		{
			_scanner::scan_keyword_or_id(ctx, (&tok), c);
		}
		else if ( _scanner::is_digit(c) )
		{
			_scanner::scan_number_literal(ctx, (&tok), c);
		}
		else if ( (c == ecc_to_char(".")) )
		{
			_scanner::scan_dot_or_float_literal(ctx, (&tok), c);
		}
		else if ( (c == ecc_to_char("\"")) )
		{
			_scanner::scan_string_literal(ctx, (&tok), c);
		}
		else if ( (c == ecc_to_char("'")) )
		{
			_scanner::scan_string_literal(ctx, (&tok), c);
		}
		else if ( (c == ecc_to_char("#")) )
		{
			_scanner::scan_comment_eol(ctx, (&tok));
		}
		else if ( (c == ecc_to_char("{")) )
		{
			tok.type = _token::TOK_GENERICS_OPEN;
		}
		else if ( (c == ecc_to_char("}")) )
		{
			tok.type = _token::TOK_GENERICS_CLOSE;
		}
		else if ( (c == ecc_to_char("[")) )
		{
			tok.type = _token::TOK_ARRAY_OPEN;
		}
		else if ( (c == ecc_to_char("]")) )
		{
			tok.type = _token::TOK_ARRAY_CLOSE;
		}
		else if ( (c == ecc_to_char("(")) )
		{
			tok.type = _token::TOK_ARGS_OPEN;
		}
		else if ( (c == ecc_to_char(")")) )
		{
			tok.type = _token::TOK_ARGS_CLOSE;
		}
		else if ( (c == ecc_to_char(",")) )
		{
			tok.type = _token::TOK_ARGS_DELIM;
		}
		else if ( (c == ecc_to_char("*")) )
		{
			tok.type = _token::TOK_STAR;
		}
		else if ( (c == ecc_to_char("%")) )
		{
			tok.type = _token::TOK_PERCENT;
		}
		else if ( (c == ecc_to_char("/")) )
		{
			tok.type = _token::TOK_SLASH;
		}
		else if ( (c == ecc_to_char("~")) )
		{
			tok.type = _token::TOK_NOT;
		}
		else if ( (c == ecc_to_char("^")) )
		{
			tok.type = _token::TOK_XOR;
		}
		else if ( (c == ecc_to_char(":")) )
		{
			tok.type = _scanner::scan_op2(ctx, ":", _token::TOK_COLON, _token::TOK_MODULE);
		}
		else if ( (c == ecc_to_char("=")) )
		{
			tok.type = _scanner::scan_op2(ctx, "=", _token::TOK_ASSIGN, _token::TOK_EQ);
		}
		else if ( (c == ecc_to_char("!")) )
		{
			tok.type = _scanner::scan_op2(ctx, "=", _token::TOK_LOGICAL_NOT, _token::TOK_NEQ);
		}
		else if ( (c == ecc_to_char("+")) )
		{
			tok.type = _scanner::scan_op2(ctx, "=", _token::TOK_PLUS, _token::TOK_ASSIGN_INC);
		}
		else if ( (c == ecc_to_char("-")) )
		{
			tok.type = _scanner::scan_op2(ctx, "=", _token::TOK_MINUS, _token::TOK_ASSIGN_DEC);
		}
		else if ( (c == ecc_to_char("&")) )
		{
			tok.type = _scanner::scan_op3(ctx, "&", "=", _token::TOK_AND, _token::TOK_LOGICAL_AND, _token::TOK_ASSIGN_AND);
		}
		else if ( (c == ecc_to_char("|")) )
		{
			tok.type = _scanner::scan_op3(ctx, "|", "=", _token::TOK_OR, _token::TOK_LOGICAL_OR, _token::TOK_ASSIGN_OR);
		}
		else if ( (c == ecc_to_char("<")) )
		{
			tok.type = _scanner::scan_op3(ctx, "=", "<", _token::TOK_LT, _token::TOK_LTEQ, _token::TOK_SHIFT_LEFT);
		}
		else if ( (c == ecc_to_char(">")) )
		{
			tok.type = _scanner::scan_op3(ctx, "=", ">", _token::TOK_GT, _token::TOK_GTEQ, _token::TOK_SHIFT_RIGHT);
		}
		else if ( (c == ecc_eof) )
		{
			tok.type = _token::TOK_EOF;
		}
		else
		{
			ctx->is_error = true;
			ctx->error_reason = ("unrecognized token at " + _position::pos_to_string((&ctx->pos)));
		}
		ctx->is_prev_eol = false;
	}
	return tok;
}
static _token::Token next_token_filtered(_scanner::Scanner* ctx)
{
	_token::Token tok;
	while ( true )
	{
		tok = _scanner::next_token_raw(ctx);
		if ( (ctx->ignore_eol && (tok.type == _token::TOK_EOL)) )
		{
			ctx->ignored_eol_since_next += 1;
		}
		else if ( (tok.type != _token::TOK_IGNORE) )
		{
			break;
		}
	}
	return tok;
}
} // namespace
