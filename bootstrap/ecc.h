#pragma once
// Includes
#include <ecc_sys.h>
#include <modules.h>
#include <generator.h>
#include <parser.h>
#include <scanner.h>
#include <emitter.h>
#include <utils.h>
#include <ast.h>
#include <log.h>
namespace _ecc {
// Constants
// Structure Prototypes
// Structures
// Function prototypes
// Function definitions (public and generic)
} // namespace
