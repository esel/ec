#pragma once
// Includes
#include <ecc_sys.h>
#include <token.h>
#include <utils.h>
namespace _ast {
// Constants
static const int AST_PROG = 0;
static const int AST_USE = 1;
static const int AST_STRUCT = 2;
static const int AST_CONST = 3;
static const int AST_CONST_LIST = 4;
static const int AST_FUN = 5;
static const int AST_FUN_TYPE = 6;
static const int AST_PARAM = 7;
static const int AST_PARAM_TYPE = 8;
static const int AST_GENERIC_PARAMS = 9;
static const int AST_GENERIC_PARAM = 10;
static const int AST_BLOCK = 11;
static const int AST_WHILE = 12;
static const int AST_FOR = 13;
static const int AST_IF = 14;
static const int AST_CALL = 15;
static const int AST_CALL_MODULE = 16;
static const int AST_VAR = 17;
static const int AST_VAR_TYPE = 18;
static const int AST_RETURN = 19;
static const int AST_BREAK = 20;
static const int AST_CONTINUE = 21;
static const int AST_EXPR_ASSIGN = 22;
static const int AST_EXPR_BINARY = 23;
static const int AST_EXPR_UNARY = 24;
static const int AST_EXPR_CONST = 25;
static const int AST_EXPR_ID = 26;
static const int AST_EXPR_ARRAY = 27;
static const int AST_EXPR_DOT = 28;
static const int AST_ARRAY_LITERAL = 29;
static const int NODE_FLAG_POINTER = 0;
static const int NODE_FLAG_ARRAY = 1;
static const int NODE_FLAG_OUT = 2;
static const int NODE_FLAG_ELSE = 3;
static const int NODE_FLAG_GENERATED = 4;
static const int NODE_FLAG_EXPR_ARRAY_DIM = 5;
static const int NODE_FLAG_EXPR_INIT = 6;
static const int NODE_FLAG_PUBLIC = 7;
static const int NODE_FLAG_IMPORTED = 8;
static const int NODE_FLAG_GENERIC = 9;
static const int NODE_FLAG_SPECIFIC = 10;
// Structure Prototypes
struct NodeList;
struct Node;
// Structures
struct NodeList
{
	int count;
	_ast::Node* first;
	_ast::Node* last;
	NodeList()
	{
		count = 0;
		first = NULL;
		last = NULL;
	}
};
struct Node
{
	int type;
	int flags;
	_ast::Node* parent;
	_ast::Node* next;
	_ast::Node* prev;
	_ast::NodeList children;
	_token::Token data;
	Node()
	{
		type = _token::UNRECOGNIZED;
		flags = 0;
		parent = NULL;
		next = NULL;
		prev = NULL;
	}
};
// Function prototypes
ecc_string ast_type_string(int t);
_ast::Node* new_node(int type);
_ast::Node* add_node(_ast::Node* parent, _ast::Node* child);
_ast::Node* add_new_node(_ast::Node* parent, int child_type, _token::Token data);
bool is_node_flag(_ast::Node* node, int flag);
void set_node_flag(_ast::Node* node, int flag);
void reset_node_flag(_ast::Node* node, int flag);
int set_flag(int bits, int bit);
_ast::Node* find_child_of_type(_ast::Node* parent, int child_type);
_ast::Node* find_child_for_id(_ast::Node* parent, ecc_string child_name);
void print_ast_json(ecc_file f, _ast::Node* node, int level);
void print_ast_xml(ecc_file f, _ast::Node* node, int level);
void destroy_ast(_ast::Node* node);
// Function definitions (public and generic)
} // namespace
