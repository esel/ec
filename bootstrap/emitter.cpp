#include "emitter.h"
namespace _emitter {
// Constants
// Structure Prototypes
// Structures
// Function prototypes
// Function definitions (private generic and all non-generic)
void emit(_emitter::Emit* emit, ecc_string s)
{
	ecc_assert((emit != NULL), "emit is not null");
	if ( emit->is_enabled )
	{
		_utils::fwrite_ec(emit->outfile_current, s);
	}
}
void emit_indent(_emitter::Emit* emit)
{
	ecc_assert((emit != NULL), "emit is not null");
	if ( emit->is_enabled )
	{
		ecc_assert(_utils::file_is_valid(emit->outfile_current), "file is valid");
		for ( int i = 0; i < emit->indent; ++i )
		{
			_utils::fwrite_ec(emit->outfile_current, "\t");
		}
	}
}
void emit_line(_emitter::Emit* emit, ecc_string s)
{
	ecc_assert((emit != NULL), "emit is not null");
	if ( emit->is_enabled )
	{
		ecc_assert(_utils::file_is_valid(emit->outfile_current), "file is valid");
		if ( (ecc_len(s) != 0) )
		{
			_emitter::emit_indent(emit);
			_utils::fwrite_ec(emit->outfile_current, s);
		}
		_utils::fwrite_ec(emit->outfile_current, "\n");
	}
}
void emit_inc_indent(_emitter::Emit* emit)
{
	ecc_assert((emit != NULL), "emit is not null");
	emit->indent += 1;
}
void emit_dec_indent(_emitter::Emit* emit)
{
	ecc_assert((emit != NULL), "emit is not null");
	emit->indent -= 1;
	ecc_assert((emit->indent >= 0), "indent is not negative");
}
void emit_set_outfile(_emitter::Emit* emit, ecc_file f)
{
	ecc_assert((emit != NULL), "emit is not null");
	emit->outfile_current = f;
}
ecc_file emit_get_outfile(_emitter::Emit* emit)
{
	ecc_assert((emit != NULL), "emit is not null");
	return emit->outfile_current;
}
void emit_enable(_emitter::Emit* emit)
{
	ecc_assert((emit != NULL), "emit is not null");
	emit->is_enabled = true;
}
void emit_disable(_emitter::Emit* emit)
{
	ecc_assert((emit != NULL), "emit is not null");
	emit->is_enabled = false;
}
} // namespace
