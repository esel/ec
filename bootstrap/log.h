#pragma once
// Includes
#include <ecc_sys.h>
#include <utils.h>
namespace _log {
// Constants
// Structure Prototypes
struct Log;
// Structures
struct Log
{
	ecc_file log_file;
	Log()
	{
		log_file = NULL;
	}
};
// Function prototypes
bool log_set_file(_log::Log* log, ecc_file f);
void log_start_debug(_log::Log* log, ecc_string msg);
void log_add_debug(_log::Log* log, ecc_string msg);
void log_end_debug(_log::Log* log, ecc_string msg);
void log_debug(_log::Log* log, ecc_string msg);
// Function definitions (public and generic)
} // namespace
