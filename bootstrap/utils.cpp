#include "utils.h"
namespace _utils {
// Constants
// Structure Prototypes
// Structures
// Function prototypes
// Function definitions (private generic and all non-generic)
ecc_file fopen_ec(ecc_string path, ecc_string mode)
{
	return ::fopen(ecc_c_str(path), ecc_c_str(mode));
}
bool fclose_ec(ecc_file f)
{
	bool success;
	success = true;
	if ( (f != NULL) )
	{
		success = (::fclose(f) != ecc_eof);
	}
	return success;
}
bool fwrite_ec(ecc_file f, ecc_string text)
{
	ecc_assert((f != NULL), "file handle is not null");
	int written;
	written = ::fwrite(ecc_c_str(text), 1, ecc_len(text), f);
	return (written == ecc_len(text));
}
bool file_is_valid(ecc_file f)
{
	return ((f != NULL) && (::ferror(f) == 0));
}
ecc_string find_file_name_no_ext_from_path(ecc_string path)
{
	int slash;
	slash = ecc_to_char("/");
	if ( (ecc_get_platform() == "win32") )
	{
		slash = ecc_to_char("\\");
	}
	ecc_string name;
	int start;
	start = 0;
	for ( int i = 0; i < ecc_len(path); ++i )
	{
		if ( (path[i] == slash) )
		{
			start = i;
		}
	}
	for ( int i = (start + 1); i < ecc_len(path); ++i )
	{
		if ( (path[i] == ecc_to_char(".")) )
		{
			break;
		}
		name += path[i];
	}
	return name;
}
ecc_string get_module_name_from_file_name(ecc_string file_name)
{
	return ("_" + file_name);
}
} // namespace
