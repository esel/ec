Purpose
===

This directory contains a snapshot of the compiler in the target language (C++). This snapshot has passed the regression tests and is also able to build the EC language version of the compiler (aka "bootstrap" the compiler).

The C++ source needs to be updated in tandem with changes to the compiler, i.e. when a new feature is used in the compiler itself. The following process is used, where A is the feature in question (e.g. a new syntax construct):

1. add compilation and code generation support for A in the EC source code of the compiler
2. add a regression test which exercises this part of the compiler
3. build the compiler itself
4. verify that the newly built native binary compiler is able to build itself
5. make a new bootstrapping snapshot by copying the C++ output to this directory
6. optional: start using A in the EC source code of the compiler
