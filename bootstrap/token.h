#pragma once
// Includes
#include <ecc_sys.h>
#include <position.h>
namespace _token {
// Constants
static const int UNRECOGNIZED = 0;
static const int TOK_IGNORE = 1;
static const int TOK_EOF = 2;
static const int TOK_EOL = 3;
static const int TOK_COMMENT_EOL = 4;
static const int TOK_DOT = 5;
static const int TOK_COLON = 6;
static const int TOK_MODULE = 7;
static const int TOK_EQ = 8;
static const int TOK_NEQ = 9;
static const int TOK_LT = 10;
static const int TOK_LTEQ = 11;
static const int TOK_GT = 12;
static const int TOK_GTEQ = 13;
static const int TOK_ASSIGN = 14;
static const int TOK_ASSIGN_INC = 15;
static const int TOK_ASSIGN_DEC = 16;
static const int TOK_ASSIGN_OR = 17;
static const int TOK_ASSIGN_AND = 18;
static const int TOK_SHIFT_LEFT = 19;
static const int TOK_SHIFT_RIGHT = 20;
static const int TOK_PLUS = 21;
static const int TOK_MINUS = 22;
static const int TOK_STAR = 23;
static const int TOK_SLASH = 24;
static const int TOK_PERCENT = 25;
static const int TOK_ARGS_OPEN = 26;
static const int TOK_ARGS_CLOSE = 27;
static const int TOK_ARRAY_OPEN = 28;
static const int TOK_ARRAY_CLOSE = 29;
static const int TOK_GENERICS_OPEN = 30;
static const int TOK_GENERICS_CLOSE = 31;
static const int TOK_ARGS_DELIM = 32;
static const int TOK_WHILE = 33;
static const int TOK_FOR = 34;
static const int TOK_IF = 35;
static const int TOK_ELIF = 36;
static const int TOK_ELSE = 37;
static const int TOK_STRUCT = 38;
static const int TOK_CONST = 39;
static const int TOK_FUN = 40;
static const int TOK_VAR = 41;
static const int TOK_OUT = 42;
static const int TOK_RETURN = 43;
static const int TOK_BREAK = 44;
static const int TOK_CONTINUE = 45;
static const int TOK_INT = 46;
static const int TOK_FLOAT = 47;
static const int TOK_STRING = 48;
static const int TOK_FILE = 49;
static const int TOK_BOOL = 50;
static const int TOK_TRUE = 51;
static const int TOK_FALSE = 52;
static const int TOK_NIL = 53;
static const int TOK_LITERAL_STRING = 54;
static const int TOK_LITERAL_INT = 55;
static const int TOK_LITERAL_FLOAT = 56;
static const int TOK_ID = 57;
static const int TOK_OR = 58;
static const int TOK_LOGICAL_OR = 59;
static const int TOK_AND = 60;
static const int TOK_LOGICAL_AND = 61;
static const int TOK_LOGICAL_NOT = 62;
static const int TOK_NOT = 63;
static const int TOK_XOR = 64;
static const int TOK_USE = 65;
static const int TOK_IN = 66;
static const int TOK_PUBLIC = 67;
// Structure Prototypes
struct Token;
// Structures
struct Token
{
	int type;
	int int_value;
	float float_value;
	_position::Position pos;
	ecc_string text;
	Token()
	{
		type = UNRECOGNIZED;
		int_value = 0;
		float_value = 0;
	}
};
// Function prototypes
ecc_string token_to_string(_token::Token* tok);
ecc_string token_type_string(int t);
// Function definitions (public and generic)
} // namespace
