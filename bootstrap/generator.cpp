#include "generator.h"
namespace _generator {
// Constants
// Structure Prototypes
struct Eval;
struct Symbol;
// Structures
struct Eval
{
	_ast::Node* decl;
	Eval()
	{
		decl = NULL;
	}
};
struct Symbol
{
	_ast::Node* node;
	ecc_string module_name;
	bool is_imported;
	Symbol()
	{
		node = NULL;
		is_imported = false;
	}
};
// Function prototypes
static void generator_exit_error(_generator::Generator* ctx, ecc_string msg, _position::Position pos);
static void generate_namespace_start(_generator::Generator* ctx);
static void generate_public_includes(_generator::Generator* ctx, _ast::Node* ast);
static void generate_private_includes(_generator::Generator* ctx, _ast::Node* ast);
static void generate_constants(_generator::Generator* ctx, _ast::Node* ast, bool is_public);
static void generate_constant(_generator::Generator* ctx, _ast::Node* c, int index);
static void generate_module_symbols(_generator::Generator* ctx, _ast::Node* ast);
static void generate_structure_prototypes(_generator::Generator* ctx, _ast::Node* ast, bool is_public);
static void generate_template(_generator::Generator* ctx, _ast::Node* declaration);
static void generate_function_prototypes(_generator::Generator* ctx, _ast::Node* ast, bool is_public);
static void generate_structures(_generator::Generator* ctx, _ast::Node* ast, bool is_public);
static void reset_generated_flag_on_structures(_generator::Generator* ctx, _ast::Node* ast);
static bool all_members_defined(_generator::Generator* ctx, _ast::Node* ast, _ast::Node* struct_node);
static bool is_member_generic(_generator::Generator* ctx, _ast::Node* struct_node, _ast::Node* member_node);
static bool is_type_generic(_ast::Node* parent_node, _ast::Node* child_node, _ast::Node* child_type, _ast::Node* generic_types);
static _ast::Node* get_node_in_ast(_ast::Node* ast, ecc_string name, int container_type, int type);
static _generator::Symbol get_node_for_name(_generator::Generator* ctx, ecc_string symbol_name, int container_type, int symbol_type);
static _generator::Symbol get_struct(_generator::Generator* ctx, ecc_string type_name);
static _generator::Symbol get_fun(_generator::Generator* ctx, ecc_string fun_name);
static _generator::Symbol get_const(_generator::Generator* ctx, ecc_string const_name);
static void generate_structure(_generator::Generator* ctx, _ast::Node* child);
static void generate_main(_generator::Generator* ctx);
static int param_count(_ast::Node* func);
static int child_count(_ast::Node* root, int child_type);
static void generate_functions(_generator::Generator* ctx, _ast::Node* ast, bool is_generic, bool is_public);
static void generate_function_definitions(_generator::Generator* ctx, _ast::Node* ast);
static void generate_block(_generator::Generator* ctx, _ast::Node* block);
static bool is_user_type(_ast::Node* type_node);
static void generate_return(_generator::Generator* ctx, _ast::Node* n);
static void generate_if(_generator::Generator* ctx, _ast::Node* n);
static void generate_while(_generator::Generator* ctx, _ast::Node* n);
static void generate_for(_generator::Generator* ctx, _ast::Node* n);
static void generate_expr(_generator::Generator* ctx, _ast::Node* expr);
static void generate_expr0(_generator::Generator* ctx, _ast::Node* expr, _generator::Eval* eval);
static void generate_array_expr(_generator::Generator* ctx, _ast::Node* expr, _generator::Eval* eval);
static void generate_dot_expr(_generator::Generator* ctx, _ast::Node* expr, _generator::Eval* eval_return);
static _ast::Node* find_var_for_id(_generator::Generator* ctx, _ast::Node* id);
static void generate_string_literal(_generator::Generator* ctx, _ast::Node* expr);
static void generate_call(_generator::Generator* ctx, _ast::Node* expr, _generator::Eval* eval);
static bool is_builtin_function(ecc_string f);
static void generate_call_user(_generator::Generator* ctx, _ast::Node* expr, _generator::Eval* eval);
static void generate_call_builtin(_generator::Generator* ctx, _ast::Node* expr, _generator::Eval* eval);
static void generate_call_new(_generator::Generator* ctx, _ast::Node* expr, _generator::Eval* eval);
static void generate_call_delete(_generator::Generator* ctx, _ast::Node* expr, _generator::Eval* eval_return);
static void generate_call_println(_generator::Generator* ctx, _ast::Node* expr, _generator::Eval* eval);
static void generate_call_print(_generator::Generator* ctx, _ast::Node* expr, _generator::Eval* eval);
static void generate_function_prototype(_generator::Generator* ctx, _ast::Node* node);
static void generate_function_header(_generator::Generator* ctx, _ast::Node* node);
static void generate_param_list(_generator::Generator* ctx, _ast::Node* node, _ast::Node* generic_types);
static void generate_function_pointer(_generator::Generator* ctx, _ast::Node* var_node, _ast::Node* type_node, _ast::Node* generic_types);
static bool is_function_pointer(_ast::Node* type_node);
static void generate_var(_generator::Generator* ctx, _ast::Node* var_node);
static void generate_param_type(_generator::Generator* ctx, _ast::Node* parent_node, _ast::Node* decl_node, _ast::Node* type_node, _ast::Node* generic_types);
static void generate_type(_generator::Generator* ctx, _ast::Node* parent_node, _ast::Node* decl_node, _ast::Node* type_node, _ast::Node* generic_types);
static ecc_string ec_type_to_c_type(_generator::Generator* ctx, _ast::Node* node);
static ecc_string ec_type_to_c_type_token(_generator::Generator* ctx, _token::Token* tok);
// Function definitions (private generic and all non-generic)
void generate_code(_generator::Generator* ctx)
{
	_ast::Node* ast;
	ast = ctx->ast;
	ecc_assert((ast != NULL), "ast is not null");
	_generator::generate_module_symbols(ctx, ast);
	_generator::generate_private_includes(ctx, ast);
	_generator::generate_namespace_start(ctx);
	_generator::generate_constants(ctx, ast, false);
	_generator::generate_structure_prototypes(ctx, ast, false);
	_generator::generate_structures(ctx, ast, false);
	_generator::generate_function_prototypes(ctx, ast, false);
	_generator::generate_function_definitions(ctx, ast);
	_emitter::emit_line((&ctx->emit), "} // namespace");
	_generator::generate_main(ctx);
}
static void generator_exit_error(_generator::Generator* ctx, ecc_string msg, _position::Position pos)
{
	std::cout << "GENERATOR ERROR: " << msg << std::endl;
	std::cout << "Near " << _position::pos_to_string((&pos)) << std::endl;
	::exit(1);
}
static void generate_namespace_start(_generator::Generator* ctx)
{
	_emitter::emit((&ctx->emit), "namespace ");
	_emitter::emit((&ctx->emit), ctx->module_name);
	_emitter::emit_line((&ctx->emit), " {");
}
static void generate_public_includes(_generator::Generator* ctx, _ast::Node* ast)
{
	_emitter::emit((&ctx->emit), "// Includes\n");
	_emitter::emit((&ctx->emit), "#include <ecc_sys.h>\n");
	_ast::Node* child;
	child = ast->children.first;
	while ( (child != NULL) )
	{
		if ( (child->type == _ast::AST_USE) )
		{
			_emitter::emit((&ctx->emit), "#include <");
			_emitter::emit((&ctx->emit), child->data.text);
			_emitter::emit((&ctx->emit), ".h>\n");
		}
		child = child->next;
	}
}
static void generate_private_includes(_generator::Generator* ctx, _ast::Node* ast)
{
	_emitter::emit((&ctx->emit), "#include \"");
	_emitter::emit((&ctx->emit), ctx->infile_name_no_ext);
	_emitter::emit((&ctx->emit), ".h\"\n");
}
static void generate_constants(_generator::Generator* ctx, _ast::Node* ast, bool is_public)
{
	_emitter::emit((&ctx->emit), "// Constants\n");
	_ast::Node* child;
	child = ast->children.first;
	while ( (child != NULL) )
	{
		if ( (_ast::is_node_flag(child, _ast::NODE_FLAG_PUBLIC) == is_public) )
		{
			if ( (child->type == _ast::AST_CONST) )
			{
				_generator::generate_constant(ctx, child, 0);
			}
			else if ( (child->type == _ast::AST_CONST_LIST) )
			{
				int index;
				index = 0;
				_ast::Node* c;
				c = child->children.first;
				while ( (c != NULL) )
				{
					_generator::generate_constant(ctx, c, index);
					index += 1;
					c = c->next;
				}
			}
		}
		child = child->next;
	}
}
static void generate_constant(_generator::Generator* ctx, _ast::Node* c, int index)
{
	_emitter::emit((&ctx->emit), "static const ");
	_emitter::emit((&ctx->emit), _generator::ec_type_to_c_type(ctx, c->children.first));
	_emitter::emit((&ctx->emit), " ");
	_emitter::emit((&ctx->emit), c->data.text);
	_emitter::emit((&ctx->emit), " = ");
	if ( (c->children.count == 2) )
	{
		_generator::generate_expr(ctx, c->children.last);
	}
	else
	{
		_emitter::emit((&ctx->emit), ecc_to_string(index));
	}
	_emitter::emit((&ctx->emit), ";\n");
}
static void generate_module_symbols(_generator::Generator* ctx, _ast::Node* ast)
{
	ecc_file prev_outfile;
	prev_outfile = _emitter::emit_get_outfile((&ctx->emit));
	_emitter::emit_set_outfile((&ctx->emit), ctx->outfile_symbols);
	_emitter::emit_line((&ctx->emit), "#pragma once");
	_generator::generate_public_includes(ctx, ast);
	_generator::generate_namespace_start(ctx);
	_generator::generate_constants(ctx, ast, true);
	_generator::generate_structure_prototypes(ctx, ast, true);
	_generator::generate_structures(ctx, ast, true);
	_generator::generate_function_prototypes(ctx, ast, true);
	_emitter::emit((&ctx->emit), "// Function definitions (public and generic)\n");
	_generator::generate_functions(ctx, ast, true, true);
	_emitter::emit_line((&ctx->emit), "} // namespace");
	_emitter::emit_set_outfile((&ctx->emit), prev_outfile);
}
static void generate_structure_prototypes(_generator::Generator* ctx, _ast::Node* ast, bool is_public)
{
	_emitter::emit((&ctx->emit), "// Structure Prototypes\n");
	_ast::Node* child;
	child = ast->children.first;
	while ( (child != NULL) )
	{
		if ( (child->type == _ast::AST_STRUCT) )
		{
			if ( (_ast::is_node_flag(child, _ast::NODE_FLAG_PUBLIC) == is_public) )
			{
				if ( _ast::is_node_flag(child, _ast::NODE_FLAG_GENERIC) )
				{
					_generator::generate_template(ctx, child);
				}
				_emitter::emit((&ctx->emit), (("struct " + child->data.text) + ";\n"));
			}
		}
		child = child->next;
	}
}
static void generate_template(_generator::Generator* ctx, _ast::Node* declaration)
{
	_emitter::emit((&ctx->emit), "template <");
	_ast::Node* generic_types;
	generic_types = _ast::find_child_of_type(declaration, _ast::AST_GENERIC_PARAMS);
	ecc_assert((generic_types != NULL), "generic parameter list is not nil");
	_ast::Node* generic_child;
	generic_child = generic_types->children.first;
	while ( (generic_child != NULL) )
	{
		ecc_assert((generic_child->type == _ast::AST_GENERIC_PARAM), "node is a generic type name");
		_emitter::emit((&ctx->emit), "class ");
		_emitter::emit((&ctx->emit), generic_child->data.text);
		generic_child = generic_child->next;
		if ( (generic_child != NULL) )
		{
			_emitter::emit((&ctx->emit), ",");
		}
	}
	_emitter::emit((&ctx->emit), "> ");
}
static void generate_function_prototypes(_generator::Generator* ctx, _ast::Node* ast, bool is_public)
{
	_emitter::emit((&ctx->emit), "// Function prototypes\n");
	_ast::Node* child;
	child = ast->children.first;
	while ( (child != NULL) )
	{
		if ( (child->type == _ast::AST_FUN) )
		{
			if ( (_ast::is_node_flag(child, _ast::NODE_FLAG_PUBLIC) == is_public) )
			{
				_generator::generate_function_prototype(ctx, child);
			}
		}
		child = child->next;
	}
}
static void generate_structures(_generator::Generator* ctx, _ast::Node* ast, bool is_public)
{
	_emitter::emit((&ctx->emit), "// Structures\n");
	_generator::reset_generated_flag_on_structures(ctx, ast);
	bool generated;
	generated = true;
	while ( generated )
	{
		generated = false;
		_ast::Node* child;
		child = ast->children.first;
		while ( (child != NULL) )
		{
			if ( ((child->type == _ast::AST_STRUCT) && (_ast::is_node_flag(child, _ast::NODE_FLAG_PUBLIC) == is_public)) )
			{
				if ( ((!_ast::is_node_flag(child, _ast::NODE_FLAG_GENERATED)) && _generator::all_members_defined(ctx, ast, child)) )
				{
					ctx->current_fun_or_struct = child;
					_generator::generate_structure(ctx, child);
					ctx->current_fun_or_struct = NULL;
					generated = true;
					_ast::set_node_flag(child, _ast::NODE_FLAG_GENERATED);
				}
			}
			child = child->next;
		}
	}
}
static void reset_generated_flag_on_structures(_generator::Generator* ctx, _ast::Node* ast)
{
	_ast::Node* child;
	child = ast->children.first;
	while ( (child != NULL) )
	{
		if ( (child->type == _ast::AST_STRUCT) )
		{
			_ast::reset_node_flag(child, _ast::NODE_FLAG_GENERATED);
		}
		child = child->next;
	}
}
static bool all_members_defined(_generator::Generator* ctx, _ast::Node* ast, _ast::Node* struct_node)
{
	_ast::Node* member_node;
	member_node = struct_node->children.first;
	while ( (member_node != NULL) )
	{
		_ast::Node* typenode;
		typenode = member_node->children.first;
		_token::Token* typetok;
		typetok = (&typenode->data);
		if ( ((((member_node->type == _ast::AST_VAR) && (typetok->type == _token::TOK_ID)) && (!_ast::is_node_flag(typenode, _ast::NODE_FLAG_POINTER))) && (!_generator::is_member_generic(ctx, struct_node, member_node))) )
		{
			_generator::Symbol membertype;
			membertype = _generator::get_struct(ctx, typetok->text);
			if ( (membertype.node == NULL) )
			{
				_generator::generator_exit_error(ctx, ("undefined structure member type " + typetok->text), typetok->pos);
			}
			if ( ((!_ast::is_node_flag(membertype.node, _ast::NODE_FLAG_GENERATED)) && (!_ast::is_node_flag(membertype.node, _ast::NODE_FLAG_IMPORTED))) )
			{
				return false;
			}
		}
		member_node = member_node->next;
	}
	return true;
}
static bool is_member_generic(_generator::Generator* ctx, _ast::Node* struct_node, _ast::Node* member_node)
{
	ecc_assert((struct_node->type == _ast::AST_STRUCT), "struct node is a structure definition");
	ecc_assert((member_node->type == _ast::AST_VAR), "member node is a variable definition");
	return _generator::is_type_generic(struct_node, member_node, member_node->children.first, NULL);
}
static bool is_type_generic(_ast::Node* parent_node, _ast::Node* child_node, _ast::Node* child_type, _ast::Node* generic_types)
{
	if ( _ast::is_node_flag(child_node, _ast::NODE_FLAG_GENERIC) )
	{
		return true;
	}
	if ( _ast::is_node_flag(child_node, _ast::NODE_FLAG_SPECIFIC) )
	{
		return false;
	}
	if ( (child_type->data.type == _token::TOK_ID) )
	{
		if ( (generic_types == NULL) )
		{
			generic_types = _ast::find_child_of_type(parent_node, _ast::AST_GENERIC_PARAMS);
		}
		if ( (generic_types != NULL) )
		{
			_ast::Node* generic_type;
			generic_type = generic_types->children.first;
			while ( (generic_type != NULL) )
			{
				if ( (generic_type->data.text == child_type->data.text) )
				{
					_ast::set_node_flag(child_node, _ast::NODE_FLAG_GENERIC);
					return true;
				}
				generic_type = generic_type->next;
			}
		}
	}
	_ast::set_node_flag(child_node, _ast::NODE_FLAG_SPECIFIC);
	return false;
}
static _ast::Node* get_node_in_ast(_ast::Node* ast, ecc_string name, int container_type, int type)
{
	ecc_assert((ast != NULL), "not null");
	_ast::Node* child;
	child = ast->children.first;
	while ( (child != NULL) )
	{
		if ( (((type == (-1)) || (child->type == type)) && (child->data.text == name)) )
		{
			return child;
		}
		if ( (child->type == container_type) )
		{
			_ast::Node* n;
			n = _generator::get_node_in_ast(child, name, type, type);
			if ( (n != NULL) )
			{
				return n;
			}
		}
		child = child->next;
	}
	return NULL;
}
static _generator::Symbol get_node_for_name(_generator::Generator* ctx, ecc_string symbol_name, int container_type, int symbol_type)
{
	_generator::Symbol symbol;
	symbol.node = _generator::get_node_in_ast(ctx->ast, symbol_name, container_type, symbol_type);
	if ( (symbol.node != NULL) )
	{
		symbol.module_name = ctx->module_name;
		return symbol;
	}
	_log::log_start_debug((&ctx->log), "NON-LOCAL symbol used, searching in imported modules.");
	_log::log_add_debug((&ctx->log), " Name=");
	_log::log_add_debug((&ctx->log), symbol_name);
	_log::log_add_debug((&ctx->log), " Type=");
	_log::log_end_debug((&ctx->log), _ast::ast_type_string(symbol_type));
	for ( int i = 0; i < ctx->modules->n_used_modules; ++i )
	{
		_modules::Module* module;
		module = (&ctx->modules->used_modules[i]);
		symbol.node = _generator::get_node_in_ast(module->ast, symbol_name, container_type, symbol_type);
		if ( (symbol.node != NULL) )
		{
			_log::log_start_debug((&ctx->log), "FOUND SYMBOL in module: ");
			_log::log_end_debug((&ctx->log), module->name);
			symbol.module_name = module->name;
			symbol.is_imported = true;
			break;
		}
	}
	if ( (symbol.node == NULL) )
	{
		_log::log_debug((&ctx->log), "WARNING: could not find symbol in any module");
	}
	return symbol;
}
static _generator::Symbol get_struct(_generator::Generator* ctx, ecc_string type_name)
{
	return _generator::get_node_for_name(ctx, type_name, _ast::AST_STRUCT, _ast::AST_STRUCT);
}
static _generator::Symbol get_fun(_generator::Generator* ctx, ecc_string fun_name)
{
	return _generator::get_node_for_name(ctx, fun_name, _ast::AST_FUN, _ast::AST_FUN);
}
static _generator::Symbol get_const(_generator::Generator* ctx, ecc_string const_name)
{
	return _generator::get_node_for_name(ctx, const_name, _ast::AST_CONST_LIST, _ast::AST_CONST);
}
static void generate_structure(_generator::Generator* ctx, _ast::Node* child)
{
	if ( _ast::is_node_flag(child, _ast::NODE_FLAG_GENERIC) )
	{
		_generator::generate_template(ctx, child);
	}
	_emitter::emit((&ctx->emit), "struct ");
	_emitter::emit((&ctx->emit), child->data.text);
	_emitter::emit((&ctx->emit), "\n{\n");
	_emitter::emit_inc_indent((&ctx->emit));
	_ast::Node* member;
	member = child->children.first;
	while ( (member != NULL) )
	{
		if ( (member->type == _ast::AST_VAR) )
		{
			_emitter::emit_indent((&ctx->emit));
			_generator::generate_var(ctx, member);
		}
		member = member->next;
	}
	_emitter::emit_line((&ctx->emit), (child->data.text + "()"));
	_emitter::emit_line((&ctx->emit), "{");
	_emitter::emit_inc_indent((&ctx->emit));
	member = child->children.first;
	while ( (member != NULL) )
	{
		if ( (member->type == _ast::AST_VAR) )
		{
			if ( ((member->children.count > 1) && _ast::is_node_flag(member->children.last, _ast::NODE_FLAG_EXPR_INIT)) )
			{
				_emitter::emit_indent((&ctx->emit));
				_emitter::emit((&ctx->emit), (member->data.text + " = "));
				_generator::generate_expr(ctx, member->children.last);
				_emitter::emit((&ctx->emit), ";\n");
			}
		}
		member = member->next;
	}
	_emitter::emit_dec_indent((&ctx->emit));
	_emitter::emit_line((&ctx->emit), "}");
	_emitter::emit_dec_indent((&ctx->emit));
	_emitter::emit((&ctx->emit), "};\n");
}
static void generate_main(_generator::Generator* ctx)
{
	_ast::Node* mainfun;
	mainfun = _generator::get_node_in_ast(ctx->ast, "main", _ast::AST_FUN, _ast::AST_FUN);
	if ( (mainfun != NULL) )
	{
		_emitter::emit((&ctx->emit), "#define ECC_MAIN_FUNCTION ");
		_emitter::emit((&ctx->emit), ctx->module_name);
		_emitter::emit((&ctx->emit), "::ecc_main\n");
		if ( (_generator::param_count(mainfun) == 0) )
		{
			_emitter::emit_line((&ctx->emit), "#include <ecc_main_void.h>");
		}
		else if ( (_generator::param_count(mainfun) == 2) )
		{
			_emitter::emit_line((&ctx->emit), "#include <ecc_main_args.h>");
		}
		else
		{
			_generator::generator_exit_error(ctx, "incorrect signature for main function", mainfun->data.pos);
		}
	}
}
static int param_count(_ast::Node* func)
{
	return _generator::child_count(func, _ast::AST_PARAM);
}
static int child_count(_ast::Node* root, int child_type)
{
	int count;
	count = 0;
	_ast::Node* child;
	child = root->children.first;
	while ( (child != NULL) )
	{
		if ( (child->type == child_type) )
		{
			count += 1;
		}
		child = child->next;
	}
	return count;
}
static void generate_functions(_generator::Generator* ctx, _ast::Node* ast, bool is_generic, bool is_public)
{
	_ast::Node* child;
	child = ast->children.first;
	while ( (child != NULL) )
	{
		if ( (((child->type == _ast::AST_FUN) && (_ast::is_node_flag(child, _ast::NODE_FLAG_GENERIC) == is_generic)) && (_ast::is_node_flag(child, _ast::NODE_FLAG_PUBLIC) == is_public)) )
		{
			ctx->current_fun_or_struct = child;
			_generator::generate_function_header(ctx, child);
			_emitter::emit((&ctx->emit), "\n");
			_generator::generate_block(ctx, _ast::find_child_of_type(child, _ast::AST_BLOCK));
			ctx->current_fun_or_struct = NULL;
		}
		child = child->next;
	}
}
static void generate_function_definitions(_generator::Generator* ctx, _ast::Node* ast)
{
	_emitter::emit((&ctx->emit), "// Function definitions (private generic and all non-generic)\n");
	_generator::generate_functions(ctx, ast, false, true);
	_generator::generate_functions(ctx, ast, false, false);
	_generator::generate_functions(ctx, ast, true, false);
}
static void generate_block(_generator::Generator* ctx, _ast::Node* block)
{
	ecc_assert((block->type == _ast::AST_BLOCK), "block type");
	_emitter::emit_line((&ctx->emit), "{");
	_emitter::emit_inc_indent((&ctx->emit));
	_ast::Node* child;
	child = block->children.first;
	while ( (child != NULL) )
	{
		_emitter::emit_indent((&ctx->emit));
		if ( (child->type == _ast::AST_RETURN) )
		{
			_generator::generate_return(ctx, child);
		}
		else if ( (child->type == _ast::AST_BREAK) )
		{
			_emitter::emit((&ctx->emit), "break;\n");
		}
		else if ( (child->type == _ast::AST_CONTINUE) )
		{
			_emitter::emit((&ctx->emit), "continue;\n");
		}
		else if ( (child->type == _ast::AST_IF) )
		{
			_generator::generate_if(ctx, child);
		}
		else if ( (child->type == _ast::AST_WHILE) )
		{
			_generator::generate_while(ctx, child);
		}
		else if ( (child->type == _ast::AST_FOR) )
		{
			_generator::generate_for(ctx, child);
		}
		else if ( (child->type == _ast::AST_VAR) )
		{
			_generator::generate_var(ctx, child);
		}
		else
		{
			_generator::generate_expr(ctx, child);
			_emitter::emit((&ctx->emit), ";\n");
		}
		child = child->next;
	}
	_emitter::emit_dec_indent((&ctx->emit));
	_emitter::emit_line((&ctx->emit), "}");
}
static bool is_user_type(_ast::Node* type_node)
{
	return (type_node->data.type == _token::TOK_ID);
}
static void generate_return(_generator::Generator* ctx, _ast::Node* n)
{
	_emitter::emit((&ctx->emit), "return ");
	if ( (n->children.count != 0) )
	{
		ecc_assert((n->children.count == 1), "return statement takes one expression");
		_generator::generate_expr(ctx, n->children.first);
	}
	_emitter::emit((&ctx->emit), ";\n");
}
static void generate_if(_generator::Generator* ctx, _ast::Node* n)
{
	_emitter::emit((&ctx->emit), "if ( ");
	_ast::Node* child;
	child = n->children.first;
	_generator::generate_expr(ctx, child);
	_emitter::emit((&ctx->emit), " )\n");
	_generator::generate_block(ctx, child->next);
	child = child->next->next;
	while ( ((child != NULL) && (!_ast::is_node_flag(child, _ast::NODE_FLAG_ELSE))) )
	{
		_emitter::emit_indent((&ctx->emit));
		_emitter::emit((&ctx->emit), "else if ( ");
		_generator::generate_expr(ctx, child);
		_emitter::emit((&ctx->emit), " )\n");
		_generator::generate_block(ctx, child->next);
		child = child->next->next;
	}
	if ( ((child != NULL) && _ast::is_node_flag(child, _ast::NODE_FLAG_ELSE)) )
	{
		_emitter::emit_line((&ctx->emit), "else");
		_generator::generate_block(ctx, child);
		child = child->next;
	}
}
static void generate_while(_generator::Generator* ctx, _ast::Node* n)
{
	ecc_assert((n->children.count == 2), "while statement takes one expression and one body");
	_emitter::emit((&ctx->emit), "while ( ");
	_generator::generate_expr(ctx, n->children.first);
	_emitter::emit((&ctx->emit), " )\n");
	_generator::generate_block(ctx, n->children.last);
}
static void generate_for(_generator::Generator* ctx, _ast::Node* n)
{
	ecc_assert((n->children.count == 4), "for statement takes one id, two expressions and one body");
	_ast::Node* decl;
	decl = n->children.first;
	ecc_assert((decl->type == _ast::AST_VAR), "first child is the declaration of the loop counter variable");
	_ast::Node* decl_type;
	decl_type = decl->children.first;
	ecc_assert((decl_type->type == _ast::AST_VAR_TYPE), "loop counter type");
	_emitter::emit((&ctx->emit), "for ( ");
	_emitter::emit((&ctx->emit), _generator::ec_type_to_c_type_token(ctx, (&decl_type->data)));
	_emitter::emit((&ctx->emit), " ");
	_emitter::emit((&ctx->emit), decl->data.text);
	_emitter::emit((&ctx->emit), " = ");
	_ast::Node* start;
	start = decl->next;
	_generator::generate_expr(ctx, start);
	_emitter::emit((&ctx->emit), "; ");
	_emitter::emit((&ctx->emit), decl->data.text);
	_emitter::emit((&ctx->emit), " < ");
	_ast::Node* end;
	end = start->next;
	_generator::generate_expr(ctx, end);
	_emitter::emit((&ctx->emit), "; ++");
	_emitter::emit((&ctx->emit), decl->data.text);
	_emitter::emit((&ctx->emit), " )\n");
	_generator::generate_block(ctx, n->children.last);
}
static void generate_expr(_generator::Generator* ctx, _ast::Node* expr)
{
	_generator::generate_expr0(ctx, expr, NULL);
}
static void generate_expr0(_generator::Generator* ctx, _ast::Node* expr, _generator::Eval* eval)
{
	if ( (expr->type == _ast::AST_EXPR_CONST) )
	{
		if ( (expr->data.type == _token::TOK_LITERAL_STRING) )
		{
			_generator::generate_string_literal(ctx, expr);
		}
		else if ( (expr->data.type == _token::TOK_NIL) )
		{
			_emitter::emit((&ctx->emit), "NULL");
		}
		else if ( (expr->data.type == _token::TOK_TRUE) )
		{
			_emitter::emit((&ctx->emit), "true");
		}
		else if ( (expr->data.type == _token::TOK_FALSE) )
		{
			_emitter::emit((&ctx->emit), "false");
		}
		else
		{
			_emitter::emit((&ctx->emit), expr->data.text);
		}
	}
	else if ( (expr->type == _ast::AST_EXPR_ID) )
	{
		if ( (expr->data.text == "eof") )
		{
			_emitter::emit((&ctx->emit), "ecc_eof");
		}
		else
		{
			_generator::Symbol decl;
			decl.node = _generator::find_var_for_id(ctx, expr);
			if ( (decl.node == NULL) )
			{
				decl = _generator::get_const(ctx, expr->data.text);
			}
			if ( (decl.node == NULL) )
			{
				decl = _generator::get_fun(ctx, expr->data.text);
			}
			if ( (decl.node == NULL) )
			{
				_generator::generator_exit_error(ctx, ("undeclared identifier " + expr->data.text), expr->data.pos);
			}
			if ( decl.is_imported )
			{
				_emitter::emit((&ctx->emit), decl.module_name);
				_emitter::emit((&ctx->emit), "::");
			}
			_emitter::emit((&ctx->emit), expr->data.text);
			if ( (eval != NULL) )
			{
				eval->decl = decl.node;
			}
		}
	}
	else if ( (expr->type == _ast::AST_EXPR_ASSIGN) )
	{
		_generator::generate_expr0(ctx, expr->children.first, eval);
		_emitter::emit((&ctx->emit), ((" " + _token::token_type_string(expr->data.type)) + " "));
		_generator::generate_expr0(ctx, expr->children.last, eval);
	}
	else if ( (expr->type == _ast::AST_EXPR_BINARY) )
	{
		_emitter::emit((&ctx->emit), "(");
		_generator::generate_expr0(ctx, expr->children.first, eval);
		_emitter::emit((&ctx->emit), ((" " + _token::token_type_string(expr->data.type)) + " "));
		_generator::generate_expr0(ctx, expr->children.last, eval);
		_emitter::emit((&ctx->emit), ")");
	}
	else if ( (expr->type == _ast::AST_EXPR_UNARY) )
	{
		_emitter::emit((&ctx->emit), "(");
		_emitter::emit((&ctx->emit), _token::token_type_string(expr->data.type));
		_generator::generate_expr0(ctx, expr->children.first, eval);
		_emitter::emit((&ctx->emit), ")");
	}
	else if ( (expr->type == _ast::AST_EXPR_ARRAY) )
	{
		_generator::generate_array_expr(ctx, expr, eval);
	}
	else if ( (expr->type == _ast::AST_EXPR_DOT) )
	{
		_generator::generate_dot_expr(ctx, expr, eval);
	}
	else if ( (expr->type == _ast::AST_CALL_MODULE) )
	{
		ecc_string* module;
		module = (&expr->children.first->data.text);
		if ( ((*module) != "c") )
		{
			_emitter::emit((&ctx->emit), _utils::get_module_name_from_file_name(expr->children.first->data.text));
		}
		_emitter::emit((&ctx->emit), "::");
		_generator::generate_call_user(ctx, expr->children.last, eval);
	}
	else
	{
		ecc_assert((expr->type == _ast::AST_CALL), _ast::ast_type_string(expr->type));
		_generator::generate_call(ctx, expr, eval);
	}
}
static void generate_array_expr(_generator::Generator* ctx, _ast::Node* expr, _generator::Eval* eval)
{
	ecc_assert((expr->type == _ast::AST_EXPR_ARRAY), _ast::ast_type_string(expr->type));
	_generator::generate_expr0(ctx, expr->children.first, eval);
	_emitter::emit((&ctx->emit), "[");
	_generator::generate_expr0(ctx, expr->children.last, eval);
	_emitter::emit((&ctx->emit), "]");
}
static void generate_dot_expr(_generator::Generator* ctx, _ast::Node* expr, _generator::Eval* eval_return)
{
	ecc_assert((expr != NULL), "expression is not nil");
	ecc_assert((expr->type == _ast::AST_EXPR_DOT), _ast::ast_type_string(expr->type));
	_generator::Eval eval;
	_generator::generate_expr0(ctx, expr->children.first, (&eval));
	ecc_assert((eval.decl != NULL), "type of left side of dot expression is known");
	if ( _ast::is_node_flag(eval.decl, _ast::NODE_FLAG_POINTER) )
	{
		_emitter::emit((&ctx->emit), "->");
	}
	else
	{
		_emitter::emit((&ctx->emit), ".");
	}
	_emitter::emit((&ctx->emit), expr->children.last->data.text);
	if ( (eval_return != NULL) )
	{
		ecc_string type_name;
		type_name = eval.decl->data.text;
		_generator::Symbol typedecl;
		typedecl = _generator::get_struct(ctx, type_name);
		if ( (typedecl.node == NULL) )
		{
			_generator::generator_exit_error(ctx, ("undefined type " + type_name), eval.decl->data.pos);
		}
		ecc_string member_name;
		member_name = expr->children.last->data.text;
		_ast::Node* memberdecl;
		memberdecl = _ast::find_child_for_id(typedecl.node, member_name);
		ecc_assert((memberdecl != NULL), member_name);
		eval_return->decl = memberdecl->children.first;
	}
}
static _ast::Node* find_var_for_id(_generator::Generator* ctx, _ast::Node* id)
{
	ecc_assert((id != NULL), "expression is not nil");
	ecc_string name;
	name = id->data.text;
	_ast::Node* node;
	node = id->prev;
	_ast::Node* parent;
	parent = id->parent;
	while ( true )
	{
		while ( (node != NULL) )
		{
			if ( (((node->type == _ast::AST_VAR) || (node->type == _ast::AST_PARAM)) && (node->data.text == name)) )
			{
				return node->children.first;
			}
			node = node->prev;
		}
		node = parent;
		if ( (node == NULL) )
		{
			return NULL;
		}
		parent = node->parent;
	}
}
static void generate_string_literal(_generator::Generator* ctx, _ast::Node* expr)
{
	int length;
	length = ecc_len(expr->data.text);
	_emitter::emit((&ctx->emit), "\"");
	for ( int i = 0; i < length; ++i )
	{
		int c;
		c = expr->data.text[i];
		if ( (c == ecc_to_char("\n")) )
		{
			_emitter::emit((&ctx->emit), "\\n");
		}
		else if ( (c == ecc_to_char("\r")) )
		{
			_emitter::emit((&ctx->emit), "\\r");
		}
		else if ( (c == ecc_to_char("\t")) )
		{
			_emitter::emit((&ctx->emit), "\\t");
		}
		else if ( (c == ecc_to_char("\\")) )
		{
			_emitter::emit((&ctx->emit), "\\\\");
		}
		else if ( (c == ecc_to_char("\"")) )
		{
			_emitter::emit((&ctx->emit), "\\\"");
		}
		else
		{
			_emitter::emit((&ctx->emit), ecc_to_char(c));
		}
	}
	_emitter::emit((&ctx->emit), "\"");
}
static void generate_call(_generator::Generator* ctx, _ast::Node* expr, _generator::Eval* eval)
{
	if ( (expr->data.text == "print") )
	{
		_generator::generate_call_print(ctx, expr, eval);
	}
	else if ( (expr->data.text == "println") )
	{
		_generator::generate_call_println(ctx, expr, eval);
	}
	else if ( (expr->data.text == "new") )
	{
		_generator::generate_call_new(ctx, expr, eval);
	}
	else if ( (expr->data.text == "delete") )
	{
		_generator::generate_call_delete(ctx, expr, eval);
	}
	else if ( _generator::is_builtin_function(expr->data.text) )
	{
		_generator::generate_call_builtin(ctx, expr, eval);
	}
	else
	{
		_generator::Symbol fundecl;
		fundecl = _generator::get_fun(ctx, expr->data.text);
		if ( (fundecl.node == NULL) )
		{
			_ast::Node* fp;
			fp = _generator::find_var_for_id(ctx, expr);
			ecc_assert((fp != NULL), "function found");
		}
		else
		{
			_emitter::emit((&ctx->emit), fundecl.module_name);
			_emitter::emit((&ctx->emit), "::");
		}
		_generator::generate_call_user(ctx, expr, eval);
		if ( (eval != NULL) )
		{
			eval->decl = _ast::find_child_of_type(fundecl.node, _ast::AST_FUN_TYPE);
		}
	}
}
static bool is_builtin_function(ecc_string f)
{
	return (((((((((f == "assert") || (f == "to_string")) || (f == "to_char")) || (f == "to_int")) || (f == "to_float")) || (f == "len")) || (f == "sizeof")) || (f == "c_str")) || (f == "get_platform"));
}
static void generate_call_user(_generator::Generator* ctx, _ast::Node* expr, _generator::Eval* eval)
{
	_emitter::emit((&ctx->emit), expr->data.text);
	_ast::Node* generic_params;
	generic_params = _ast::find_child_of_type(expr, _ast::AST_GENERIC_PARAMS);
	if ( ((generic_params != NULL) && (generic_params->children.count != 0)) )
	{
		_emitter::emit((&ctx->emit), "<");
		_ast::Node* param;
		param = generic_params->children.first;
		while ( (param != NULL) )
		{
			_generator::generate_param_type(ctx, expr, param, param, generic_params);
			param = param->next;
			if ( (param != NULL) )
			{
				_emitter::emit((&ctx->emit), ",");
			}
		}
		_emitter::emit((&ctx->emit), ">");
	}
	_emitter::emit((&ctx->emit), "(");
	_ast::Node* child;
	child = expr->children.first;
	while ( (child != NULL) )
	{
		if ( (child->type != _ast::AST_GENERIC_PARAMS) )
		{
			_generator::generate_expr0(ctx, child, eval);
			if ( (child->next != NULL) )
			{
				_emitter::emit((&ctx->emit), ", ");
			}
		}
		child = child->next;
	}
	_emitter::emit((&ctx->emit), ")");
}
static void generate_call_builtin(_generator::Generator* ctx, _ast::Node* expr, _generator::Eval* eval)
{
	_emitter::emit((&ctx->emit), ("ecc_" + expr->data.text));
	_emitter::emit((&ctx->emit), "(");
	_ast::Node* child;
	child = expr->children.first;
	if ( (child != NULL) )
	{
		_generator::generate_expr0(ctx, child, eval);
		child = child->next;
	}
	while ( (child != NULL) )
	{
		_emitter::emit((&ctx->emit), ", ");
		_generator::generate_expr0(ctx, child, eval);
		child = child->next;
	}
	_emitter::emit((&ctx->emit), ")");
}
static void generate_call_new(_generator::Generator* ctx, _ast::Node* expr, _generator::Eval* eval)
{
	_emitter::emit((&ctx->emit), "new(");
	if ( (expr->children.count == 1) )
	{
		ecc_assert((expr->children.first->type == _ast::AST_EXPR_ID), "first parameter is a type id");
		_emitter::emit((&ctx->emit), _generator::ec_type_to_c_type(ctx, expr->children.last));
	}
	else if ( (expr->children.count == 2) )
	{
		ecc_assert((expr->children.last->type == _ast::AST_EXPR_ID), "second parameter is a type id");
		_emitter::emit((&ctx->emit), _generator::ec_type_to_c_type(ctx, expr->children.last));
		_emitter::emit((&ctx->emit), "[");
		_generator::generate_expr0(ctx, expr->children.first, eval);
		_emitter::emit((&ctx->emit), "]");
	}
	else
	{
		ecc_assert(false, "new() takes one or two parameters");
	}
	_emitter::emit((&ctx->emit), ")");
}
static void generate_call_delete(_generator::Generator* ctx, _ast::Node* expr, _generator::Eval* eval_return)
{
	ecc_assert((expr->children.count == 1), "delete() takes one parameter");
	_generator::Eval eval;
	_emitter::emit_disable((&ctx->emit));
	_generator::generate_expr0(ctx, expr->children.first, (&eval));
	_emitter::emit_enable((&ctx->emit));
	ecc_assert((eval.decl != NULL), "declaration of pointer to delete is found");
	_emitter::emit((&ctx->emit), "delete");
	if ( _ast::is_node_flag(eval.decl, _ast::NODE_FLAG_ARRAY) )
	{
		_emitter::emit((&ctx->emit), "[]");
	}
	_emitter::emit((&ctx->emit), "(");
	_generator::generate_expr0(ctx, expr->children.first, eval_return);
	_emitter::emit((&ctx->emit), ")");
}
static void generate_call_println(_generator::Generator* ctx, _ast::Node* expr, _generator::Eval* eval)
{
	_generator::generate_call_print(ctx, expr, eval);
	_emitter::emit((&ctx->emit), " << std::endl");
}
static void generate_call_print(_generator::Generator* ctx, _ast::Node* expr, _generator::Eval* eval)
{
	_ast::Node* child;
	child = expr->children.first;
	_emitter::emit((&ctx->emit), "std::cout");
	while ( (child != NULL) )
	{
		_emitter::emit((&ctx->emit), " << ");
		_generator::generate_expr0(ctx, child, eval);
		child = child->next;
	}
}
static void generate_function_prototype(_generator::Generator* ctx, _ast::Node* node)
{
	_generator::generate_function_header(ctx, node);
	_emitter::emit((&ctx->emit), ";\n");
}
static void generate_function_header(_generator::Generator* ctx, _ast::Node* node)
{
	if ( _ast::is_node_flag(node, _ast::NODE_FLAG_GENERIC) )
	{
		_generator::generate_template(ctx, node);
	}
	if ( (!_ast::is_node_flag(node, _ast::NODE_FLAG_PUBLIC)) )
	{
		_emitter::emit((&ctx->emit), "static ");
	}
	_ast::Node* generic_types;
	generic_types = node->children.first;
	ecc_assert((generic_types->type == _ast::AST_GENERIC_PARAMS), "first function child is type parameter list");
	_ast::Node* fun_type;
	fun_type = _ast::find_child_of_type(node, _ast::AST_FUN_TYPE);
	if ( (fun_type != NULL) )
	{
		if ( _generator::is_function_pointer(fun_type) )
		{
			_generator::generate_function_pointer(ctx, node, fun_type, generic_types);
		}
		else
		{
			_generator::generate_param_type(ctx, node, node, fun_type, generic_types);
		}
		_emitter::emit((&ctx->emit), " ");
	}
	else
	{
		_emitter::emit((&ctx->emit), "void ");
	}
	if ( (node->data.text == "main") )
	{
		_emitter::emit((&ctx->emit), "ecc_main");
	}
	else
	{
		_emitter::emit((&ctx->emit), node->data.text);
	}
	_generator::generate_param_list(ctx, node, generic_types);
}
static void generate_param_list(_generator::Generator* ctx, _ast::Node* node, _ast::Node* generic_types)
{
	_emitter::emit((&ctx->emit), "(");
	int n_param;
	n_param = 0;
	_ast::Node* param;
	param = _ast::find_child_of_type(node, _ast::AST_PARAM);
	while ( (param != NULL) )
	{
		if ( (param->type == _ast::AST_PARAM) )
		{
			_ast::Node* type_node;
			type_node = param->children.first;
			if ( (n_param != 0) )
			{
				_emitter::emit((&ctx->emit), ", ");
			}
			if ( _generator::is_function_pointer(type_node) )
			{
				_generator::generate_function_pointer(ctx, param, type_node, generic_types);
			}
			else
			{
				_generator::generate_param_type(ctx, node, param, type_node, generic_types);
				_emitter::emit((&ctx->emit), " ");
				_emitter::emit((&ctx->emit), param->data.text);
			}
			n_param += 1;
		}
		param = param->next;
	}
	if ( (n_param == 0) )
	{
		_emitter::emit((&ctx->emit), "void");
	}
	_emitter::emit((&ctx->emit), ")");
}
static void generate_function_pointer(_generator::Generator* ctx, _ast::Node* var_node, _ast::Node* type_node, _ast::Node* generic_types)
{
	_ast::Node* fun_type;
	fun_type = _ast::find_child_of_type(type_node, _ast::AST_FUN_TYPE);
	if ( (fun_type != NULL) )
	{
		_emitter::emit((&ctx->emit), _generator::ec_type_to_c_type_token(ctx, (&fun_type->data)));
	}
	else
	{
		_emitter::emit((&ctx->emit), "void");
	}
	_emitter::emit((&ctx->emit), " (*");
	_emitter::emit((&ctx->emit), var_node->data.text);
	_emitter::emit((&ctx->emit), ")");
	_generator::generate_param_list(ctx, type_node, generic_types);
}
static bool is_function_pointer(_ast::Node* type_node)
{
	return (type_node->data.type == _token::TOK_FUN);
}
static void generate_var(_generator::Generator* ctx, _ast::Node* var_node)
{
	ecc_assert((var_node->type == _ast::AST_VAR), "node is a variable declaration");
	_ast::Node* parent_node;
	parent_node = ctx->current_fun_or_struct;
	ecc_assert((parent_node != NULL), "current parent node is not nil");
	_ast::Node* type_node;
	type_node = var_node->children.first;
	_ast::Node* generic_types;
	generic_types = _ast::find_child_of_type(parent_node, _ast::AST_GENERIC_PARAMS);
	if ( _generator::is_function_pointer(type_node) )
	{
		_generator::generate_function_pointer(ctx, var_node, type_node, generic_types);
	}
	else
	{
		_generator::generate_type(ctx, parent_node, var_node, type_node, generic_types);
		if ( _ast::is_node_flag(var_node, _ast::NODE_FLAG_POINTER) )
		{
			_emitter::emit((&ctx->emit), "* ");
		}
		else
		{
			_emitter::emit((&ctx->emit), " ");
		}
		_emitter::emit((&ctx->emit), var_node->data.text);
		if ( _ast::is_node_flag(type_node, _ast::NODE_FLAG_ARRAY) )
		{
			if ( (!_ast::is_node_flag(var_node, _ast::NODE_FLAG_POINTER)) )
			{
				_emitter::emit((&ctx->emit), "[");
				ecc_assert((var_node->children.count > 1), "array declaration has explicit size");
				_generator::generate_expr(ctx, var_node->children.last);
				_emitter::emit((&ctx->emit), "]");
			}
			else
			{
				ecc_assert((var_node->children.count == 1), "array type does not include the dimension");
			}
		}
	}
	_emitter::emit((&ctx->emit), ";\n");
}
static void generate_param_type(_generator::Generator* ctx, _ast::Node* parent_node, _ast::Node* decl_node, _ast::Node* type_node, _ast::Node* generic_types)
{
	_generator::generate_type(ctx, parent_node, decl_node, type_node, generic_types);
	if ( _ast::is_node_flag(decl_node, _ast::NODE_FLAG_ARRAY) )
	{
		_emitter::emit((&ctx->emit), "*");
	}
	if ( _ast::is_node_flag(decl_node, _ast::NODE_FLAG_POINTER) )
	{
		_emitter::emit((&ctx->emit), "*");
	}
}
static void generate_type(_generator::Generator* ctx, _ast::Node* parent_node, _ast::Node* decl_node, _ast::Node* type_node, _ast::Node* generic_types)
{
	ecc_assert((parent_node != NULL), "parent node is not nil");
	ecc_assert((decl_node != NULL), "decl node is not nil");
	ecc_assert((type_node != NULL), "type node is not nil");
	if ( ((generic_types != NULL) && _generator::is_type_generic(parent_node, decl_node, type_node, generic_types)) )
	{
		_emitter::emit((&ctx->emit), type_node->data.text);
	}
	else
	{
		_emitter::emit((&ctx->emit), _generator::ec_type_to_c_type_token(ctx, (&type_node->data)));
		_ast::Node* params;
		params = _ast::find_child_of_type(decl_node, _ast::AST_GENERIC_PARAMS);
		if ( ((params != NULL) && (params->children.count != 0)) )
		{
			_emitter::emit((&ctx->emit), "<");
			_ast::Node* param;
			param = params->children.first;
			while ( (param != NULL) )
			{
				if ( ((generic_types != NULL) && (_ast::find_child_for_id(generic_types, param->data.text) != NULL)) )
				{
					_emitter::emit((&ctx->emit), param->data.text);
				}
				else
				{
					_emitter::emit((&ctx->emit), _generator::ec_type_to_c_type(ctx, param));
				}
				param = param->next;
				if ( (param != NULL) )
				{
					_emitter::emit((&ctx->emit), ",");
				}
			}
			_emitter::emit((&ctx->emit), ">");
		}
	}
}
static ecc_string ec_type_to_c_type(_generator::Generator* ctx, _ast::Node* node)
{
	ecc_string str;
	str = _generator::ec_type_to_c_type_token(ctx, (&node->data));
	if ( _ast::is_node_flag(node, _ast::NODE_FLAG_ARRAY) )
	{
		str += "*";
	}
	if ( _ast::is_node_flag(node, _ast::NODE_FLAG_POINTER) )
	{
		str += "*";
	}
	return str;
}
static ecc_string ec_type_to_c_type_token(_generator::Generator* ctx, _token::Token* tok)
{
	if ( (tok->type == _token::TOK_INT) )
	{
		return "int";
	}
	if ( (tok->type == _token::TOK_FLOAT) )
	{
		return "float";
	}
	if ( (tok->type == _token::TOK_BOOL) )
	{
		return "bool";
	}
	if ( (tok->type == _token::TOK_STRING) )
	{
		return "ecc_string";
	}
	if ( (tok->type == _token::TOK_FILE) )
	{
		return "ecc_file";
	}
	ecc_assert((tok->type == _token::TOK_ID), _token::token_to_string(tok));
	_generator::Symbol sym;
	sym = _generator::get_struct(ctx, tok->text);
	return ((sym.module_name + "::") + tok->text);
}
} // namespace
