#pragma once
// Includes
#include <ecc_sys.h>
#include <scanner.h>
#include <emitter.h>
#include <utils.h>
#include <token.h>
#include <position.h>
#include <ast.h>
#include <log.h>
#include <modules.h>
namespace _generator {
// Constants
// Structure Prototypes
struct Generator;
// Structures
struct Generator
{
	_log::Log log;
	_ast::Node* ast;
	_emitter::Emit emit;
	_ast::Node* current_fun_or_struct;
	_modules::Modules* modules;
	ecc_string module_name;
	ecc_string infile_name_no_ext;
	ecc_file outfile_impl;
	ecc_file outfile_symbols;
	Generator()
	{
		ast = NULL;
		current_fun_or_struct = NULL;
		outfile_impl = NULL;
		outfile_symbols = NULL;
	}
};
// Function prototypes
void generate_code(_generator::Generator* ctx);
// Function definitions (public and generic)
} // namespace
