#pragma once
// Includes
#include <ecc_sys.h>
#include <ast.h>
namespace _modules {
// Constants
static const int MAX_USED_MODULES = 256;
static const int MAX_SEARCH_DIRS = 256;
// Structure Prototypes
struct Modules;
struct Module;
// Structures
struct Module
{
	_ast::Node* ast;
	ecc_string name;
	Module()
	{
		ast = NULL;
	}
};
struct Modules
{
	int n_used_modules;
	_modules::Module used_modules[MAX_USED_MODULES];
	int n_search_dirs;
	ecc_string search_dirs[MAX_SEARCH_DIRS];
	Modules()
	{
		n_used_modules = 0;
		n_search_dirs = 0;
	}
};
// Function prototypes
// Function definitions (public and generic)
} // namespace
