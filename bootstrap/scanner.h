#pragma once
// Includes
#include <ecc_sys.h>
#include <token.h>
#include <position.h>
#include <utils.h>
namespace _scanner {
// Constants
static const int CHAR_BUFFER_SIZE = 256;
// Structure Prototypes
struct Scanner;
// Structures
struct Scanner
{
	ecc_file infile;
	_position::Position pos;
	int char_buffer[CHAR_BUFFER_SIZE];
	int char_buffer_size;
	int char_buffer_index;
	bool is_peeked;
	bool is_error;
	ecc_string error_reason;
	_token::Token peeked;
	bool is_prev_eol;
	bool ignore_eol;
	int ignored_eol_since_next;
	Scanner()
	{
		char_buffer_size = 0;
		char_buffer_index = 0;
		is_peeked = false;
		is_error = false;
		is_prev_eol = true;
		ignore_eol = false;
		ignored_eol_since_next = 0;
	}
};
// Function prototypes
_token::Token scanner_next_token(_scanner::Scanner* ctx);
_token::Token scanner_peek_token(_scanner::Scanner* ctx);
void scanner_ignore_newlines(_scanner::Scanner* ctx, bool ignore);
// Function definitions (public and generic)
} // namespace
