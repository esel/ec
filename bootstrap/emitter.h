#pragma once
// Includes
#include <ecc_sys.h>
#include <utils.h>
namespace _emitter {
// Constants
// Structure Prototypes
struct Emit;
// Structures
struct Emit
{
	int indent;
	bool is_enabled;
	ecc_file outfile_current;
	Emit()
	{
		indent = 0;
		is_enabled = true;
		outfile_current = NULL;
	}
};
// Function prototypes
void emit(_emitter::Emit* emit, ecc_string s);
void emit_indent(_emitter::Emit* emit);
void emit_line(_emitter::Emit* emit, ecc_string s);
void emit_inc_indent(_emitter::Emit* emit);
void emit_dec_indent(_emitter::Emit* emit);
void emit_set_outfile(_emitter::Emit* emit, ecc_file f);
ecc_file emit_get_outfile(_emitter::Emit* emit);
void emit_enable(_emitter::Emit* emit);
void emit_disable(_emitter::Emit* emit);
// Function definitions (public and generic)
} // namespace
