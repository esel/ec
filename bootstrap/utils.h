#pragma once
// Includes
#include <ecc_sys.h>
namespace _utils {
// Constants
// Structure Prototypes
// Structures
// Function prototypes
ecc_file fopen_ec(ecc_string path, ecc_string mode);
bool fclose_ec(ecc_file f);
bool fwrite_ec(ecc_file f, ecc_string text);
bool file_is_valid(ecc_file f);
ecc_string find_file_name_no_ext_from_path(ecc_string path);
ecc_string get_module_name_from_file_name(ecc_string file_name);
// Function definitions (public and generic)
} // namespace
