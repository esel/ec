#include "ast.h"
namespace _ast {
// Constants
// Structure Prototypes
// Structures
// Function prototypes
static ecc_string escape_xml(ecc_string text);
static void print_indent(ecc_file f, int level);
// Function definitions (private generic and all non-generic)
ecc_string ast_type_string(int t)
{
	if ( (t == AST_PROG) )
	{
		return "ast_prog";
	}
	if ( (t == AST_USE) )
	{
		return "ast_use";
	}
	if ( (t == AST_STRUCT) )
	{
		return "ast_struct";
	}
	if ( (t == AST_CONST) )
	{
		return "ast_const";
	}
	if ( (t == AST_CONST_LIST) )
	{
		return "ast_const_list";
	}
	if ( (t == AST_FUN) )
	{
		return "ast_fun";
	}
	if ( (t == AST_FUN_TYPE) )
	{
		return "ast_fun_type";
	}
	if ( (t == AST_PARAM) )
	{
		return "ast_param";
	}
	if ( (t == AST_PARAM_TYPE) )
	{
		return "ast_param_type";
	}
	if ( (t == AST_GENERIC_PARAMS) )
	{
		return "ast_generic_params";
	}
	if ( (t == AST_GENERIC_PARAM) )
	{
		return "ast_generic_param";
	}
	if ( (t == AST_BLOCK) )
	{
		return "ast_block";
	}
	if ( (t == AST_WHILE) )
	{
		return "ast_while";
	}
	if ( (t == AST_FOR) )
	{
		return "ast_for";
	}
	if ( (t == AST_IF) )
	{
		return "ast_if";
	}
	if ( (t == AST_CALL_MODULE) )
	{
		return "ast_call_module";
	}
	if ( (t == AST_CALL) )
	{
		return "ast_call";
	}
	if ( (t == AST_VAR) )
	{
		return "ast_var";
	}
	if ( (t == AST_VAR_TYPE) )
	{
		return "ast_var_type";
	}
	if ( (t == AST_RETURN) )
	{
		return "ast_return";
	}
	if ( (t == AST_BREAK) )
	{
		return "ast_break";
	}
	if ( (t == AST_CONTINUE) )
	{
		return "ast_continue";
	}
	if ( (t == AST_EXPR_ASSIGN) )
	{
		return "ast_expr_assign";
	}
	if ( (t == AST_EXPR_BINARY) )
	{
		return "ast_expr_binary";
	}
	if ( (t == AST_EXPR_UNARY) )
	{
		return "ast_expr_unary";
	}
	if ( (t == AST_EXPR_CONST) )
	{
		return "ast_expr_const";
	}
	if ( (t == AST_EXPR_ID) )
	{
		return "ast_expr_id";
	}
	if ( (t == AST_EXPR_ARRAY) )
	{
		return "ast_expr_array";
	}
	if ( (t == AST_EXPR_DOT) )
	{
		return "ast_expr_dot";
	}
	if ( (t == AST_ARRAY_LITERAL) )
	{
		return "ast_array_literal";
	}
	return "UNKNOWN";
}
_ast::Node* new_node(int type)
{
	_ast::Node* node;
	node = new(_ast::Node);
	node->type = type;
	node->data.type = _token::TOK_IGNORE;
	return node;
}
_ast::Node* add_node(_ast::Node* parent, _ast::Node* child)
{
	_ast::NodeList* list;
	list = (&parent->children);
	if ( (list->first == NULL) )
	{
		list->first = child;
		list->last = child;
	}
	else if ( (list->last == NULL) )
	{
		list->last = child;
		list->first->next = child;
		child->prev = list->first;
	}
	else
	{
		child->prev = list->last;
		list->last->next = child;
		list->last = child;
	}
	child->parent = parent;
	list->count += 1;
	return child;
}
_ast::Node* add_new_node(_ast::Node* parent, int child_type, _token::Token data)
{
	_ast::Node* child;
	child = _ast::new_node(child_type);
	child->data = data;
	return _ast::add_node(parent, child);
}
bool is_node_flag(_ast::Node* node, int flag)
{
	ecc_assert((node != NULL), "node is not nil");
	int pow2;
	pow2 = (1 << flag);
	return ((node->flags & pow2) != 0);
}
void set_node_flag(_ast::Node* node, int flag)
{
	ecc_assert((node != NULL), "node is not nil");
	node->flags = _ast::set_flag(node->flags, flag);
}
void reset_node_flag(_ast::Node* node, int flag)
{
	ecc_assert((node != NULL), "node is not nil");
	int mask;
	mask = (~(1 << flag));
	node->flags &= mask;
}
int set_flag(int bits, int bit)
{
	return (bits | (1 << bit));
}
_ast::Node* find_child_of_type(_ast::Node* parent, int child_type)
{
	_ast::Node* child;
	child = parent->children.first;
	while ( (child != NULL) )
	{
		if ( (child->type == child_type) )
		{
			return child;
		}
		child = child->next;
	}
	return NULL;
}
_ast::Node* find_child_for_id(_ast::Node* parent, ecc_string child_name)
{
	ecc_assert((parent != NULL), "parent is not null");
	_ast::Node* child;
	child = parent->children.first;
	while ( (child != NULL) )
	{
		if ( (child->data.text == child_name) )
		{
			return child;
		}
		child = child->next;
	}
	return NULL;
}
void print_ast_json(ecc_file f, _ast::Node* node, int level)
{
	_ast::print_indent(f, level);
	_utils::fwrite_ec(f, "{\"");
	_utils::fwrite_ec(f, _ast::ast_type_string(node->type));
	_utils::fwrite_ec(f, "\": {\n");
	_ast::print_indent(f, (level + 1));
	_utils::fwrite_ec(f, "\"token_type\": \"");
	_utils::fwrite_ec(f, _token::token_type_string(node->data.type));
	_utils::fwrite_ec(f, "\",\n");
	_ast::print_indent(f, (level + 1));
	_utils::fwrite_ec(f, "\"token_line\": \"");
	_utils::fwrite_ec(f, ecc_to_string(node->data.pos.line));
	_utils::fwrite_ec(f, "\",\n");
	_ast::print_indent(f, (level + 1));
	_utils::fwrite_ec(f, "\"token_col\": \"");
	_utils::fwrite_ec(f, ecc_to_string(node->data.pos.col));
	_utils::fwrite_ec(f, "\",\n");
	_ast::print_indent(f, (level + 1));
	_utils::fwrite_ec(f, "\"children\": [\n");
	_ast::Node* child;
	child = node->children.first;
	while ( (child != NULL) )
	{
		_ast::print_ast_json(f, child, (level + 2));
		_utils::fwrite_ec(f, ",\n");
		child = child->next;
	}
	_ast::print_indent(f, (level + 2));
	_utils::fwrite_ec(f, "null\n");
	_ast::print_indent(f, (level + 1));
	_utils::fwrite_ec(f, "]\n");
	_ast::print_indent(f, level);
	_utils::fwrite_ec(f, "}}");
}
void print_ast_xml(ecc_file f, _ast::Node* node, int level)
{
	_ast::print_indent(f, level);
	_utils::fwrite_ec(f, "<");
	_utils::fwrite_ec(f, _ast::ast_type_string(node->type));
	_utils::fwrite_ec(f, " line=\"");
	_utils::fwrite_ec(f, ecc_to_string(node->data.pos.line));
	_utils::fwrite_ec(f, "\"");
	_utils::fwrite_ec(f, " token_text=\"");
	_utils::fwrite_ec(f, _ast::escape_xml(node->data.text));
	_utils::fwrite_ec(f, "\">\n");
	_ast::print_indent(f, (level + 1));
	_utils::fwrite_ec(f, "<token line=\"");
	_utils::fwrite_ec(f, ecc_to_string(node->data.pos.line));
	_utils::fwrite_ec(f, "\" col=\"");
	_utils::fwrite_ec(f, ecc_to_string(node->data.pos.col));
	_utils::fwrite_ec(f, "\"/>\n");
	_ast::Node* child;
	child = node->children.first;
	while ( (child != NULL) )
	{
		_ast::print_ast_xml(f, child, (level + 1));
		child = child->next;
	}
	_ast::print_indent(f, level);
	_utils::fwrite_ec(f, "</");
	_utils::fwrite_ec(f, _ast::ast_type_string(node->type));
	_utils::fwrite_ec(f, ">\n");
}
void destroy_ast(_ast::Node* node)
{
	_ast::Node* child;
	child = node->children.first;
	while ( (child != NULL) )
	{
		_ast::Node* next;
		next = child->next;
		_ast::destroy_ast(child);
		child = next;
	}
	delete(node);
}
static ecc_string escape_xml(ecc_string text)
{
	ecc_string s;
	for ( int i = 0; i < ecc_len(text); ++i )
	{
		int c;
		c = text[i];
		if ( (c == ecc_to_char("<")) )
		{
			s += "&lt;";
		}
		else if ( (c == ecc_to_char(">")) )
		{
			s += "&gt;";
		}
		else if ( (c == ecc_to_char("\"")) )
		{
			s += "&quot;";
		}
		else if ( (c == ecc_to_char("&")) )
		{
			s += "&amp;";
		}
		else
		{
			s += c;
		}
	}
	return s;
}
static void print_indent(ecc_file f, int level)
{
	for ( int i = 0; i < level; ++i )
	{
		_utils::fwrite_ec(f, "  ");
	}
}
} // namespace
