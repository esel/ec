#include "parser.h"
namespace _parser {
// Constants
// Structure Prototypes
// Structures
// Function prototypes
static void parser_exit_mismatch(_parser::Parser* ctx, _token::Token* actual, ecc_string msg);
static void parser_exit_error(_parser::Parser* ctx, ecc_string msg, _position::Position pos);
static void parse_use(_parser::Parser* ctx, _ast::Node* root);
static void import_module(_parser::Parser* ctx, _token::Token* module);
static _ast::Node* import_module_symbols(_parser::Parser* ctx);
static void parse_const_single(_parser::Parser* ctx, _ast::Node* root, bool is_const_list, bool is_single_type, _token::Token single_type, bool is_public);
static void parse_const(_parser::Parser* ctx, _ast::Node* root, _token::Token* consttok);
static _ast::Node* parse_generic_parameters(_parser::Parser* ctx, _ast::Node* node);
static void parse_struct(_parser::Parser* ctx, _ast::Node* root, _token::Token* typetok);
static void parse_fun(_parser::Parser* ctx, _ast::Node* root, _token::Token* funtok);
static void parse_fun_signature(_parser::Parser* ctx, _ast::Node* parent);
static void parse_fun_type(_parser::Parser* ctx, int type, _ast::Node* fnode, _ast::Node* idnode);
static void parse_statement_block(_parser::Parser* ctx, _ast::Node* root, _token::Token* parent);
static void parse_statement_compound(_parser::Parser* ctx, _ast::Node* block, _token::Token* tok);
static void parse_statement_simple(_parser::Parser* ctx, _ast::Node* block, _token::Token* tok);
static void add_int_expr(_parser::Parser* ctx, _ast::Node* parent, int i);
static void parse_statement_var(_parser::Parser* ctx, _ast::Node* parent, bool is_struct_member);
static void parse_conditional(_parser::Parser* ctx, _ast::Node* node, _token::Token* parenttok, bool is_else);
static void parse_statement_if(_parser::Parser* ctx, _ast::Node* root);
static void parse_statement_while(_parser::Parser* ctx, _ast::Node* root);
static void parse_statement_for(_parser::Parser* ctx, _ast::Node* root);
static void parse_statement_return(_parser::Parser* ctx, _ast::Node* root);
static _ast::Node* parse_expr0(_parser::Parser* ctx);
static _ast::Node* parse_expr_rvalue(_parser::Parser* ctx);
static _ast::Node* parse_expr_statement(_parser::Parser* ctx);
static _ast::Node* parse_expr_postfix(_parser::Parser* ctx);
static _ast::Node* parse_expr_call(_parser::Parser* ctx, _token::Token* id);
static _ast::Node* parse_expr_primary(_parser::Parser* ctx);
static _ast::Node* parse_expr_binary(_parser::Parser* ctx);
static _ast::Node* parse_expr_log_or(_parser::Parser* ctx);
static _ast::Node* parse_expr_log_and(_parser::Parser* ctx);
static _ast::Node* parse_expr_bit_or(_parser::Parser* ctx);
static _ast::Node* parse_expr_bit_xor(_parser::Parser* ctx);
static _ast::Node* parse_expr_bit_and(_parser::Parser* ctx);
static _ast::Node* parse_expr_eq_neq(_parser::Parser* ctx);
static _ast::Node* parse_expr_lt_gt(_parser::Parser* ctx);
static _ast::Node* parse_expr_shift(_parser::Parser* ctx);
static _ast::Node* parse_expr_add(_parser::Parser* ctx);
static _ast::Node* parse_expr_mul(_parser::Parser* ctx);
static _ast::Node* new_expr_binary(_token::Token op, _ast::Node* left, _ast::Node* right);
static _ast::Node* parse_expr_unary(_parser::Parser* ctx);
static _token::Token match(_parser::Parser* ctx, int type);
static bool is_token_not_end(_token::Token tok);
static bool is_next_token_not_end(_parser::Parser* ctx);
static bool is_ast_child(_token::Token parent, _token::Token child);
static bool is_next_token_ast_child(_parser::Parser* ctx, _token::Token* parent);
static void match_end(_parser::Parser* ctx);
static bool is_type_token(int type);
static _token::Token match_type(_parser::Parser* ctx);
static _token::Token next_token(_parser::Parser* ctx);
static _token::Token peek_token(_parser::Parser* ctx);
// Function definitions (private generic and all non-generic)
_ast::Node* parse_prog(_parser::Parser* ctx)
{
	ctx->ast = _ast::new_node(_ast::AST_PROG);
	_parser::parse_use(ctx, ctx->ast);
	while ( true )
	{
		_token::Token tok;
		tok = _parser::next_token(ctx);
		if ( (tok.type == _token::TOK_CONST) )
		{
			_parser::parse_const(ctx, ctx->ast, (&tok));
		}
		else if ( (tok.type == _token::TOK_STRUCT) )
		{
			_parser::parse_struct(ctx, ctx->ast, (&tok));
		}
		else if ( (tok.type == _token::TOK_FUN) )
		{
			_parser::parse_fun(ctx, ctx->ast, (&tok));
		}
		else if ( (tok.type == _token::TOK_EOF) )
		{
			break;
		}
		else
		{
			_parser::parser_exit_mismatch(ctx, (&tok), "expected constant, structure or function");
		}
	}
	return ctx->ast;
}
static void parser_exit_mismatch(_parser::Parser* ctx, _token::Token* actual, ecc_string msg)
{
	std::cout << "PARSER ERROR: unexpected token " << _token::token_to_string(actual) << std::endl;
	std::cout << msg << std::endl;
	::exit(1);
}
static void parser_exit_error(_parser::Parser* ctx, ecc_string msg, _position::Position pos)
{
	std::cout << "PARSER ERROR: " << msg << std::endl;
	std::cout << "Near " << _position::pos_to_string((&pos)) << std::endl;
	::exit(1);
}
static void parse_use(_parser::Parser* ctx, _ast::Node* root)
{
	while ( (_parser::peek_token(ctx).type == _token::TOK_USE) )
	{
		_parser::next_token(ctx);
		_token::Token module;
		module = _parser::match(ctx, _token::TOK_ID);
		if ( (_parser::peek_token(ctx).type == _token::TOK_MODULE) )
		{
			_parser::next_token(ctx);
			if ( (_parser::peek_token(ctx).type == _token::TOK_STAR) )
			{
				_parser::next_token(ctx);
				_parser::import_module(ctx, (&module));
			}
			else
			{
				_parser::parser_exit_error(ctx, "TODO only star allowed for now", module.pos);
			}
		}
		_ast::add_new_node(root, _ast::AST_USE, module);
		_parser::match_end(ctx);
	}
}
static void import_module(_parser::Parser* ctx, _token::Token* module)
{
	ecc_string file_name;
	file_name = (module->text + ".ec");
	for ( int i = 0; i < ctx->modules->n_search_dirs; ++i )
	{
		ecc_string path;
		path = ((ctx->modules->search_dirs[i] + "/") + file_name);
		_log::log_start_debug((&ctx->log), "SEARCHING path ");
		_log::log_end_debug((&ctx->log), path);
		ecc_file f;
		f = _utils::fopen_ec(path, "rb");
		if ( (f != NULL) )
		{
			_log::log_start_debug((&ctx->log), "LOADING symbols from file: ");
			_log::log_end_debug((&ctx->log), path);
			int module_index;
			module_index = ctx->modules->n_used_modules;
			_modules::Module* m;
			m = (&ctx->modules->used_modules[module_index]);
			ctx->modules->n_used_modules += 1;
			_parser::Parser ctx2;
			ctx2.scanner.infile = f;
			ctx2.log = ctx->log;
			m->ast = _parser::import_module_symbols((&ctx2));
			m->name = _utils::get_module_name_from_file_name(module->text);
			_utils::fclose_ec(f);
			return ;
		}
	}
	_parser::parser_exit_error(ctx, ("could not find file: " + file_name), module->pos);
}
static _ast::Node* import_module_symbols(_parser::Parser* ctx)
{
	_ast::Node* ast;
	ast = _ast::new_node(_ast::AST_PROG);
	_token::Token tok;
	while ( (tok.type != _token::TOK_EOF) )
	{
		tok = _parser::next_token(ctx);
		bool was_imported;
		was_imported = true;
		if ( ((tok.type == _token::TOK_STRUCT) && (_parser::peek_token(ctx).type == _token::TOK_PUBLIC)) )
		{
			_parser::parse_struct(ctx, ast, (&tok));
		}
		else if ( ((tok.type == _token::TOK_FUN) && (_parser::peek_token(ctx).type == _token::TOK_PUBLIC)) )
		{
			_parser::parse_fun(ctx, ast, (&tok));
		}
		else if ( ((tok.type == _token::TOK_CONST) && (_parser::peek_token(ctx).type == _token::TOK_PUBLIC)) )
		{
			_parser::parse_const(ctx, ast, (&tok));
		}
		else
		{
			was_imported = false;
		}
		if ( was_imported )
		{
			_ast::Node* typenode;
			typenode = ast->children.last;
			_ast::set_node_flag(typenode, _ast::NODE_FLAG_IMPORTED);
			_log::log_start_debug((&ctx->log), "GOT PUBLIC SYMBOL: ");
			_log::log_end_debug((&ctx->log), typenode->data.text);
		}
	}
	return ast;
}
static void parse_const_single(_parser::Parser* ctx, _ast::Node* root, bool is_const_list, bool is_single_type, _token::Token single_type, bool is_public)
{
	_ast::Node* node;
	node = _ast::add_new_node(root, _ast::AST_CONST, _parser::match(ctx, _token::TOK_ID));
	if ( is_public )
	{
		_ast::set_node_flag(node, _ast::NODE_FLAG_PUBLIC);
	}
	if ( is_single_type )
	{
		_ast::add_new_node(node, _ast::AST_VAR_TYPE, single_type);
	}
	else
	{
		_ast::add_new_node(node, _ast::AST_VAR_TYPE, _parser::next_token(ctx));
	}
	_token::Token assign;
	assign = _parser::peek_token(ctx);
	if ( (assign.type == _token::TOK_ASSIGN) )
	{
		_parser::next_token(ctx);
		_ast::add_node(node, _parser::parse_expr_rvalue(ctx));
	}
	_parser::match_end(ctx);
}
static void parse_const(_parser::Parser* ctx, _ast::Node* root, _token::Token* consttok)
{
	_token::Token type;
	bool is_public;
	is_public = false;
	if ( (_parser::peek_token(ctx).type == _token::TOK_PUBLIC) )
	{
		_parser::next_token(ctx);
		is_public = true;
	}
	bool is_single_type;
	is_single_type = false;
	if ( (_parser::peek_token(ctx).type != _token::TOK_COLON) )
	{
		is_single_type = true;
		type = _parser::match_type(ctx);
	}
	_parser::match(ctx, _token::TOK_COLON);
	if ( ((!is_single_type) && _parser::is_next_token_not_end(ctx)) )
	{
		_parser::parse_const_single(ctx, root, false, false, type, is_public);
	}
	else
	{
		_parser::match_end(ctx);
		_ast::Node* node;
		node = _ast::add_new_node(root, _ast::AST_CONST_LIST, (*consttok));
		if ( is_public )
		{
			_ast::set_node_flag(node, _ast::NODE_FLAG_PUBLIC);
		}
		while ( true )
		{
			_parser::parse_const_single(ctx, node, true, is_single_type, type, is_public);
			if ( (!_parser::is_next_token_ast_child(ctx, consttok)) )
			{
				break;
			}
		}
	}
}
static _ast::Node* parse_generic_parameters(_parser::Parser* ctx, _ast::Node* node)
{
	_ast::Node* type_list;
	type_list = _ast::add_node(node, _ast::new_node(_ast::AST_GENERIC_PARAMS));
	if ( (_parser::peek_token(ctx).type == _token::TOK_GENERICS_OPEN) )
	{
		type_list->data = _parser::next_token(ctx);
		_ast::set_node_flag(node, _ast::NODE_FLAG_GENERIC);
		while ( (_parser::peek_token(ctx).type != _token::TOK_GENERICS_CLOSE) )
		{
			_ast::add_new_node(type_list, _ast::AST_GENERIC_PARAM, _parser::match(ctx, _token::TOK_ID));
			if ( (_parser::peek_token(ctx).type != _token::TOK_GENERICS_CLOSE) )
			{
				_parser::match(ctx, _token::TOK_ARGS_DELIM);
			}
		}
		_parser::match(ctx, _token::TOK_GENERICS_CLOSE);
	}
	return type_list;
}
static void parse_struct(_parser::Parser* ctx, _ast::Node* root, _token::Token* typetok)
{
	int flags;
	flags = 0;
	if ( (_parser::peek_token(ctx).type == _token::TOK_PUBLIC) )
	{
		_parser::next_token(ctx);
		flags = _ast::set_flag(flags, _ast::NODE_FLAG_PUBLIC);
	}
	_ast::Node* node;
	node = _ast::add_new_node(root, _ast::AST_STRUCT, _parser::match(ctx, _token::TOK_ID));
	node->flags = flags;
	_parser::parse_generic_parameters(ctx, node);
	_parser::match(ctx, _token::TOK_COLON);
	_parser::match_end(ctx);
	while ( true )
	{
		_parser::parse_statement_var(ctx, node, true);
		if ( (!_parser::is_next_token_ast_child(ctx, typetok)) )
		{
			break;
		}
	}
}
static void parse_fun(_parser::Parser* ctx, _ast::Node* root, _token::Token* funtok)
{
	int flags;
	flags = 0;
	if ( (_parser::peek_token(ctx).type == _token::TOK_PUBLIC) )
	{
		_parser::next_token(ctx);
		flags = _ast::set_flag(flags, _ast::NODE_FLAG_PUBLIC);
	}
	_ast::Node* fnode;
	fnode = _ast::add_new_node(root, _ast::AST_FUN, _parser::match(ctx, _token::TOK_ID));
	fnode->flags = flags;
	_parser::parse_generic_parameters(ctx, fnode);
	_parser::parse_fun_signature(ctx, fnode);
	_parser::match(ctx, _token::TOK_COLON);
	_parser::match_end(ctx);
	_parser::parse_statement_block(ctx, fnode, funtok);
}
static void parse_fun_signature(_parser::Parser* ctx, _ast::Node* parent)
{
	if ( (_parser::peek_token(ctx).type == _token::TOK_ARGS_OPEN) )
	{
		_parser::next_token(ctx);
		while ( (_parser::peek_token(ctx).type != _token::TOK_ARGS_CLOSE) )
		{
			_ast::Node* param;
			param = _ast::add_new_node(parent, _ast::AST_PARAM, _parser::match(ctx, _token::TOK_ID));
			_parser::parse_fun_type(ctx, _ast::AST_PARAM_TYPE, parent, param);
			if ( (_parser::peek_token(ctx).type != _token::TOK_ARGS_CLOSE) )
			{
				_parser::match(ctx, _token::TOK_ARGS_DELIM);
			}
		}
		_parser::match(ctx, _token::TOK_ARGS_CLOSE);
	}
	_token::Token tok;
	tok = _parser::peek_token(ctx);
	if ( (((tok.type != _token::TOK_ARGS_DELIM) && (tok.type != _token::TOK_COLON)) && _parser::is_token_not_end(tok)) )
	{
		_parser::parse_fun_type(ctx, _ast::AST_FUN_TYPE, parent, parent);
	}
}
static void parse_fun_type(_parser::Parser* ctx, int type, _ast::Node* fnode, _ast::Node* idnode)
{
	if ( (_parser::peek_token(ctx).type == _token::TOK_FUN) )
	{
		_ast::Node* typenode;
		typenode = _ast::add_new_node(idnode, type, _parser::next_token(ctx));
		_parser::parse_fun_signature(ctx, typenode);
	}
	else
	{
		int flags;
		flags = 0;
		if ( (_parser::peek_token(ctx).type == _token::TOK_STAR) )
		{
			_parser::next_token(ctx);
			flags = _ast::set_flag(flags, _ast::NODE_FLAG_POINTER);
		}
		_ast::Node* typenode;
		typenode = _ast::add_new_node(idnode, type, _parser::match_type(ctx));
		if ( (_parser::peek_token(ctx).type == _token::TOK_GENERICS_OPEN) )
		{
			_ast::Node* type_list;
			type_list = _ast::add_new_node(idnode, _ast::AST_GENERIC_PARAMS, _parser::next_token(ctx));
			while ( (_parser::peek_token(ctx).type != _token::TOK_GENERICS_CLOSE) )
			{
				_ast::add_new_node(type_list, _ast::AST_GENERIC_PARAM, _parser::match_type(ctx));
			}
			_parser::match(ctx, _token::TOK_GENERICS_CLOSE);
		}
		if ( (_parser::peek_token(ctx).type == _token::TOK_ARRAY_OPEN) )
		{
			_parser::next_token(ctx);
			_parser::match(ctx, _token::TOK_ARRAY_CLOSE);
			flags = _ast::set_flag(flags, _ast::NODE_FLAG_ARRAY);
		}
		typenode->flags = flags;
		idnode->flags |= flags;
	}
}
static void parse_statement_block(_parser::Parser* ctx, _ast::Node* root, _token::Token* parent)
{
	_ast::Node* block;
	block = _ast::add_new_node(root, _ast::AST_BLOCK, (*parent));
	_token::Token tok;
	tok = _parser::peek_token(ctx);
	while ( _parser::is_ast_child((*parent), tok) )
	{
		_parser::parse_statement_compound(ctx, block, (&tok));
		tok = _parser::peek_token(ctx);
	}
}
static void parse_statement_compound(_parser::Parser* ctx, _ast::Node* block, _token::Token* tok)
{
	if ( (tok->type == _token::TOK_VAR) )
	{
		_parser::next_token(ctx);
		_parser::parse_statement_var(ctx, block, false);
	}
	else if ( (tok->type == _token::TOK_IF) )
	{
		_parser::parse_statement_if(ctx, block);
	}
	else if ( (tok->type == _token::TOK_WHILE) )
	{
		_parser::parse_statement_while(ctx, block);
	}
	else if ( (tok->type == _token::TOK_FOR) )
	{
		_parser::parse_statement_for(ctx, block);
	}
	else
	{
		_parser::parse_statement_simple(ctx, block, tok);
	}
}
static void parse_statement_simple(_parser::Parser* ctx, _ast::Node* block, _token::Token* tok)
{
	if ( (tok->type == _token::TOK_RETURN) )
	{
		_parser::parse_statement_return(ctx, block);
	}
	else if ( (tok->type == _token::TOK_BREAK) )
	{
		_ast::add_new_node(block, _ast::AST_BREAK, _parser::next_token(ctx));
	}
	else if ( (tok->type == _token::TOK_CONTINUE) )
	{
		_ast::add_new_node(block, _ast::AST_CONTINUE, _parser::next_token(ctx));
	}
	else
	{
		_ast::add_node(block, _parser::parse_expr_statement(ctx));
	}
	_parser::match_end(ctx);
}
static void add_int_expr(_parser::Parser* ctx, _ast::Node* parent, int i)
{
	_token::Token tok;
	tok.type = _token::TOK_LITERAL_INT;
	tok.int_value = i;
	_ast::add_new_node(parent, _ast::AST_EXPR_CONST, tok);
}
static void parse_statement_var(_parser::Parser* ctx, _ast::Node* parent, bool is_struct_member)
{
	bool is_array;
	is_array = false;
	bool is_array_dim;
	is_array_dim = false;
	bool is_pointer;
	is_pointer = false;
	int flags;
	flags = 0;
	_ast::Node* vnode;
	vnode = _ast::add_new_node(parent, _ast::AST_VAR, _parser::match(ctx, _token::TOK_ID));
	_ast::Node* typenode;
	typenode = _ast::add_node(vnode, _ast::new_node(_ast::AST_VAR_TYPE));
	if ( (_parser::peek_token(ctx).type == _token::TOK_FUN) )
	{
		typenode->data = _parser::next_token(ctx);
		_parser::parse_fun_signature(ctx, typenode);
	}
	else
	{
		if ( (_parser::peek_token(ctx).type == _token::TOK_STAR) )
		{
			_parser::next_token(ctx);
			is_pointer = true;
			flags = _ast::set_flag(flags, _ast::NODE_FLAG_POINTER);
		}
		typenode->data = _parser::match_type(ctx);
		if ( (_parser::peek_token(ctx).type == _token::TOK_GENERICS_OPEN) )
		{
			_ast::Node* generic_types;
			generic_types = _ast::add_new_node(vnode, _ast::AST_GENERIC_PARAMS, _parser::next_token(ctx));
			while ( (_parser::peek_token(ctx).type != _token::TOK_GENERICS_CLOSE) )
			{
				_ast::add_new_node(generic_types, _ast::AST_GENERIC_PARAM, _parser::match_type(ctx));
				if ( (_parser::peek_token(ctx).type != _token::TOK_GENERICS_CLOSE) )
				{
					_parser::match(ctx, _token::TOK_ARGS_DELIM);
				}
			}
			_parser::match(ctx, _token::TOK_GENERICS_CLOSE);
		}
	}
	if ( (_parser::peek_token(ctx).type == _token::TOK_ARRAY_OPEN) )
	{
		_parser::next_token(ctx);
		is_array = true;
		flags = _ast::set_flag(flags, _ast::NODE_FLAG_ARRAY);
		if ( (_parser::peek_token(ctx).type != _token::TOK_ARRAY_CLOSE) )
		{
			is_array_dim = true;
			_ast::Node* dim_expr;
			dim_expr = _ast::add_node(vnode, _parser::parse_expr_rvalue(ctx));
			_ast::set_node_flag(dim_expr, _ast::NODE_FLAG_EXPR_ARRAY_DIM);
		}
		_parser::match(ctx, _token::TOK_ARRAY_CLOSE);
	}
	vnode->flags = flags;
	typenode->flags = flags;
	if ( (_parser::peek_token(ctx).type == _token::TOK_ASSIGN) )
	{
		_token::Token assigntok;
		assigntok = _parser::next_token(ctx);
		_ast::Node* init_expr;
		init_expr = _parser::parse_expr_rvalue(ctx);
		_ast::set_node_flag(init_expr, _ast::NODE_FLAG_EXPR_INIT);
		if ( is_struct_member )
		{
			ecc_assert((parent->type == _ast::AST_STRUCT), "parent is a struct");
			_ast::add_node(vnode, init_expr);
		}
		else
		{
			_ast::Node* init_assign;
			init_assign = _ast::add_new_node(parent, _ast::AST_EXPR_ASSIGN, assigntok);
			_ast::Node* init_id;
			init_id = _ast::add_new_node(init_assign, _ast::AST_EXPR_ID, vnode->data);
			_ast::add_node(init_assign, init_expr);
		}
		if ( (is_array && (!is_array_dim)) )
		{
			_parser::add_int_expr(ctx, vnode, init_expr->children.count);
		}
	}
	else if ( ((is_array && (!is_pointer)) && (!is_array_dim)) )
	{
		_parser::parser_exit_error(ctx, "missing array dimension in previous array declaration", vnode->data.pos);
	}
	_parser::match_end(ctx);
}
static void parse_conditional(_parser::Parser* ctx, _ast::Node* node, _token::Token* parenttok, bool is_else)
{
	if ( (!is_else) )
	{
		_ast::add_node(node, _parser::parse_expr_rvalue(ctx));
	}
	_parser::match(ctx, _token::TOK_COLON);
	_token::Token tok;
	tok = _parser::peek_token(ctx);
	_ast::Node* block;
	if ( _parser::is_token_not_end(tok) )
	{
		block = _ast::add_new_node(node, _ast::AST_BLOCK, (*parenttok));
		_parser::parse_statement_simple(ctx, block, (&tok));
	}
	else
	{
		_parser::next_token(ctx);
		_parser::parse_statement_block(ctx, node, parenttok);
		block = node->children.last;
	}
	if ( is_else )
	{
		_ast::set_node_flag(block, _ast::NODE_FLAG_ELSE);
	}
}
static void parse_statement_if(_parser::Parser* ctx, _ast::Node* root)
{
	_token::Token iftok;
	iftok = _parser::match(ctx, _token::TOK_IF);
	int ifindent;
	ifindent = iftok.pos.indent;
	_ast::Node* node;
	node = _ast::add_new_node(root, _ast::AST_IF, iftok);
	_parser::parse_conditional(ctx, node, (&iftok), false);
	while ( true )
	{
		_token::Token eliftok;
		eliftok = _parser::peek_token(ctx);
		if ( ((eliftok.type == _token::TOK_ELIF) && (eliftok.pos.indent == ifindent)) )
		{
			_parser::next_token(ctx);
			_parser::parse_conditional(ctx, node, (&eliftok), false);
		}
		else
		{
			break;
		}
	}
	_token::Token elsetok;
	elsetok = _parser::peek_token(ctx);
	if ( ((_parser::peek_token(ctx).type == _token::TOK_ELSE) && (elsetok.pos.indent == ifindent)) )
	{
		_parser::next_token(ctx);
		_parser::parse_conditional(ctx, node, (&elsetok), true);
	}
}
static void parse_statement_while(_parser::Parser* ctx, _ast::Node* root)
{
	_token::Token parenttok;
	parenttok = _parser::match(ctx, _token::TOK_WHILE);
	_ast::Node* node;
	node = _ast::add_new_node(root, _ast::AST_WHILE, parenttok);
	_ast::add_node(node, _parser::parse_expr_rvalue(ctx));
	_parser::match(ctx, _token::TOK_COLON);
	_parser::match_end(ctx);
	_parser::parse_statement_block(ctx, node, (&parenttok));
}
static void parse_statement_for(_parser::Parser* ctx, _ast::Node* root)
{
	_token::Token parenttok;
	parenttok = _parser::match(ctx, _token::TOK_FOR);
	_ast::Node* node;
	node = _ast::add_new_node(root, _ast::AST_FOR, parenttok);
	_ast::Node* vnode;
	vnode = _ast::add_new_node(node, _ast::AST_VAR, _parser::match(ctx, _token::TOK_ID));
	_token::Token type_token;
	type_token.type = _token::TOK_INT;
	_ast::add_new_node(vnode, _ast::AST_VAR_TYPE, type_token);
	_parser::match(ctx, _token::TOK_IN);
	_ast::add_node(node, _parser::parse_expr_rvalue(ctx));
	_parser::match(ctx, _token::TOK_ARGS_DELIM);
	_ast::add_node(node, _parser::parse_expr_rvalue(ctx));
	_parser::match(ctx, _token::TOK_COLON);
	_parser::match_end(ctx);
	_parser::parse_statement_block(ctx, node, (&parenttok));
}
static void parse_statement_return(_parser::Parser* ctx, _ast::Node* root)
{
	_ast::Node* rnode;
	rnode = _ast::add_new_node(root, _ast::AST_RETURN, _parser::match(ctx, _token::TOK_RETURN));
	if ( _parser::is_next_token_not_end(ctx) )
	{
		_ast::add_node(rnode, _parser::parse_expr_rvalue(ctx));
	}
}
static _ast::Node* parse_expr0(_parser::Parser* ctx)
{
	return _parser::parse_expr_binary(ctx);
}
static _ast::Node* parse_expr_rvalue(_parser::Parser* ctx)
{
	_scanner::scanner_ignore_newlines((&ctx->scanner), true);
	_ast::Node* expr;
	expr = _parser::parse_expr0(ctx);
	_scanner::scanner_ignore_newlines((&ctx->scanner), false);
	return expr;
}
static _ast::Node* parse_expr_statement(_parser::Parser* ctx)
{
	_ast::Node* e;
	e = _parser::parse_expr_rvalue(ctx);
	_token::Token t;
	t = _parser::peek_token(ctx);
	if ( (((((t.type == _token::TOK_ASSIGN) || (t.type == _token::TOK_ASSIGN_INC)) || (t.type == _token::TOK_ASSIGN_DEC)) || (t.type == _token::TOK_ASSIGN_OR)) || (t.type == _token::TOK_ASSIGN_AND)) )
	{
		_parser::next_token(ctx);
		_ast::Node* lvalue;
		lvalue = e;
		_ast::Node* rvalue;
		rvalue = _parser::parse_expr_rvalue(ctx);
		e = _ast::new_node(_ast::AST_EXPR_ASSIGN);
		e->data = t;
		_ast::add_node(e, lvalue);
		_ast::add_node(e, rvalue);
	}
	return e;
}
static _ast::Node* parse_expr_postfix(_parser::Parser* ctx)
{
	_ast::Node* e;
	e = _parser::parse_expr_primary(ctx);
	while ( true )
	{
		_token::Token t;
		t = _parser::peek_token(ctx);
		if ( (t.type == _token::TOK_MODULE) )
		{
			if ( (e->type != _ast::AST_EXPR_ID) )
			{
				_parser::parser_exit_mismatch(ctx, (&t), "invalid module specifier");
			}
			_parser::next_token(ctx);
			_token::Token funcname;
			funcname = _parser::next_token(ctx);
			_ast::Node* call;
			call = _parser::parse_expr_call(ctx, (&funcname));
			_ast::Node* modulecall;
			modulecall = _ast::new_node(_ast::AST_CALL_MODULE);
			_ast::add_node(modulecall, e);
			_ast::add_node(modulecall, call);
			e = modulecall;
		}
		else if ( ((t.type == _token::TOK_ARGS_OPEN) || (t.type == _token::TOK_GENERICS_OPEN)) )
		{
			if ( (e->type != _ast::AST_EXPR_ID) )
			{
				_parser::parser_exit_mismatch(ctx, (&t), "malformed function call");
			}
			_ast::Node* call;
			call = _parser::parse_expr_call(ctx, (&e->data));
			_ast::destroy_ast(e);
			e = call;
		}
		else if ( (t.type == _token::TOK_ARRAY_OPEN) )
		{
			_ast::Node* node;
			node = _ast::new_node(_ast::AST_EXPR_ARRAY);
			node->data = _parser::next_token(ctx);
			_ast::add_node(node, e);
			_ast::add_node(node, _parser::parse_expr0(ctx));
			e = node;
			_parser::match(ctx, _token::TOK_ARRAY_CLOSE);
		}
		else if ( (t.type == _token::TOK_DOT) )
		{
			_ast::Node* node;
			node = _ast::new_node(_ast::AST_EXPR_DOT);
			node->data = _parser::next_token(ctx);
			_ast::add_node(node, e);
			_ast::Node* id;
			id = _ast::add_new_node(node, _ast::AST_EXPR_ID, _parser::match(ctx, _token::TOK_ID));
			e = node;
		}
		else
		{
			break;
		}
	}
	return e;
}
static _ast::Node* parse_expr_call(_parser::Parser* ctx, _token::Token* id)
{
	ecc_assert((id->type == _token::TOK_ID), "token is an identifier");
	_ast::Node* cnode;
	cnode = _ast::new_node(_ast::AST_CALL);
	cnode->data = (*id);
	if ( (_parser::peek_token(ctx).type == _token::TOK_GENERICS_OPEN) )
	{
		_ast::set_node_flag(cnode, _ast::NODE_FLAG_GENERIC);
		_ast::Node* generic_types;
		generic_types = _ast::add_new_node(cnode, _ast::AST_GENERIC_PARAMS, _parser::next_token(ctx));
		while ( (_parser::peek_token(ctx).type != _token::TOK_GENERICS_CLOSE) )
		{
			_ast::add_new_node(generic_types, _ast::AST_GENERIC_PARAM, _parser::match_type(ctx));
		}
		_parser::match(ctx, _token::TOK_GENERICS_CLOSE);
	}
	_parser::match(ctx, _token::TOK_ARGS_OPEN);
	_token::Token next;
	next = _parser::peek_token(ctx);
	while ( (next.type != _token::TOK_ARGS_CLOSE) )
	{
		if ( (((next.type == _token::TOK_INT) || (next.type == _token::TOK_FLOAT)) || (next.type == _token::TOK_STRING)) )
		{
			_ast::add_new_node(cnode, _ast::AST_EXPR_ID, _parser::next_token(ctx));
		}
		else
		{
			_ast::add_node(cnode, _parser::parse_expr0(ctx));
		}
		if ( (_parser::peek_token(ctx).type != _token::TOK_ARGS_CLOSE) )
		{
			_parser::match(ctx, _token::TOK_ARGS_DELIM);
		}
		next = _parser::peek_token(ctx);
	}
	_parser::match(ctx, _token::TOK_ARGS_CLOSE);
	return cnode;
}
static _ast::Node* parse_expr_primary(_parser::Parser* ctx)
{
	_token::Token t;
	t = _parser::next_token(ctx);
	_ast::Node* node;
	node = NULL;
	bool save_token;
	save_token = true;
	if ( ((((((t.type == _token::TOK_LITERAL_INT) || (t.type == _token::TOK_LITERAL_FLOAT)) || (t.type == _token::TOK_LITERAL_STRING)) || (t.type == _token::TOK_NIL)) || (t.type == _token::TOK_TRUE)) || (t.type == _token::TOK_FALSE)) )
	{
		node = _ast::new_node(_ast::AST_EXPR_CONST);
	}
	else if ( (t.type == _token::TOK_ID) )
	{
		node = _ast::new_node(_ast::AST_EXPR_ID);
	}
	else if ( (t.type == _token::TOK_ARGS_OPEN) )
	{
		save_token = false;
		node = _parser::parse_expr0(ctx);
		_parser::match(ctx, _token::TOK_ARGS_CLOSE);
	}
	else if ( (t.type == _token::TOK_ARRAY_OPEN) )
	{
		node = _ast::new_node(_ast::AST_ARRAY_LITERAL);
		_token::Token item;
		item = _parser::peek_token(ctx);
		while ( (item.type != _token::TOK_ARRAY_CLOSE) )
		{
			_ast::add_node(node, _parser::parse_expr0(ctx));
			item = _parser::peek_token(ctx);
			if ( (item.type != _token::TOK_ARRAY_CLOSE) )
			{
				_parser::match(ctx, _token::TOK_ARGS_DELIM);
				item = _parser::peek_token(ctx);
			}
		}
		_parser::match(ctx, _token::TOK_ARRAY_CLOSE);
		t = _parser::match_type(ctx);
	}
	else
	{
		_parser::parser_exit_mismatch(ctx, (&t), "expected a constant, identifier or '('");
	}
	if ( save_token )
	{
		node->data = t;
	}
	return node;
}
static _ast::Node* parse_expr_binary(_parser::Parser* ctx)
{
	return _parser::parse_expr_log_or(ctx);
}
static _ast::Node* parse_expr_log_or(_parser::Parser* ctx)
{
	_ast::Node* expr;
	expr = _parser::parse_expr_log_and(ctx);
	_token::Token op;
	op = _parser::peek_token(ctx);
	while ( (op.type == _token::TOK_LOGICAL_OR) )
	{
		_parser::next_token(ctx);
		expr = _parser::new_expr_binary(op, expr, _parser::parse_expr_log_and(ctx));
		op = _parser::peek_token(ctx);
	}
	return expr;
}
static _ast::Node* parse_expr_log_and(_parser::Parser* ctx)
{
	_ast::Node* expr;
	expr = _parser::parse_expr_bit_or(ctx);
	_token::Token op;
	op = _parser::peek_token(ctx);
	while ( (op.type == _token::TOK_LOGICAL_AND) )
	{
		_parser::next_token(ctx);
		expr = _parser::new_expr_binary(op, expr, _parser::parse_expr_bit_or(ctx));
		op = _parser::peek_token(ctx);
	}
	return expr;
}
static _ast::Node* parse_expr_bit_or(_parser::Parser* ctx)
{
	_ast::Node* expr;
	expr = _parser::parse_expr_bit_xor(ctx);
	_token::Token op;
	op = _parser::peek_token(ctx);
	while ( (op.type == _token::TOK_OR) )
	{
		_parser::next_token(ctx);
		expr = _parser::new_expr_binary(op, expr, _parser::parse_expr_bit_xor(ctx));
		op = _parser::peek_token(ctx);
	}
	return expr;
}
static _ast::Node* parse_expr_bit_xor(_parser::Parser* ctx)
{
	_ast::Node* expr;
	expr = _parser::parse_expr_bit_and(ctx);
	_token::Token op;
	op = _parser::peek_token(ctx);
	while ( (op.type == _token::TOK_XOR) )
	{
		_parser::next_token(ctx);
		expr = _parser::new_expr_binary(op, expr, _parser::parse_expr_bit_and(ctx));
		op = _parser::peek_token(ctx);
	}
	return expr;
}
static _ast::Node* parse_expr_bit_and(_parser::Parser* ctx)
{
	_ast::Node* expr;
	expr = _parser::parse_expr_eq_neq(ctx);
	_token::Token op;
	op = _parser::peek_token(ctx);
	while ( (op.type == _token::TOK_AND) )
	{
		_parser::next_token(ctx);
		expr = _parser::new_expr_binary(op, expr, _parser::parse_expr_eq_neq(ctx));
		op = _parser::peek_token(ctx);
	}
	return expr;
}
static _ast::Node* parse_expr_eq_neq(_parser::Parser* ctx)
{
	_ast::Node* expr;
	expr = _parser::parse_expr_lt_gt(ctx);
	_token::Token op;
	op = _parser::peek_token(ctx);
	while ( ((op.type == _token::TOK_EQ) || (op.type == _token::TOK_NEQ)) )
	{
		_parser::next_token(ctx);
		expr = _parser::new_expr_binary(op, expr, _parser::parse_expr_lt_gt(ctx));
		op = _parser::peek_token(ctx);
	}
	return expr;
}
static _ast::Node* parse_expr_lt_gt(_parser::Parser* ctx)
{
	_ast::Node* expr;
	expr = _parser::parse_expr_shift(ctx);
	_token::Token op;
	op = _parser::peek_token(ctx);
	while ( ((((op.type == _token::TOK_LT) || (op.type == _token::TOK_LTEQ)) || (op.type == _token::TOK_GT)) || (op.type == _token::TOK_GTEQ)) )
	{
		_parser::next_token(ctx);
		expr = _parser::new_expr_binary(op, expr, _parser::parse_expr_shift(ctx));
		op = _parser::peek_token(ctx);
	}
	return expr;
}
static _ast::Node* parse_expr_shift(_parser::Parser* ctx)
{
	_ast::Node* expr;
	expr = _parser::parse_expr_add(ctx);
	_token::Token op;
	op = _parser::peek_token(ctx);
	while ( ((op.type == _token::TOK_SHIFT_LEFT) || (op.type == _token::TOK_SHIFT_RIGHT)) )
	{
		_parser::next_token(ctx);
		expr = _parser::new_expr_binary(op, expr, _parser::parse_expr_add(ctx));
		op = _parser::peek_token(ctx);
	}
	return expr;
}
static _ast::Node* parse_expr_add(_parser::Parser* ctx)
{
	_ast::Node* expr;
	expr = _parser::parse_expr_mul(ctx);
	_token::Token op;
	op = _parser::peek_token(ctx);
	while ( ((op.type == _token::TOK_PLUS) || (op.type == _token::TOK_MINUS)) )
	{
		_parser::next_token(ctx);
		expr = _parser::new_expr_binary(op, expr, _parser::parse_expr_mul(ctx));
		op = _parser::peek_token(ctx);
	}
	return expr;
}
static _ast::Node* parse_expr_mul(_parser::Parser* ctx)
{
	_ast::Node* expr;
	expr = _parser::parse_expr_unary(ctx);
	_token::Token op;
	op = _parser::peek_token(ctx);
	while ( (((op.type == _token::TOK_STAR) || (op.type == _token::TOK_SLASH)) || (op.type == _token::TOK_PERCENT)) )
	{
		_parser::next_token(ctx);
		expr = _parser::new_expr_binary(op, expr, _parser::parse_expr_unary(ctx));
		op = _parser::peek_token(ctx);
	}
	return expr;
}
static _ast::Node* new_expr_binary(_token::Token op, _ast::Node* left, _ast::Node* right)
{
	_ast::Node* expr;
	expr = _ast::new_node(_ast::AST_EXPR_BINARY);
	expr->data = op;
	_ast::add_node(expr, left);
	_ast::add_node(expr, right);
	return expr;
}
static _ast::Node* parse_expr_unary(_parser::Parser* ctx)
{
	_token::Token t;
	t = _parser::peek_token(ctx);
	_ast::Node* e;
	e = NULL;
	if ( ((((((t.type == _token::TOK_LOGICAL_NOT) || (t.type == _token::TOK_NOT)) || (t.type == _token::TOK_PLUS)) || (t.type == _token::TOK_MINUS)) || (t.type == _token::TOK_STAR)) || (t.type == _token::TOK_AND)) )
	{
		e = _ast::new_node(_ast::AST_EXPR_UNARY);
		e->data = _parser::next_token(ctx);
		_ast::add_node(e, _parser::parse_expr_postfix(ctx));
	}
	else
	{
		e = _parser::parse_expr_postfix(ctx);
	}
	return e;
}
static _token::Token match(_parser::Parser* ctx, int type)
{
	_token::Token tok;
	tok = _parser::next_token(ctx);
	if ( (tok.type != type) )
	{
		_parser::parser_exit_mismatch(ctx, (&tok), (("expected '" + _token::token_type_string(type)) + "'"));
	}
	return tok;
}
static bool is_token_not_end(_token::Token tok)
{
	return ((tok.type != _token::TOK_EOL) && (tok.type != _token::TOK_EOF));
}
static bool is_next_token_not_end(_parser::Parser* ctx)
{
	return _parser::is_token_not_end(_parser::peek_token(ctx));
}
static bool is_ast_child(_token::Token parent, _token::Token child)
{
	return ((child.type != _token::TOK_EOF) && (child.pos.indent == (parent.pos.indent + 1)));
}
static bool is_next_token_ast_child(_parser::Parser* ctx, _token::Token* parent)
{
	return _parser::is_ast_child((*parent), _parser::peek_token(ctx));
}
static void match_end(_parser::Parser* ctx)
{
	_token::Token next;
	next = _parser::next_token(ctx);
	if ( _parser::is_token_not_end(next) )
	{
		_parser::parser_exit_mismatch(ctx, (&next), "expected end-of-line or end-of-file");
	}
}
static bool is_type_token(int type)
{
	return ((((((type == _token::TOK_BOOL) || (type == _token::TOK_INT)) || (type == _token::TOK_FLOAT)) || (type == _token::TOK_STRING)) || (type == _token::TOK_FILE)) || (type == _token::TOK_ID));
}
static _token::Token match_type(_parser::Parser* ctx)
{
	_token::Token tok;
	tok = _parser::next_token(ctx);
	if ( (!_parser::is_type_token(tok.type)) )
	{
		_parser::parser_exit_mismatch(ctx, (&tok), "expected type");
	}
	return tok;
}
static _token::Token next_token(_parser::Parser* ctx)
{
	_token::Token tok;
	tok = _scanner::scanner_next_token((&ctx->scanner));
	if ( ctx->scanner.is_error )
	{
		_parser::parser_exit_error(ctx, ctx->scanner.error_reason, ctx->scanner.pos);
	}
	return tok;
}
static _token::Token peek_token(_parser::Parser* ctx)
{
	_token::Token tok;
	tok = _scanner::scanner_peek_token((&ctx->scanner));
	if ( ctx->scanner.is_error )
	{
		_parser::parser_exit_error(ctx, ctx->scanner.error_reason, ctx->scanner.pos);
	}
	return tok;
}
} // namespace
