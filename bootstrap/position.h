#pragma once
// Includes
#include <ecc_sys.h>
namespace _position {
// Constants
// Structure Prototypes
struct Position;
// Structures
struct Position
{
	int line;
	int col;
	int indent;
	Position()
	{
		line = 0;
		col = (-1);
		indent = 0;
	}
};
// Function prototypes
ecc_string pos_to_string(_position::Position* pos);
// Function definitions (public and generic)
} // namespace
