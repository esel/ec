#!/usr/bin/env python3

from subprocess import check_call
from os.path import join, basename, splitext, normpath
import glob
import sys
import argparse
import tempfile

parser = argparse.ArgumentParser(description='Build script for ECC.')
parser.add_argument('--ec-dir', default=normpath('./ecc'))
parser.add_argument('--cpp-dir', default=normpath('./build'))
parser.add_argument('--obj-dir', default=normpath('./build'))
parser.add_argument('--exe-dir', default=normpath('./build'))
parser.add_argument('--ecc', default=normpath('./build/ecc'))
parser.add_argument('--bootstrap', action='store_true')
parser.add_argument('--verbose', action='store_true')
args = parser.parse_args()

cpp_files = []
obj_files = []

if args.bootstrap:
	print('[ECC] Bootstrapping')
	for in_file in glob.iglob(join(args.cpp_dir, '*.cpp')):
		name = basename(in_file)
		name_no_ext = splitext(name)[0]
		cpp_files.append(in_file)
		obj_files.append(join(args.obj_dir, name_no_ext) + '.o')
else:
	print('[ECC] -- compiling ec to c++')
	for in_file in glob.iglob(join(args.ec_dir, '*.ec')):
		name = basename(in_file)
		name_no_ext = splitext(name)[0]
		out_file = join(args.cpp_dir, name_no_ext)
		cpp_files.append(out_file + '.cpp')
		obj_files.append(join(args.obj_dir, name_no_ext) + '.o')
		cmd = ' '.join([args.ecc, in_file, out_file, args.ec_dir])
		print('[ECC] ', in_file, ' -> ', out_file)
		if args.verbose:
			print(cmd)
			check_call(cmd, shell=True)
		else:
			check_call(cmd, stdout=tempfile.TemporaryFile(), shell=True)

print('[ECC] -- compiling c++ to native')

for cpp_file, obj_file in zip(cpp_files, obj_files):
	print('[ECC] ', cpp_file, ' -> ', obj_file)
	cmd = ' '.join(['g++', '-c', '-g', '-Iecc_target', '-I' + args.cpp_dir, '-o', obj_file, cpp_file])
	if args.verbose:
		print(cmd)
	check_call(cmd, shell=True)

exe_file = join(args.exe_dir, 'ecc')
print('[ECC] -- linking object files into executable')
print('[ECC] ', exe_file)
cmd = ' '.join(['g++ -o', exe_file, ' '.join(obj_files)])
if args.verbose:
	print(cmd)
check_call(cmd, shell=True)

print('Done.')
