#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string>
#include <iostream>
#include <sstream>
#include <cassert>
#include <cstring>

#define ecc_sizeof(expr) sizeof(expr)

#define ecc_assert(expr, msg) ecc_assert_func((expr), (msg), __FILE__, __LINE__)

typedef FILE*              ecc_file;
typedef std::string        ecc_string;
static const int           ecc_eof = EOF;

static ecc_string ecc_get_platform(void)
{
#if __APPLE__ & __MACH__
	return "darwin";
#elif __linux__
	return "linux";
#elif _WIN32
	return "win32";
#else
	return "";
#endif
}

static ecc_string ecc_to_string(int i)
{
	std::ostringstream buffer;
	buffer << i;
	return buffer.str();
}

static ecc_string ecc_to_string(float f)
{
	std::ostringstream buffer;
	buffer << f;
	return buffer.str();
}

static void ecc_assert_func(bool expr, const ecc_string& msg, const char * file, int line)
{
	if( !expr ){
		std::cerr << "ERROR: assertion failure at " << file << ":" << line << " " << msg << std::endl;
		abort();
	}
}

static int ecc_len(const ecc_string& str)
{
	return str.size();
}

static float ecc_to_float(const ecc_string& str, bool& err)
{
	std::istringstream buffer(str);
	bool fail = false;
	float f;
	buffer >> std::ws >> f;
	fail = buffer.fail();
	buffer >> std::ws;
	bool eof = buffer.eof();
	if( fail || buffer.fail() || !eof ){
		err = true;
		return 0.0f;
	}
	err = false;
	return f;
}

static int ecc_to_int(const ecc_string& str, bool& err)
{
	err = false;
	int i = 0;
	std::istringstream buffer(str);
	buffer >> std::ws;
	if( buffer.peek() == '0' ){
		buffer.ignore();
		char c = buffer.peek();
		if( c == 'x' || c == 'X' ){
			buffer.ignore();
			buffer >> std::hex;
		}else{
			if( buffer.eof() ){
				return 0;
			}
		}
	}
	bool fail = false;
	buffer >> i;
	fail = buffer.fail();
	buffer >> std::ws;
	bool eof = buffer.eof();
	if( fail || buffer.fail() || !eof ){
		err = true;
		return 0;
	}
	return i;
}

static int ecc_to_char(const ecc_string& str)
{
	ecc_assert(str.size() == 1, "string has a single char");
	return (int)str[0];
}

static const char* ecc_c_str(const ecc_string& str)
{
	return str.c_str();
}

static ecc_string ecc_to_char(int i)
{
	return ecc_string(1, (char)i);
}

static ecc_string ecc_to_char(const int* buffer, int count)
{
	ecc_string s;
	s.reserve(count);
	for( int i = 0; i < count; i++ ){
		s.push_back((char)buffer[i]);
	}
	return s;
}
