How to enable syntax highlighting in SublimeText2
===

Make a symbolic link to "ide/EC.tmLanguage" in the user package dir. 

Linux:
---
/home/username/.config/sublime-text-2/Packages/User

OSX:
---
/Users/username/Library/Application Support/Sublime Text 2/Packages/User

Windows:
---
%APPDATA%\Sublime Text 2\Packages\User

(on my Windows 7 pc: C:\Users\eriklovlie\AppData\Roaming\Sublime Text 2\Packages\User)
