#!/usr/bin/env bash
build/ecc scratch/scratch.ec build/scratch scratch && \
g++ -c -g -Iecc_target -Ibuild build/scratch.cpp -o build/scratch.o && \
g++ build/scratch.o -o build/scratch && \
build/scratch
