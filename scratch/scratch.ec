use mymodule::*

fun main:
	var foo Foo{int}
	init_foo(&foo, foo_callback, 42)

fun foo_callback(i int):
	println("foo_callback ", i)
