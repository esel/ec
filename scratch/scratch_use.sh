#!/usr/bin/env bash
build/ecc scratch/mymodule.ec build/mymodule scratch && g++ -c -g -Iecc_target build/mymodule.cpp -o build/mymodule.o && \
build/ecc scratch/scratch.ec build/scratch scratch && g++ -c -g -Iecc_target -Ibuild build/scratch.cpp -o build/scratch.o && \
g++ build/mymodule.o build/scratch.o -o build/scratch && \
build/scratch
