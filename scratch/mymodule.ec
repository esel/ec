fun public foo_init {T} (foo *Foo{T}, callback_fun fun (t T), callback_data T):
	println("foo")
	foo.callback_fun = callback_fun
	foo.callback_data = callback_data

fun public foo_do {T} (foo *Foo{T}, i int) :
	foo.i = i
	foo.callback_fun(foo)

struct public Foo {T} :
	i int
	callback_fun fun (t T)
	callback_data T
