struct A:
	i int = 42
	j int = 42
	s string = "mystring"

struct B:
	a A

fun main:
	var b B
	b.a.j = 43

	println(b.a.i)
	#expect:42
	println(b.a.j)
	#expect:43
	println(b.a.s)
	#expect:mystring
