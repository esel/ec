fun main:
	print(foo())

fun foo () int :
	var i int
	for i in 0, 10:
		print(i)
	# should return zero, since a loop counter is local to the loop scope (as in c++)
	return i

#expect:01234567890
