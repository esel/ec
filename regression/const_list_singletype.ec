fun main:
	println(MYCONST0)
	#expect:0
	println(MYCONST1)
	#expect:1
	println(MYCONST2)
	#expect:2

	println(MYCONST3)
	#expect:3
	println(MYCONST4)
	#expect:4
	println(MYCONST5)
	#expect:5

const int:
	MYCONST0 = 0
	MYCONST1 = 1
	MYCONST2 = 2
	MYCONST3
	MYCONST4
	MYCONST5
