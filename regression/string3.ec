#expectedfail
# multiline strings are not a priority for now, but should be implemented

fun main:
	println(
		"My " + "complex " +
		'multiline' +
		" string" + ' thing'
	)

	#expect:My complex multiline string thing

	println(
		"My " "complex "
		'multiline'
		" string" ' thing without +'
	)

	#expect:My complex multiline string thing without +
