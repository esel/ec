fun main:
	println(MYCONST0)
	#expect:0
	println(MYCONST1)
	#expect:1
	println(MYCONST2)
	#expect:2

	c::printf("%.6f\n", MYCONST3)
	#expect:9.000000
	c::printf("%.6f\n", MYCONST4)
	#expect:8.000000
	c::printf("%.6f\n", MYCONST5)
	#expect:7.000000

	println(MYCONST6)
	#expect:1
	println(MYCONST7)
	#expect:my constant string

const:
	MYCONST0 int = 0
	MYCONST1 int = 1
	MYCONST2 int = 2

const:
	MYCONST3 float = 9.0
	MYCONST4 float = 8.0
	MYCONST5 float = 7.0

const:
	MYCONST6 bool = true
	MYCONST7 string = "my constant string"
