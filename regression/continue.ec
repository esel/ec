fun foo (skip int) :
	var i int = -1
	while i < 4:
		i += 1
		if i == skip:
			continue
		print(i)
	println()

fun main:
	foo(0)
	#expect:1234

	foo(1)
	#expect:0234

	foo(2)
	#expect:0134

	foo(3)
	#expect:0124

	foo(4)
	#expect:0123

	foo(5)
	#expect:01234
