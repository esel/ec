fun main :
	foo()
	#expect:foo

	bar()
	#expect:bar

	tulle(1)
	#expect:1

	var i int
	i = bulle(2)
	#expect:2

	println(i)
	#expect:3

fun foo :
	println("foo")

fun bar :
	println("bar")

fun tulle (i int) :
	println(i)

fun bulle (i int) int :
	println(i)
	return i + 1
