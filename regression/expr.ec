fun main :
	println(1)
	#expect:1

	println((1))
	#expect:1

	println((((1))))
	#expect:1

	println((1) + (2))
	#expect:3

	println((1 + 2))
	#expect:3

	println(((1) + (2)))
	#expect:3

	println( 1 + 2 - 1 )
	#expect:2

	println( 1 + 2 - 1 - 1 )
	#expect:1

	println(1 + (2 - 1))
	#expect:2

	println( 1 + 2 * 3 )
	#expect:7

	println( (1 + 2) * 3 )
	#expect:9
