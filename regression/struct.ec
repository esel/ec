struct A:
	i int

struct B:
	a A[5]

struct C:
	b B[3]
	a A[4]

fun main:
	var a A
	var b B
	var c C

	a.i = 42
	println(a.i)
	#expect:42

	b.a[1].i = 1
	println(b.a[1].i)
	#expect:1

	b.a[2].i = 2
	println(b.a[2].i)
	#expect:2

	println(b.a[1].i)
	#expect:1
