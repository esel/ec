fun main:
	var i int = 42
	if peek_token(i).type == 0:
		next_token(i)

fun peek_token (i int) Token :
	var t Token
	t.type = i
	return t

fun next_token (i int) Token :
	return peek_token(i)

struct Token:
	type int = 0
