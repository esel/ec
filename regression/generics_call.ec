fun main:
	foo{int}(3)
	#expect:3
	foo{bool}(true)
	#expect:1

fun foo {T} (yo T):
	println(yo)
