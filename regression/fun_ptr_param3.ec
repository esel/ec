fun main:
	return

fun init_foo {T} (foo *Foo{T}, callback_fun fun (t T), callback_data T):
	foo.callback_fun = callback_fun
	foo.callback_data = callback_data

struct Foo {T} :
	callback_fun fun (t T)
	callback_data T
