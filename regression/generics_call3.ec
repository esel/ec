#expectedfail
# fails due to only matching a single type token for each generic parameter (so no asterix allowed)
# uncertain of whether to fix this.

fun main :
	var i int = 42
	var j int = 42
	if foo{int*,int*}(&i,&j):
		println("true")
	else:
		println("false")

fun foo {A,B} foo(a A, b B) bool :
	return a < b
