fun main:
	foo(0)

	foo(1)
	#expect:0

	foo(2)
	#expect:0
	#expect:1

fun foo (limit int) :
	var i int = 0
	while true:
		if i == limit:
			break
		println(i)
		i += 1
