fun main:
	var arrptr * int[]
	arrptr = new(1, int)
	arrptr[0] = 123

	var ptrarr int[1] *
	ptrarr[0] = nil

	#expect:0
	println(ptrarr[0])

	ptrarr[0] = &arrptr[0]
	#expect:123
	println(*ptrarr[0])

	var i int = 42
	ptrarr[0] = &i

	#expect:42
	println(*ptrarr[0])
