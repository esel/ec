struct T:
	a int
	b int

fun foo (i int) T :
	var t T
	t.a = i
	t.b = 42
	return t

fun main:
	var t T = foo(123)
	println(t.a, " ", t.b)
	#expect:123 42

	println(foo(456).a)
	#expect:456	