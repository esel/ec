struct T:
	i int = 42
	j int = 42
	s string = "mystring"

fun main:
	var t T
	t.j = 43

	println(t.i)
	#expect:42
	println(t.j)
	#expect:43
	println(t.s)
	#expect:mystring
