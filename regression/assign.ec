fun main :
	var i int
	i = 1

	println(i)
	#expect:1

	i += 1
	println(i)
	#expect:2

	i -= 1
	println(i)
	#expect:1

	modify_copy(i)
	println(i)
	#expect:1

	i += fortytwo()
	println(i)
	#expect:43

	var f float
	f = 1.0
	c::printf("%.2f\n", f)
	#expect:1.00

fun modify_copy (i int) :
	i += 1

fun fortytwo int :
	return 42
