fun main:
	var yo Yo{int,float}
	yo.a = 42
	yo.b = 3.14
	println(yo.a, " - ", yo.b)
	#expect:42 - 3.14

struct Yo {A,B}:
	a A
	b B
