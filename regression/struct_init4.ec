const int:
	MYCONST

struct A:
	i int = MYCONST

fun main:
	var a A
	println(a.i)
	#expect:0