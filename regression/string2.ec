fun main:
	println('My "double quoted" string')
	#expect:My "double quoted" string

	println("My 'single quoted' string")
	#expect:My 'single quoted' string
