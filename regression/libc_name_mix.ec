fun main:
	c::printf("abs(-42) using module function = %i\n", abs(-42))
	#expect:abs(-42) using module function = -1
	c::printf("abs(-42) using stdlib function = %i\n", c::abs(-42))
	#expect:abs(-42) using stdlib function = 42

fun abs (i int) int :
	# a function with the same signature as stdlib abs, but with a completely different behavior
	return -1
