fun main:
	foo(7, 13)
	#expect:20

fun foo {A,B} (a A, b B):
	println(a + b)
