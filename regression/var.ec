fun main:
	var i1 int = 42
	println(i1)
	#expect:42

	var f1 float = 0.5
	c::printf("%.2f\n", f1)
	#expect:0.50

	var s1 string = "mongo bongo"
	println(s1)
	#expect:mongo bongo


