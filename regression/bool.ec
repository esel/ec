fun main:
	var b bool
	b = true
	println(b)
	#expect:1

	b = false
	println(b)
	#expect:0

	b = 1 == 1
	println(b)
	#expect:1

	b = 1 == 2
	println(b)
	#expect:0
