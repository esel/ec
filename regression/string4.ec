#expectedfail
# multiline strings are not a priority for now, but should be implemented

fun main:
	var s string =
		"My " + "complex " +
		'multiline' +
		" string" + ' thing'

	println(s)
	#expect:My complex multiline string thing
