fun main :
	var liste int[4]
	var i int
	i = 0
	while( i < 4 ):
		liste[i] = i
		print(liste[i])
		#expect:0123
		i += 1

	println("")

	println(liste[1] + liste[2])
	#expect:3

	println(~liste[0])
	#expect:-1
