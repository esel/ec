fun main:
	var a1 *A
	if true:
		var a2 *A
		a2 = new(A)
		a2.i = 42
		a1 = a2
	println(a1.i)
	#expect:42
	delete(a1)

struct A:
	i int
