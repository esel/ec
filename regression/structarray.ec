fun main:
	var c C[10]

	c[3].b[2].a[1].i = 321
	println(c[3].b[2].a[1].i)
	#expect:321

	c[4].a[3].i = 43
	println(c[4].a[3].i)
	#expect:43

	foo(&c[9])
	println(c[9].a[0].i)
	#expect:666

fun foo (param *C) :
	param.a[0].i = 666

struct C:
	b B[3]
	a A[4]

struct B:
	a A[2]

struct A:
	i int
