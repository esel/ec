fun main:
	var arr int[4]
	arr[0] = 1
	arr[1] = 2
	arr[2] = 3
	arr[3] = 4

	foo(arr, 3)
	#expect:4
	foo(arr, 2)
	#expect:3
	foo(arr, 1)
	#expect:2
	foo(arr, 0)
	#expect:1

	var arr2 UserType[1]
	arr2[0].i = 42
	bar(arr2, 0)
	#expect:42

fun foo (arrayparam int[], index int) :
	println(arrayparam[index])

fun bar (arrayparam UserType[], index int) :
	println(arrayparam[index].i)

struct UserType:
	i int
