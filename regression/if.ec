fun main :
	foo(0)
	#expect:i < 10

	foo(10)
	#expect:10 <= i < 100

	foo(100)
	#expect:i >= 100


fun foo (i int) :
	if i < 10:
		println("i < 10")
	 elif i < 100:
		println("10 <= i < 100")
	 else:
		println("i >= 100")
