struct A:
	i int = 42

struct B:
	a A[2]

fun main:
	var b B
	println(b.a[0].i)
	#expect:42
	println(b.a[1].i)
	#expect:42