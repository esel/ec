fun main :
	println(0)
	#expect:0

	println(123)
	#expect:123

	println(-123)
	#expect:-123

	println(+123)
	#expect:123

	println(0x10)
	#expect:16

