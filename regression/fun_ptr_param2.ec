fun main:
	return

fun init_foo (foo *Foo, callback_fun fun (i int), callback_data int):
	foo.callback_fun = callback_fun
	foo.callback_data = callback_data

struct Foo :
	callback_fun fun (i int)
	callback_data int
