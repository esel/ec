fun main :
	var v Vector

	v.arr = new(10, int)
	v.arr[0] = 42
	println(v.arr[0])
	#expect:42

	delete(v.arr)

	v.ptr = new(int)
	delete(v.ptr)

struct Vector:
	arr *int[]
	ptr *int
