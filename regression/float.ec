fun main :
	c::printf("%.2f\n", 1.0)
	#expect:1.00

	c::printf("%.2f\n", 1.)
	#expect:1.00

	c::printf("%.2f\n", .5)
	#expect:0.50

	c::printf("%.2f\n", 1.0e6)
	#expect:1000000.00

	c::printf("%.2f\n", .1e6)
	#expect:100000.00

	c::printf("%.2f\n", 1.e6)
	#expect:1000000.00

	c::printf("%.2f\n", 1000000.000000e-6)
	#expect:1.00

	c::printf("%.2f\n", 1.e+6)
	#expect:1000000.00
