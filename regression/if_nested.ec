fun foo (i int, j int, k int) :
	if i == 1:
		if j == 1:
			println("11")
		elif j == 2:
			println("12")
	elif i == 2:
		if j == 1:
			println("21")
		elif j == 2:
			println("22")
			if k == 1:
				println("221")
	elif i == 3:
		println("3")
	else:
		println("x")

fun main:
	foo(3, 1, 1)
	#expect:3
