fun main:
	var err bool
	var i int

	i = to_int("123", err)
	println(err, " ", i)
	#expect:0 123

	i = to_int("123  ", err)
	println(err, " ", i)
	#expect:0 123

	i = to_int("0xFF", err)
	println(err, " ", i)
	#expect:0 255

	i = to_int("0xff", err)
	println(err, " ", i)
	#expect:0 255

	i = to_int("0xfF", err)
	println(err, " ", i)
	#expect:0 255

	i = to_int("0xFf", err)
	println(err, " ", i)
	#expect:0 255

	i = to_int("  123", err)
	println(err, " ", i)
	#expect:0 123

	i = to_int("  0xFF", err)
	println(err, " ", i)
	#expect:0 255

	i = to_int("123 456", err)
	println(err, " ", i)
	#expect:1 0
