fun main (argc int, argv string[]) :
	println(argc)
	#expect:1
	var i int = 0
	while i < argc:
		println(argv[i])
		#expect:build/regression/mainargs
		i += 1
