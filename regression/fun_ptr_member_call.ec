fun main :
	var foo Foo
	yo(&foo)

fun yo (foo *Foo) :
	foo.fp(42)

struct public Foo :
	fp fun (i int)
