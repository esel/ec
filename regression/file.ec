struct T:
	f file = nil

fun main:
	var t T
	println(t.f)
	#expect:0

	t.f = nil
	println(t.f)
	#expect:0
