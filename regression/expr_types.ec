fun main :
	println(1)
	#expect:1

	println(2*3)
	#expect:6

	println(6/2)
	#expect:3

	println(6%2)
	#expect:0

	println(6%4)
	#expect:2

	println(1 << 3)
	#expect:8

	println(8 >> 3)
	#expect:1

	println(1 == 1)
	#expect:1

	println(1.1 == 1.1)
	#expect:1

	println(1.1 == 1.10)
	#expect:1

	println(1.1 == 1.11)
	#expect:0

	println(1 == 2)
	#expect:0

	println(1 != 1)
	#expect:0

	println(1 != 2)
	#expect:1

	println(!false)
	#expect:1

	println(!true)
	#expect:0

	println(~0)
	#expect:-1

	println(-42)
	#expect:-42

	println(-(-42))
	#expect:42

	println(+42)
	#expect:42

	println(+(+42))
	#expect:42

