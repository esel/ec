const:
	MYCONST0 int
	MYCONST1 int
	MYCONST2 int

const:
	MYCONST3 float
	MYCONST4 float
	MYCONST5 float

const:
	MYCONST6 int
	MYCONST7 float
	MYCONST8 string = "my string constant"
	MYCONST9 bool = true

fun main:
	println(MYCONST0)
	#expect:0
	println(MYCONST1)
	#expect:1
	println(MYCONST2)
	#expect:2

	c::printf("%.6f\n", MYCONST3)
	#expect:0.000000
	c::printf("%.6f\n", MYCONST4)
	#expect:1.000000
	c::printf("%.6f\n", MYCONST5)
	#expect:2.000000

	println(MYCONST6)
	#expect:0
	c::printf("%.6f\n", MYCONST7)
	#expect:1.000000
	println(MYCONST8)
	#expect:my string constant
	println(MYCONST9)
	#expect:1
