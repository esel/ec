#expectedfail
# C++ doesn't appear to support generic return type, which seems weird.

fun main:
	var i int
	i = foo(7, 13)
	println(i)
	#expect:20

fun foo {A,B,C} (b B, c C) A :
	return b + c
