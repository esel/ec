#expectedfail
# "use" tests are not setup correctly

use mymodule::*

struct T:
	p MyPublicType1

fun main:
	var t T
	t.p.x += 1
	println("yo ", t.p.x)
	var p MyPublicType1
	p.x = 42
	myFunction3(&p)
	p.x = 64
	myFunction4(p)
