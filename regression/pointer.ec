fun main :
	var a1 A
	var a2 *A
	a2 = nil

	a1.i = 42
	println(a1.i)
	#expect:42
	println(a2)
	#expect:0

	a2 = &a1
	println(a1.i)
	#expect:42
	println(a2.i)
	#expect:42

	a2.i = 123
	println(a1.i)
	#expect:123
	println(a2.i)
	#expect:123

struct A:
	i int
