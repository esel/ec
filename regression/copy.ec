struct T:
	i1 int
	i2 int	

struct A:
	i1 int
	t T

fun main:
	var t1 T
	var t2 T

	t1.i1 = 123
	t1.i2 = 456

	t2 = t1
	println(t2.i1, " ", t2.i2)
	#expect:123 456

	var a1 A
	var a2 A

	a1.i1 = 123
	a1.t.i1 = 456
	a1.t.i2 = 789

	a2 = a1
	println(a2.i1, " ", a2.t.i1, " ", a2.t.i2)
	#expect:123 456 789
