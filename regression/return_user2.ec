struct T:
	i int

fun foo T:
	var t T
	t.i = 42
	return t

fun bar T:
	return foo()

fun main:
	var t T
	t = bar()
	println(t.i)
	#expect:42