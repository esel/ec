fun main:
	var ptr *T
	ptr = new(T)
	ptr.i = 42
	println(ptr.i)
	#expect:42
	delete(ptr)

struct T:
	i int
