fun foo int:
	print("should not be called")
	return 0

fun main :
	println(true || foo())
	#expect:1

	println(false && foo())
	#expect:0
