fun main:
	var b *B
	if foo(b).a_member:
		return
	if bar(b).a_member:
		return

fun foo (b *B) A :
	var a A
	return a

fun bar (b *B) *A :
	var a *A = new(A)
	return a

struct A:
	a_member bool = false

struct B:
	b_member bool = false
