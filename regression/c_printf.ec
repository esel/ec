fun main:
	var s string = "ansi C"
	c::printf("printf is a function in the %s standard library!\n", c_str(s))
	#expect:printf is a function in the ansi C standard library!
	c::printf("and also in the %s standard library!\n", "iso C")
	#expect:and also in the iso C standard library!
	c::printf("pi = %.3f\n", 3.14152)
	#expect:pi = 3.142
