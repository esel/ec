fun main:
	foo{int}(3)
	#expect:3
	foo{bool}(false)
	#expect:0

fun foo {T} (yo T):
	println(yo)

fun bar {T} (yo T):
	foo{T}(yo)
