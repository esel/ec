fun main:
	println(MYCONST_INT)
	#expect:42

	c::printf("%.6f\n", MYCONST_FLT)
	#expect:3.140000

	println(MYCONST_BOO)
	#expect:0

	println(MYCONST_STR)
	#expect:my constant string

const: MYCONST_INT int = 41 + 1
const: MYCONST_FLT float = 3.14
const: MYCONST_BOO bool = (false && true)
const: MYCONST_STR string = "my constant string"
