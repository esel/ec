struct T:
	i int = 42
	t * T = nil

fun assign2 (parent *T, child *T) :
	parent.t = child

fun assign (parent *T, child *T) :
	assign2(parent, child)

fun main:
	var parent T
	parent.i = 1
	var child T
	child.i = 2
	assign(&parent, &child)
	println(parent.t)
	#regexpect:0x.+$
