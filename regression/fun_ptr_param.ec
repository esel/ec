fun main:
	yo(nil, 42)
	#expect:yo=42

fun yo (fp fun (x int) bool, data int) :
	println("yo=", data)
