fun main:
	foo(bar)
	#expect:yo=43

fun foo (yo fun (x int) int) :
	println("yo=", yo(42))

fun bar (x int) int :
	return x + 1
