#expectedfail
# known issue due to not looking at the _expression_ type when deciding whether to emit -> or . (assume lhs is an id)

fun main:
	var a A
	println(&a.i)
	println((&a).i)

struct A:
	i int = 42
