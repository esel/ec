fun main:
	var m Matrix3{int}
	var pi int
	pi = matrix_foo(3)
	#expect:v = 3
	println("pi = ", pi)
	#expect:pi = 3
	matrix_bar(&m)
	#expect:0
	matrix_init(&m, 9)
	matrix_print(&m)
	#expect:[(9,9,9)(9,9,9)(9,9,9)]

fun matrix_foo {T} (arg T) T :
	var v T
	v = arg
	println("v = ", v)
	return v

fun matrix_bar {T} (arg *Matrix3{T}):
	c::memset(arg.m, 0, sizeof(arg.m))
	println(arg.m[0])

fun matrix_init {T} (mat * Matrix3{T}, val T):
	for i in 0, 9:
		mat.m[i] = val

fun matrix_print {T} (mat * Matrix3{T}):
	print("[")
	for i in 0, 3:
		print("(")
		for j in 0, 3:
			print(mat.m[i*3 + j])
			if j != 2:
				print(",")
		print(")")
	println("]")

struct Matrix3 {T}:
	m T[9]
