fun main:
	var a *int[]
	a = nil
	println(a)
	#expect:0

	a = new(10, int)

	a[0] = 123
	println(a[0])
	#expect:123

	a[9] = 456
	println(a[9])
	#expect:456

	delete(a)
