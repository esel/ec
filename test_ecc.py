#!/usr/bin/env python3

import subprocess as sp
import re
import os.path as path
import glob
import sys
import os
import argparse

REGEXPECT = "#regexpect:"
VERBATIM = "#expect:"
BROKEN = "#expectedfail"

#
# Regression tests are EC programs which print some values to stdout.
# The output is matched to expected values which are recorded in comments in the test files themselves.
# Each expected output is recorded as such: #expect:<verbatim output>
# NOTE: trailing whitespace in the expected and actual output is removed before comparison.
#

def get_expected_output(testfile):
	expect_list = []
	expectedfail = False
	with open(testfile) as test:
		linenum = 0
		for line in test:
			linenum += 1
			line = line.strip()
			if line.find('#expectedfail') == 0:
				expectedfail = True
			if line.find(VERBATIM) == 0:
				expect_list.append( (VERBATIM, linenum, line[len(VERBATIM):]) )
			elif line.find(REGEXPECT) == 0:
				expect_list.append( (REGEXPECT, linenum, line[len(REGEXPECT):]) )
	return (expect_list, expectedfail)

def compare_output(args, expect_list, testout):
	actual_line = 0
	n_matching = 0
	failed = []
	for line in testout.splitlines():
		line = line.rstrip()
		if len(line) == 0:
			continue
		if actual_line >= len(expect_list):
			if args.verbose:
				print('[EC] FAIL extranous output')
			failed.append(('extranous output: ' + line, actual_line))
			return n_matching, actual_line, failed
		cmptype, expect_line, expect = expect_list[actual_line]
		actual_line += 1
		matched = False
		if cmptype == VERBATIM:
			matched = (expect == line)
		else:
			p = re.compile(expect)
			matched = p.match(line)
		if matched:
			n_matching += 1
			if args.verbose:
				print('[EC] PASS test line=', expect_line, ' value=', expect)
		else:
			failed.append(('incorrect output', expect_line))
			if args.verbose:
				print('[EC] FAIL test line=', expect_line, ' expect=', expect, ' actual=', line)
	return n_matching, actual_line, failed

def wait_and_get_output(pid):
	out, err = pid.communicate()
	out = out.decode("utf-8")
	err = err.decode("utf-8")
	return pid, out, err

def functional(args):
	failed = []
	passed = 0
	expected_fail_but_passed = []
	ecc_include_dirs = [path.join('regression','utils')]
	for test in glob.glob('regression/*.ec'):
		if args.tests and test not in args.tests:
			continue
		print('[EC] testing: ', test)
		expected_output, expected_fail = get_expected_output(test)

		output_file_no_ext = path.join('build', test[:-3])
		cmd = [path.join('build', 'ecc'), test, output_file_no_ext] + ecc_include_dirs
		# generating c++ using ecc
		if args.verbose:
			print('[EC] VERBOSE: ', ' '.join(cmd))
		pid, testout, testerr = wait_and_get_output(sp.Popen(cmd, stdout=sp.PIPE, stderr=sp.PIPE))
		if args.verbose:
			print('[EC] ecc stdout:')
			print(testout)
			print('[EC] ecc stderr:')
			print(testerr)
		if pid.returncode != 0 or len(testerr) != 0:
			print('[EC] FAIL ecc compilation ', pid.returncode)
			failed.append((test, 'ecc', expected_fail))
			continue
		# generating executable using g++
		pid, testout, testerr = wait_and_get_output(sp.Popen(['g++', '-g', '-Iecc_target', '-I' + path.join('regression', 'utils'), '-o', output_file_no_ext, output_file_no_ext + '.cpp'], stdout=sp.PIPE, stderr=sp.PIPE))
		if args.verbose:
			print('[EC] g++ stdout:')
			print(testout)
			print('[EC] g++ stderr:')
			print(testerr)
		if pid.returncode != 0 or len(testerr) != 0:
			print('[EC] FAIL g++ compilation ', pid.returncode)
			failed.append((test, 'g++', expected_fail))
			continue
		# running executable and collecting output
		pid, testout, testerr = wait_and_get_output(sp.Popen([output_file_no_ext], stdout=sp.PIPE, stderr=sp.PIPE))
		if args.verbose:
			print('[EC] test run stdout:')
			print(testout)
			print('[EC] test run stderr:')
			print(testerr)
		if pid.returncode != 0 or len(testerr) != 0:
			print('[EC] FAIL test run ', pid.returncode)
			failed.append((test, 'run', expected_fail))
			continue
		# comparing actual to expected output
		n_matching, n_outputs, diff_list = compare_output(args, expected_output, testout)
		if diff_list:
			print('[EC] FAIL incorrect output, #matching=', n_matching, ' #different=', n_outputs - n_matching)
			failed.append((test, 'cmp', expected_fail))
			continue

		if expected_fail:
			unexpected_fail_but_passed.append(test)
		passed += 1

	print('--')
	print('[EC] SUMMARY')
	print('[EC] #passed ', passed)
	print('--')
	unexpected_fails = [result for result in failed if not result[2]]
	print('[EC] #unexpected failures ', len(unexpected_fails))
	for result in unexpected_fails:
		print('[EC] (', result[1], ') ', result[0])
	print('--')
	expected_fails = [result for result in failed if result[2]]
	print('[EC] #expected failures ', len(expected_fails))
	for result in expected_fails:
		print('[EC] (', result[1], ') ', result[0])
	print('--')
	print('[EC] #unexpected passes ', len(expected_fail_but_passed))
	for result in expected_fail_but_passed:
		print('[EC] (', result[1], ') ', result[0])

def main():
	parser = argparse.ArgumentParser(description='Regression testing for ECC.')
	parser.add_argument('tests', metavar='T', nargs='*',
	                   help='restrict to a specific set of tests')
	parser.add_argument('--verbose', action='store_true',
	                   help='include extra output (e.g. from ecc and g++)')
	args = parser.parse_args()

	try:
		os.mkdir(path.join('build', 'regression'))
	except OSError:
		print('(build directory already exists)')

	functional(args)

main()
