# The EC programming language
---

This is a silly little programming language I made for myself a while back.
It was intended to be a simple C-like language ("EC"... Easy, get it?) but
with cleaner syntax. In particular it uses the off-side rule (like python).

The compiler is actually self-hosting, i.e. written in the EC language itself
and compiled using a bootstrapping process. See e.g. ecc/ecc.ec.

# Hello World (simple example)
---
	fun main:
		println("Hello World!")

# Hello World (slightly less simple example)
---
	fun main:
		println(greeting("bad"))
		c::printf("%i is the Answer!\n", 42)

	fun greeting (mood string) string :
		if mood == "bad":
			return 'Hello "cruel" World!'
		elif mood == "good":
			return 'Hello World!'
		else:
			return 'Whatever...'

# Features
---

Some examples of specific features:

- no header files (you include _symbols_, not raw text).
- no semicolons.
- no curly braces.
- indentation determines scope ("off-side rule", similar to Python).
- functions can be defined in any order.
- parentheses are optional around the expressions in conditionals and loops.
- double quotes can be used unescaped inside a single quoted string literal, and vice versa.
- functions are private (have static linkage) by default.
- accessing a struct member is always done with ".", also for pointers (the "->" token from C/C++ does not exist).

# Editors and Tools
---
There is a syntax highlighting file in the `ide` subdirectory (`ide/EC.tmLanguage`). This works well with the editor _Sublime Text 2_ and should in theory also work with _Textmate_.